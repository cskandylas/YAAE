#include "path_generator.h"
#include "task_engine.h"
#include "planner.h"
#include "mape_k.h"
#include "arc_model.h"

#include "e_map.h"
#include "dijkstra_map.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void path_generate_ctrl(struct task *t);
static void astar_run(struct task *t);

void path_generate_run(struct plan *p)
{
    
    struct task *t_astar = task_engine_create_task(astar_run, path_generate_ctrl,
							  &p->pl->mape_k->k_b, p);
    task_engine_add_task(t_astar,0);
}


static char *calculate_path(struct dijkstra_map *d_map, int from_r, int from_c)
{

    
    int min = d_map->distances[from_r][from_c];
    int pos_r = from_r;
    int pos_c = from_c;
    //printf("Min:%d\n",min);
    //path is at most these many steps due to the properties of a dijsktra map.
    char *path = malloc(min*min*min+1);
    memset(path, 0, min*min+1);

    
    
    size_t pos=0;
    while(min != 0)
    {
	char choice = 'F';

	if(pos_c - 1 >= 0 && d_map->distances[pos_r][pos_c-1] < min)
	{
	    min = d_map->distances[pos_r][pos_c-1];
	    choice = 'L';
	}

	if(pos_c  + 1 < d_map->width && d_map->distances[pos_r][pos_c+1] < min)
	{
	    min = d_map->distances[pos_r][pos_c+1];
	    choice = 'R';
	}

	if(pos_r - 1 >= 0 && d_map->distances[pos_r-1][pos_c] < min)
	{
	    min = d_map->distances[pos_r-1][pos_c];
	    choice = 'U';
	}

	if(pos_r + 1 < d_map->height && d_map->distances[pos_r+1][pos_c] < min)
	{
	    min = d_map->distances[pos_r+1][pos_c];
	    choice = 'D';
	}
	///printf("%c %d \n",choice, min);
	path[pos]=choice;
	if(choice == 'L')
	{
	    pos_c--;
	}
	else if(choice == 'R')
	{
	    pos_c++;
	}
	else if(choice == 'U')
	{
	    pos_r--;
	}
	else if(choice == 'D')
	{
	    pos_r++;
	}
	else
	{
	    printf("COULD NOT MAKE PROGRESS PANIC EXITING!\n");
	    exit(-1);
	}
	pos++;
	
	
    }
    //printf("Path: %s\n",path);

    return path;
	
}


static void astar_run(struct task *t)
{
    struct knowledge_base *k_b = task_get_ctx(t);
    struct arc_model *a_m = k_b->model;
    struct arc_value target_goal_res = arc_model_get_property(a_m,"Analyzer:AdaptationGoal:RESULT");

    
    
    if(target_goal_res.type == V_STRING && strcmp(target_goal_res.v_str,"S")==0)
    {
	arc_model_assign_property_str(a_m, "Planner:Path:RESULT","S");
	return;
    }
    
    struct arc_value rat_pos_res = arc_model_get_property(a_m,"navigation:POSITION");
    pthread_mutex_lock(&a_m->sys_lock);
    int from_r = atoi(rat_pos_res.v_str);
    int from_c = atoi(strchr(rat_pos_res.v_str,',')+1);
    
    int to_r = atoi(strchr(target_goal_res.v_str,'[')+1);
    int to_c = atoi(strchr(target_goal_res.v_str,',')+1);
    pthread_mutex_unlock(&a_m->sys_lock);
    //printf("Calculating path from [%d,%d] to: [%d,%d]\n", from_r, from_c, to_r,to_c);
    
    struct e_map *e_map = knowledge_base_get_res(k_b,"The Maze");
    
    struct dijkstra_map *d_map = malloc(sizeof(struct dijkstra_map));
    build_dijkstra_map(d_map,e_map,to_r,to_c);
    //print_e_map_unmod(e_map);
    //print_dijkstra_map(d_map, e_map);
    char *path = calculate_path(d_map,from_r,from_c);
    
    arc_model_assign_property_str(a_m, "Planner:Path:RESULT",path);
    free(path);
    destroy_dijkstra_map(d_map);
    free(d_map);
    free(rat_pos_res.v_str);
}


static void path_generate_ctrl(struct task *t)
{
    struct plan *p = task_get_cb_ctx(t);
    struct planner *pl = p->pl;
    //garbage collect task
    task_engine_free_task(t);
    
    plan_set_status(p, PLAN_DONE);
    p->done(p);
    
}


