#define _XOPEN_SOURCE 600

#include "tasks.h"

#include <string.h>
#include <unistd.h>

#include "e_map.h"
#include "knowledge_base.h"
#include "report_probe.h"
#include "probe.h"
#include "view.h"
#include "executor.h"
#include "string_buffer.h"
//#include "cmds.h"



void arch_update_run(struct task *task)
{
    
    struct knowledge_base *k_b = task_get_ctx(task);

    //struct event_queue *s_q = &k_b->platform->ev_queue;

    struct system_view *s_v = knowledge_base_get_res(k_b, "System View");
    
    //while(1)
    {
	//just to be safe that we don't modify state without holing a lock
//	struct event *evt = event_queue_peek_event(s_q);
//	if(evt && evt->evt_ty == 0)
	{
	    //printf("0 polling\n");
//	    evt = event_queue_poll_event(s_q);
	    //we don't really need the event in this case so just mark it as done.
//	    evt->evt_st = EVT_DONE;
	    
	    system_view_update(s_v,k_b->model->sys);
	    system_view_to_dot(s_v,"RatGen.dot");
	}
	//usleep(100000);
    }
    //printf("Arch update done:%u\n",task_get_id(task));
    if(task_get_id(task) >= 1000)
    {
	exit(-1);
    }
}

void map_update_run(struct task *task)
{
    struct knowledge_base *k_b = task_get_ctx(task);

    //struct event_queue *s_q = &k_b->platform->ev_queue;

    struct e_map *maze = knowledge_base_get_res(k_b, "The Maze");
    struct e_map *visited = knowledge_base_get_res(k_b, "Visited");

    struct arc_model *model = k_b->model;


    struct arc_value pos = arc_model_get_property(k_b->model,"navigation:POSITION");
    struct arc_value up = arc_model_get_property(k_b->model,"navigation:UP");
    struct arc_value down = arc_model_get_property(k_b->model,"navigation:DOWN");
    struct arc_value left = arc_model_get_property(k_b->model,"navigation:LEFT");
    struct arc_value right = arc_model_get_property(k_b->model,"navigation:RIGHT");

    //printf("%s\n",pos.v_str);
	    
    if(pos.type == V_INVALID || up.type == V_INVALID
       || down.type == V_INVALID  || left.type == V_INVALID || right.type == V_INVALID)
    {
	fprintf(stderr, "Some property was invalid");
	return;
    }
    
		
    int ro = atoi(&pos.v_str[0]);
    char *coidx = strchr(pos.v_str,',');
    if(!coidx)
    {
	fprintf(stderr,"invalid pos:[%s]\n",pos.v_str);
	return;
    }
    
    
    coidx++;
    int co = atoi(coidx);
    char u = up.v_str[0];
    char d = down.v_str[0];
    char l = left.v_str[0];
    char r = right.v_str[0];
    pthread_mutex_lock(&model->sys_lock);
    //printf("%c,%c,%c,%c\n",u,d,l,r);
    //printf("Before:\n");
    //print_e_map_unmod(maze);
    if(u != 'U')
    {
	//temporary hack till I fix the parser
	if(u == 'E')
	    u= '.';
	maze->tiles[ro-1][co] = u;
    }
    if(d != 'U')
    {
	if(d == 'E')
	    d= '.';
	maze->tiles[ro+1][co] = d;
    }
    if(l != 'U')
    {
	if(l == 'E')
	    l= '.';
	maze->tiles[ro][co-1] = l;
    }
    if(r != 'U')
    {
	if(r == 'E')
	    r= '.';
	maze->tiles[ro][co+1] = r;
    }
    
    maze->tiles[ro][co] = '.';
    visited->tiles[ro][co] = '.';
    //printf("After\n");
    //print_e_map_unmod(maze);
    //print_e_map_unmod(visited);
    pthread_mutex_unlock(&model->sys_lock);


    free(pos.v_str);
    free(up.v_str);
    free(down.v_str);
    free(left.v_str);  
    free(right.v_str); 
    
}
