#include "task.h"
#include "e_map.h"

/* this is not very nice;
    it bundles the dependencies of each task
    as includes for no reason.
    Maybe  has one .h/.c file per task later
*/

#ifndef TASKS_H
#define TASKS_H




void probe_task_run(struct task *task);

void rat_probe_t_run(struct task *task);
void arch_update_run(struct task *task);
void map_update_run(struct task *task);
void events_reap_run(struct task *task);
void pick_random_adjecent_run(struct task *task);
#endif
