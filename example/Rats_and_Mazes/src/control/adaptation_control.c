#include "adaptation_control.h"

#include <stdio.h>
#include "arc_model.h"

void monitor_control(struct probe *p)
{
    if(p)
    {
	struct monitor *m = probe_get_monitor(p);
	struct mape_k *a_c = m->mape_k;
	struct knowledge_base * k_b = &a_c->k_b;
	struct analyzer * a = &a_c->a;
	
    
	if(monitor_all_probes_done(m))
	{
	    bool adapt = m->goal_eval(k_b);
	    if(adapt)
	    {
		run_analyzer(a);
	    }
	    else
	    {
		printf("No adaptation needed, falling back to monitoring...\n");
		monitor_run(m);
	    }
	}
    }
}


void analysis_control(struct analysis *ana)
{
    //printf("Analysis control...");
    struct analyzer *a = analysis_get_analyzer(ana);
    struct mape_k *a_c = a->mape_k;
    struct monitor *m = &a_c->m;
    struct knowledge_base * k_b = &a_c->k_b;
    struct planner *p = &a_c->p;
    struct arc_model *a_m = k_b->model;
    //printf("%d\n",analyzer_all_analyses_done(a));

    if(analyzer_all_analyses_done(a))
    {

	void *strat = a->strategy_sel(k_b);
	
	if(strat)
	{
	    struct arc_value target = arc_model_get_property(a_m, "Analyzer:AdaptationGoal:RESULT");
	    pthread_mutex_lock(&a_m->sys_lock);
	    printf("Moving to planning phase, adaptation goal is set to: %s\n",target.v_str);
	    pthread_mutex_unlock(&a_m->sys_lock);
	    run_planner(p);
	    
	}
	else
	{
	    printf("Could not find an adaptation strategy that improves state, attempting to resume monitoring\n");
	    if(m->goal_eval(k_b))
	    {
		monitor_run(m);
	    }
	}
    }
}


void planning_control(struct plan *p)
{

    struct planner *pl = plan_get_planner(p);
    struct executor *ex = &pl->mape_k->e;
    
    if(planner_all_plans_created(pl))
    {
	executor_run(ex);
    }
	    
}

void execution_control(struct exec_cmd *cmd)
{

    printf("Execution control!\n");
    struct executor *ex = cmd_get_executor(cmd);
    struct monitor *m = &ex->mape_k->m;

    if(executor_all_cmds_done(ex))
    {
	printf("Executor done, returning to monitoring\n");
	monitor_run(m);
    }
}
