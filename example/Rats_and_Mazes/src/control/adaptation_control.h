#include "mape_k.h"
#include "probe.h"
#include "analysis.h"


void monitor_control(struct probe *probe);
void analysis_control(struct analysis *ana);
void planning_control(struct plan *p);
void execution_control(struct exec_cmd *cmd);


