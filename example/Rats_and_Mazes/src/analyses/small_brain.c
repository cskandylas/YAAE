#include "small_brain.h"

#include "mape_k.h"
#include "knowledge_base.h"
#include "analysis.h"
#include "e_map.h"
#include "task_engine.h"

#include <pthread.h>
#include <stdlib.h>
#include "arc_model.h"

#include <stdio.h>
#include <string.h>

 /*
   <-->
   Define operations on the maze?!
   Assume I'm only allowed to call those?
   then load them up
 */
/*
  I need a model parsing DSL here pretty much.
*/




static void found_treasure_run(struct task *t);
static void found_exit_run(struct task *t);
static void small_brain_ctrl(struct task *t);

struct small_brain_ctx
{
    bool have_treasure;
    struct analysis *a;
    struct task *t_found_treasure;
    struct task *t_found_exit;
    struct task *t_explore;
    struct task *t_snatch_treasure;
};

void small_brain(struct analysis *a)
{

    struct small_brain_ctx *rat_memory = malloc(sizeof(struct small_brain_ctx));
    rat_memory->a = a;
    rat_memory->t_found_treasure = NULL;
    rat_memory->t_found_exit = NULL;
    rat_memory->t_explore = NULL;
    rat_memory->t_snatch_treasure = NULL;
    rat_memory->have_treasure = false;

    struct arc_model *a_m = a->az->mape_k->k_b.model;

    struct arc_value have_treasure = arc_model_get_property(a_m,"Analyzer:HaveTreasure:RESULT");

    if(have_treasure.type = V_DOUBLE && have_treasure.v_dbl == 1)
    {
	printf("Have treasure, nothing to do...\n");
	arc_model_assign_property_str(a_m,"Analyzer:AdaptationGoal:RESULT","N");
	
    }
    else
    {
	rat_memory->t_found_treasure = task_engine_create_task(found_treasure_run, small_brain_ctrl,
							       &a->az->mape_k->k_b, rat_memory);
	task_engine_add_task(rat_memory->t_found_treasure,0);
    }
}

static void found_treasure_run(struct task *t)
{
    
    struct knowledge_base *k_b =  task_get_ctx(t);
    struct e_map *the_map = knowledge_base_get_res(k_b,"The Maze");

    int r=-1,c=-1;
    //This is a course lock, maybe we want a more granular one
    pthread_mutex_lock(&k_b->model->sys_lock);
    bool found_treasure = e_map_find_tile(the_map,'T',&r,&c);
    pthread_mutex_unlock(&k_b->model->sys_lock);
    if(r!=-1 && c!=-1)
    {
	char buff[256];
	sprintf(buff,"[%d,%d]",r,c);
	arc_model_assign_property_str(k_b->model,"Analyzer:TreasureFound:RESULT",buff);
    }
    else
    {
	arc_model_assign_property_str(k_b->model,"Analyzer:TreasureFound:RESULT","F");
    }
}

static void found_exit_run(struct task *t)
{
    
    struct knowledge_base *k_b =  task_get_ctx(t);
    struct e_map *the_map = knowledge_base_get_res(k_b,"The Maze");

    int r=-1,c=-1;
    //This is a course lock, maybe we want a more granular one
    pthread_mutex_lock(&k_b->model->sys_lock);
    bool found_exit = e_map_find_tile(the_map,'E',&r,&c);
    pthread_mutex_unlock(&k_b->model->sys_lock);
    if(r!=-1 && c!=-1)
    {
	char buff[256];
	sprintf(buff,"[%d,%d]",r,c);
	arc_model_assign_property_str(k_b->model,"Analyzer:ExitFound:RESULT",buff);
    }
    else
    {
	arc_model_assign_property_str(k_b->model,"Analyzer:ExitFound:RESULT","F");
    }
}

static void explore_run(struct task *t)
{
    struct knowledge_base *k_b =  task_get_ctx(t);
    

    int r=-1, c=-1;
    pthread_mutex_lock(&k_b->model->sys_lock);
    struct e_map *the_map = knowledge_base_get_res(k_b,"The Maze");
    struct e_map *visited = knowledge_base_get_res(k_b,"Visited");
    bool explore = e_map_explore(the_map,visited,&r,&c);
    pthread_mutex_unlock(&k_b->model->sys_lock);
    if(r!=-1 && c!=-1)
    {
	char buff[256];
	sprintf(buff,"[%d,%d]",r,c);
	arc_model_assign_property_str(k_b->model,"Analyzer:Explore:RESULT",buff);
    }
    else
    {
	arc_model_assign_property_str(k_b->model,"Analyzer:Explore:RESULT","F");
    }
}

static void snatch_treasure_run(struct task *t)
{
    struct knowledge_base *k_b =  task_get_ctx(t);
    struct arc_model *a_m = k_b->model;
    struct arc_value found_treasure = arc_model_get_property(a_m,"Analyzer:TreasureFound:RESULT");
    //TODO: Check I'm close
    if(found_treasure.type!= V_INVALID && strcmp(found_treasure.v_str,"Analyzer:TreasureFound:RESULT")!=0)
    {
	arc_model_assign_property_dbl(k_b->model, "Analyzer:HaveTreasure:RESULT",1);
	printf("SNATCH SUCCESSFUL!\n");
    }
    else
    {
	arc_model_assign_property_dbl(k_b->model, "Analyzer:HaveTreasure:RESULT",0);
    }
}

static void move_to_cell_run(struct task *t)
{
   
}

static void small_brain_ctrl(struct task *t)
{

    bool done = false;
    struct small_brain_ctx *r_m = task_get_cb_ctx(t);
    struct arc_model *a_m = r_m->a->az->mape_k->k_b.model;
    
    if(t == r_m->t_found_treasure)
    {
	
	struct arc_value found_treasure_res = arc_model_get_property(a_m,"Analyzer:TreasureFound:RESULT");
	
	printf("TreasureFound:%s\n",found_treasure_res.v_str);
	if(found_treasure_res.type == V_STRING && strcmp(found_treasure_res.v_str,"F")==0)
	{
	    printf("Have not found treasure yet, exploring for it... \n");
	    r_m->t_explore = task_engine_create_task(explore_run, small_brain_ctrl, &r_m->a->az->mape_k->k_b, r_m);
	    task_engine_add_task(r_m->t_explore,0);    
	}
	else if(found_treasure_res.type == V_STRING && strcmp(found_treasure_res.v_str,"F")!=0)
	{
	    printf("Treasure found, snatching it!\n");
	    r_m->t_snatch_treasure = task_engine_create_task(snatch_treasure_run, small_brain_ctrl, &r_m->a->az->mape_k->k_b, r_m);
	    task_engine_add_task(r_m->t_snatch_treasure,0);    
	    //done = true;
	}
    }
    else if(t == r_m->t_found_exit)
    {
	struct arc_value found_exit_res = arc_model_get_property(a_m,"Analyzer:ExitFound:RESULT");
	if(found_exit_res.type == V_STRING && strcmp(found_exit_res.v_str,"F")==0)
	{
	    printf("Have not found exit yet, exploring for it... \n");
	    r_m->t_explore = task_engine_create_task(explore_run, small_brain_ctrl, &r_m->a->az->mape_k->k_b, r_m);
	    task_engine_add_task(r_m->t_explore,0);    
	}
	else
	{
	    done = true;
	}
    }
    else if(t == r_m->t_explore)
    {
	struct arc_value explore_res = arc_model_get_property(a_m,"Analyzer:Explore:RESULT");
	//printf("Exploration Target:%s\n",explore_res.value.v_str);
	arc_model_assign_property_str(a_m,"Analyzer:AdaptationGoal:RESULT",explore_res.v_str);
	done = true;
    }
    else if(t == r_m->t_snatch_treasure)
    {
	arc_model_assign_property_str(a_m,"Analyzer:AdaptationGoal:RESULT","S");
	printf("Happy rat brain!\n");
	done = true;
    }
    
    
    
    
    //garbage collect task
    task_engine_free_task(t);
    if(done)
    {
	analysis_set_status(r_m->a, ANALYSIS_DONE);
	r_m->a->done(r_m->a);
	free(r_m);
    }
    
}






