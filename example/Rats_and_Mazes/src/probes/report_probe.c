#define _XOPEN_SOURCE 500

#include<stdio.h>
#include<string.h>	
#include<stdlib.h>	
#include<sys/socket.h>
#include<arpa/inet.h>	
#include<unistd.h>	
#include<pthread.h> 

#include "probe.h"
#include "report_probe.h"
#include "knowledge_base.h"
#include "tokenize.h"
#include "ini_parser.h"
#include "ini.h"
#include "string_buffer.h"
#include "arc_plan.h"
#include "plan_parser.h"

#include "tasks.h"

void report_probe_init(struct probe *probe)
{
    struct report_probe *r_p = probe->ctx;
    r_p->num_done=0;
    r_p->conn_init = false;
    printf("Initialized: %p\n",probe);
}


char *ini_to_prop_update(struct ini *ini, char *sys_name)
{
    char *backing_buff = malloc(1024*1024);
    struct string_buffer s_b;
    string_buffer_init(&s_b, backing_buff, 1024);
    
    for(size_t i=0;i<ini->num_global_props;i++)
    {
	struct ini_property *i_p = &ini->global_props[i];
	
	if(ini->global_props[i].p_type == INI_P_STR)
	    string_buffer_add_fmt(&s_b,"%s [ %s ] = \"%s\" { }\n",
				  sys_name, i_p->key, i_p->value.str);
	else
	    string_buffer_add_fmt(&s_b,"%s [ %s ] = %s { }\n",
				  sys_name, i_p->key, i_p->value.num);
    }

    for(size_t i=0;i<ini->num_sections;i++)
    {
	
	struct ini_section *i_s = &ini->sections[i];
	
	for( size_t j=0;j<i_s->num_properties;j++)
	{
	    struct ini_property *i_p = &i_s->properties[j];
	    if(i_s->properties[j].p_type == INI_P_STR)
	    {
		string_buffer_add_fmt(&s_b,"%s.%s [ %s ] = \"%s\" { }\n",
				      sys_name,i_s->name,
				      i_p->key,i_p->value.str);
	    }
	    else
	    {
		string_buffer_add_fmt(&s_b,"%s.%s [ %s ] = %s { }\n",
				      sys_name,i_s->name,
				      i_p->key,i_p->value.str);
	    }
	}

	for(size_t j=0;j<i_s->num_subsections;j++)
	{
	    struct ini_subsection *i_ss = &i_s->subsections[j];
	    printf("[%s]\n",i_ss->name);

	    for(size_t k=0;k<i_ss->num_properties;k++)
	    {
		struct ini_property *i_p = &i_ss->properties[k];
		
		if(i_ss->properties[k].p_type == INI_P_STR)
		{
		    string_buffer_add_fmt(&s_b,"%s.%s.%s [ %s ] = \"%s\" { }\n",
					  sys_name,i_s->name,
					  i_ss->name, i_p->key, i_p->value.str);
		}
		else
		{
		    string_buffer_add_fmt(&s_b,
					  "%s.%s.%s [ %s ] = %s { }\n",
					  sys_name,i_s->name,
					  i_ss->name, i_p->key, i_p->value.str);
		}
	    }
	}
    }
    
    /*
      quite happy with the string buffer now,
      don't need to do anything for memory management
      other than handle the backing buffer
    */
    return backing_buff;
}

void conn_recv_run(struct task *task)
{

    struct report_probe *r_p = task_get_ctx(task);
    struct probe *probe = task_get_cb_ctx(task);
    if(!r_p->conn_init)
    {
	printf("R_P Initializing connection... \n");
	bool res = connection_init(&r_p->listen, CONN_T_UNIX, r_p->sock_file);
	printf("Listening... \n");
	printf("Listenning on: %s:%d\n",r_p->sock_file,connection_listen(&r_p->listen));
	r_p->conn_init = true;
    }
    
    struct knowledge_base *k_b = r_p->k_b;
    struct arc_model *a_m = k_b->model;
    char buff[513];
    int nbytes = 0;
    
    //printf("Waiting to receive...\n");
    nbytes = connection_recv(&r_p->listen, buff, 512);
    buff[nbytes]='\0';
    
    
    //printf("nbytes:%d\n",nbytes);
    //printf("Buff: %s\n",buff);
    if(nbytes > 0)
    {
	
	//printf("Received: \n<\n%s>\n",buff);
	struct ini properties_ini;
	ini_init(&properties_ini);
	ini_parse(buff, &properties_ini);
	
	char *prop_updates = ini_to_prop_update(&properties_ini,a_m->sys->sys_name);
	//printf("Prop Updates: %s\n", prop_updates);
	struct token_stream t_s;
	token_stream_init(&t_s);
	
	struct arc_plan plan;
	
	if(build_token_stream(prop_updates, &t_s))
	{
	    plan_parse(&t_s,&plan,"Property Update",5);
	}
	pthread_mutex_lock(&a_m->sys_lock);
	
	if(plan_execute(&plan,a_m->sys)==PLAN_OK)
	{
	    
	}
	else
	{
	    fprintf(stderr,"plan failed:%s!\n",buff);
	}
	pthread_mutex_unlock(&a_m->sys_lock);
	
	ini_destroy(&properties_ini);
	token_stream_destroy(&t_s);
	free(prop_updates);
	cleanup_plan(&plan);
	//plan_destroy(&plan);
    }
    else if(nbytes == 0)
    {
	fprintf(stderr,"Received 0 bytes, closing connection\n");
    }
    else
    {
	fprintf(stderr,"PANIC recv returned: %d", nbytes);
	exit(-1);
	//connection_destroy(&r_p->listen);
	
    }
    
}


void *report_probe_run(struct probe *probe)
{
    //figure out who should know about the kb, it's probably the probe itself since we assume it updates it.
    struct report_probe *r_p = probe->ctx;
    struct knowledge_base *k_b = r_p->k_b;
    
    struct task *t_conn_recv = task_engine_create_task(conn_recv_run,
						       report_probe_control,r_p,probe);
    struct task *t_map_update = task_engine_create_task(map_update_run,
							report_probe_control,k_b,probe);
    
    task_engine_add_task(t_conn_recv,0);
    uint32_t t_conn_recv_id = task_get_id(t_conn_recv);
    task_engine_add_task(t_map_update,1, t_conn_recv_id);
    
}

void report_probe_destroy(struct probe *probe)
{
    pthread_mutex_lock(&probe->lock);
    struct report_probe *r_p = probe->ctx;
    connection_destroy(&r_p->listen);
    pthread_mutex_unlock(&probe->lock);
}

void report_probe_control(struct task *t)
{
    struct probe *probe = task_get_cb_ctx(t);
    struct report_probe *r_p = probe->ctx;


    task_engine_free_task(t);
    
    pthread_mutex_lock(&probe->lock);
    r_p->num_done++;


    
    if(r_p->num_done >= 2)
    {
	//set the probe status, note, we're already holding the lock
	probe->status = PROBE_DONE;
	/*
	  need unlock the probe before calling the done function
	  so that it can check  the probe status
	*/
	//reset
	r_p->num_done = 0;
	pthread_mutex_unlock(&probe->lock);
	probe->probe_done(probe);
	
    }
    else
    {
	pthread_mutex_unlock(&probe->lock);
    }

    //TODO: cleanup task 
    
}
