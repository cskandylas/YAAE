#include<sys/socket.h>
#include<arpa/inet.h>	
#include<unistd.h>	
#include<pthread.h> 

#include "connection.h"
#include "knowledge_base.h"

#ifndef REPORT_PROBE_H
#define REPORT_PROBE_H

struct probe;

struct report_probe
{
    char *sock_file;
    struct connection listen;
    struct knowledge_base *k_b;
    size_t num_done;
    bool conn_init;
};


void report_probe_init(struct probe *probe);
void *report_probe_run(struct probe *probe);
void report_probe_destroy(struct probe *probe);
void report_probe_control(struct task *t);


#endif
