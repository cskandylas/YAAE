#include "execution_command.h"

#ifndef INI_CMD_H
#define INI_CMD_H


struct ini_cmd
{
    struct connection *conn;
    char *conn_args;
    char *cmd;
};

void ini_cmd_run(struct exec_cmd *cmd);



#endif
