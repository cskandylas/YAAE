#include "ini_cmd.h"
#include "connection.h"
#include "ini_parser.h"
#include "ini.h"

#include <string.h>
#include <stdio.h>

#include "arc_model.h"
#include "executor.h"
#include "mape_k.h"

void ini_cmd_run(struct exec_cmd *cmd)
{

    
    struct ini_cmd *ini_cmd = cmd_get_ctx(cmd);
    printf("EC Initializing connection... \n");
    if(!ini_cmd->conn)
    {
	ini_cmd->conn = malloc(sizeof(struct connection));
	bool res = connection_init(ini_cmd->conn, CONN_T_UNIX, ini_cmd->conn_args);
	printf("Connection initialized!%d\n",res);
	printf("Listenning on: %s:%d\n",ini_cmd->conn_args,connection_listen(ini_cmd->conn));
    }
    
    
    
    
    struct executor *ex = cmd->exec;
    
    struct arc_model *a_m = ex->mape_k->k_b.model;
    
    char *path = arc_model_get_property(a_m,"Planner:Path:RESULT").v_str;
    //printf("Sending:%s\n",path);
    connection_send(ini_cmd->conn,path, strlen(path));
    //connection_destroy(ini_cmd->conn);
    cmd_set_status(cmd,CMD_DONE);
    cmd->done(cmd);
    /*
    struct ini_cmd *ini_cmd = cmd_get_ctx(cmd);
    struct connection *conn =  ini_cmd->conn;
    char *cmd = ini_cmd->cmd;
    //printf("Command context: %s\n",(char*)cmd->cmd_ctx);
    if(cmd->ctx)
    {
	
	//send and wait for reply
	connection_send(cmd->conn,cmd->ctx,strlen(cmd->ctx));
	char reply[256];
	connection_recv(cmd->conn,reply,256);
	//printf("Received: [%s]\n", reply);
	struct ini reply_ini;
	ini_init(&reply_ini);
	
	
	if(ini_parse(reply, &reply_ini))
	{
	    //find status in reply
	    
	    struct ini_section *status = ini_find_section(&reply_ini, "command");
	    if(status)
	    {
		//fprintf(stderr,"Status OK\n");
		cmd_set_status(cmd, CMD_DONE);
	    }
	    else
	    {
		fprintf(stderr,"Could not find command section\n");
	    }
	}
	else
	{
	    fprintf(stderr,"Failed to parse ini input?!\n");
	}
	
	
    }
    */
}
