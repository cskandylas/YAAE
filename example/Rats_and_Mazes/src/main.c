#define _XOPEN_SOURCE 600
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdbool.h>
#include <string.h>

#include "view.h"
#include "arc_system.h"
#include "arc_parse.h"
#include "file_utils.h"
#include "plan_parser.h"
#include "tokenize.h"
#include "probe.h"
#include "report_probe.h"
#include "thread_pool.h"
#include "task.h"
#include "ini_parser.h"
#include "ini.h"
#include "knowledge_base.h"
#include "e_map.h"
#include "named_resource.h"
#include "executor.h"

#include "rbtree.h"

#include "tasks.h"
#include "ini_cmd.h"
#include "mape_k.h"
#include "arc_export.h"

#include "task.h"
#include "adaptation_control.h"
#include "arc_model.h"
#include "small_brain.h"

#include "planner.h"
#include "plan.h"
#include "path_generator.h"

#define LINE_LEN 128
#define MAX_VIEW_FLAGS 8

void cleanup_sys(struct arc_system *sys)
{
    
    if(sys)
    {
	for(int i=0;i<sys->num_components;i++)
	{
	    if(sys->components[i])
	    {
		
		for(int j=0;j<sys->components[i]->num_interfaces;j++)
		{
		    if(sys->components[i]->interfaces[j])
		    {
			for(int k=0;k<sys->components[i]->interfaces[j]->num_properties;k++)
			{
			    if(sys->components[i]->interfaces[j]->properties[k])
			    {
				destroy_property(sys->components[i]->interfaces[j]->properties[k]);
				free(sys->components[i]->interfaces[j]->properties[k]);
			    }
			}
			destroy_interface(sys->components[i]->interfaces[j]);
			free(sys->components[i]->interfaces[j]);
		    }
		}

		for(int j=0;j<sys->components[i]->num_properties;j++)
		{
		    if(sys->components[i]->properties[j])
		    {
			destroy_property(sys->components[i]->properties[j]);
			free(sys->components[i]->properties[j]);
		    }
		}
		    
		destroy_component(sys->components[i]);
		free(sys->components[i]);
	    }
	}

	for(int i=0;i<sys->num_invocations;i++)
	{
	    if(sys->invocations[i])
		free(sys->invocations[i]);
	}

	for(int i=0;i<sys->num_properties;i++)
	{
	    if(sys->properties[i])
	    {
		destroy_property(sys->properties[i]);
		free(sys->properties[i]);
	    }
	}
    
	destroy_system(sys);
	free(sys);
    }
}

void fork_draw(char *filename)
{
    pid_t pid = fork();
    
    if(pid==0)
    {
	execl ("/usr/bin/xdot", "xdot", filename, (char *)0);
    }
    else
    {
	return;
    }
}

bool always_false(struct knowledge_base *k_b)
{
    return false;
}

bool seek_treasure(struct knowledge_base *k_b)
{

    printf("Are we done here?\n");
    struct arc_model *a_m = k_b->model;
    struct arc_value have_treasure = arc_model_get_property(a_m,"Analyzer:HaveTreasure:RESULT");
    if(have_treasure.type == V_DOUBLE)
    {
	return false;
    }
    else
    {
	
    }
    return true;
}

void *always_null(struct knowledge_base *k_b)
{
    struct arc_model *a_m = k_b->model;
    struct arc_value target_cell = arc_model_get_property(a_m,"Analyzer:AdaptationGoal:RESULT");
    if(target_cell.type == V_STRING && strcmp(target_cell.v_str,"N")!=0)
    {
	return k_b;
    }
    else
    {
	printf("No good cell is found, returning to monitoring\n");
	return NULL;
    }
}

 int main(int argc, char *argv[])
 {

     if(argc < 3)
     {
	 fprintf(stderr, "YAAE <architecture file> <dot file> \n");
	 exit(1);
     }


     task_engine_init(16,1024+256,1024);

     char *arch_file = argv[1];
     char *dot_file = argv[2];

     char *text = read_file(arch_file);
     struct arc_system *sys=arc_parse(text);
     free(text);

     if(!sys)
     {
	 fprintf(stderr,"Could not parse architecture file, exiting\n");
	 exit(-1);
     }


     struct arc_model model;
     init_model(&model, sys);

    struct mape_k a_c;
    init_knowledge_base(&a_c.k_b, &model);

    uint8_t v_f= VIEW_FLAG_INTERFACES | VIEW_FLAG_PROPERTIES | VIEW_FLAG_PROP_VALUE;
    if(argc>3)
    {
	v_f = atoi(argv[3]);
    }
    
    bool done=false;
    
    //Let's try without readline first :)
    char line_buff[LINE_LEN];
    struct system_view s_v;
    system_view_init(&s_v,sys,v_f);

    /*
      Let's start with the easy case where we know the map size
      and the rat knows its starting position ;)
    */
    struct e_map maze;
    init_e_map(&maze, 64,64);
    fill_e_map(&maze, '?');

    struct e_map visited;
    init_e_map(&visited, 64,64);
    fill_e_map(&visited, '#');



    // named resources 
    struct named_res maze_res;
    named_res_init(&maze_res, "The Maze", 1, &maze);
    knowledge_base_add_res(&a_c.k_b, &maze_res);

    struct named_res visited_res;
    named_res_init(&visited_res, "Visited", 1, &visited);
    knowledge_base_add_res(&a_c.k_b, &visited_res);

    struct named_res sv_res;
    named_res_init(&sv_res, "System View", 1, &s_v);
    knowledge_base_add_res(&a_c.k_b, &sv_res);

    /*UNCOMMENT HERE*/
    init_monitor(&a_c.m, &a_c,seek_treasure);
    
    struct report_probe maze_p;
    maze_p.sock_file="/var/tmp/maze.sock";
    maze_p.k_b = &a_c.k_b; 
    struct probe r_probe;
    init_probe(&r_probe, report_probe_init,
	       report_probe_run,report_probe_destroy,
	       monitor_control, &maze_p);
    monitor_add_probe(&a_c.m,&r_probe);

    
    struct analysis rat_brain_analysis;
    init_analysis(&rat_brain_analysis, small_brain, analysis_control, &a_c.a);
    init_analyzer(&a_c.a,&a_c,always_null);
    analyzer_add_analysis(&a_c.a, &rat_brain_analysis);

    struct plan path_gen;
    init_plan(&path_gen,path_generate_run, planning_control, &a_c.p);
    init_planner(&a_c.p,&a_c);
    planner_add_plan(&a_c.p,&path_gen);


    

    struct ini_cmd ini_cmd = { .conn = NULL, .conn_args = "/var/tmp/brain.sock", .cmd = NULL};
    
    struct exec_cmd ini_exec_cmd;
    exec_cmd_init(&ini_exec_cmd, ini_cmd_run, execution_control, &ini_cmd);
    executor_init(&a_c.e, &a_c);
    executor_add_command(&a_c.e,&ini_exec_cmd);
    monitor_run(&a_c.m);
    
    
    if(!sys)
    {
	fprintf(stderr,"Could not parse %s\n",argv[1]);
	exit(0);
    }
    printf("Starting interactive console...\n");
    while(!done)
    {
	printf(">");
	//ignore first line
	fgets(line_buff, LINE_LEN,stdin);

	char c=line_buff[0];
	
	if(c == 'd')
	{
	    //shouldn't make a difference and this wastes a few ms but eh.
	    pthread_mutex_lock(&model.sys_lock);
	    system_view_update(&s_v,sys);
	    system_view_to_dot(&s_v,dot_file);
	    pthread_mutex_unlock(&model.sys_lock);
	    
	    fork_draw(dot_file);
	}
	else if( c == 'c')
	{
	    line_buff[strlen(line_buff)-1] = '\0';
	    
	    char *line_to_parse = &line_buff[2];
	    //fprintf(stderr,"line_buff:%s\n",line_buff);
	    //fprintf(stderr,"line_to_parse:%s\n",line_to_parse);
	    
	    struct arc_plan plan;
	    struct token_stream t_s;
	    token_stream_init(&t_s);
	    //parse this as an architectural operation command and apply on sys.
	    if(build_token_stream(line_to_parse, &t_s))
	    {
		if(plan_parse(&t_s,&plan,"None",1))
		{
		    int res=plan_execute(&plan,sys); 
		    if(res)
		    {
			fprintf(stderr,"Failed to execute plan!:%s\n",
				plan_return_status(res));
			
		    }
		    pthread_mutex_lock(&model.sys_lock);
		    system_view_update(&s_v,sys);
		    system_view_to_dot(&s_v,dot_file);
		    pthread_mutex_unlock(&model.sys_lock);
		    plan_action_cleanup(plan.actions[0]);
		    plan_destroy(&plan);
		    
		}
	    }
	    
	}
	else if( c == 'v')
	{
	    line_buff[strlen(line_buff)-1] = '\0';
	    
	    char *line_to_parse = &line_buff[2];

	    //tokenize
	    struct tokenizer view_cmd_tok;
	    /*
	      Do all the tokenization and parsing in one pass,
	      get tokens split by ' ' or '|'  and then just check
	      validity later
	    */
	    
	    tokenizer_init(&view_cmd_tok,line_to_parse," |");
	    
	    char *qcomponent = next_token(&view_cmd_tok);
	    char *view_flags[MAX_VIEW_FLAGS];
	    size_t num_flags = 0;
	    char *flag = NULL;
	    while( (flag = next_token(&view_cmd_tok)) )
	    {
		view_flags[num_flags] = flag;
		num_flags++;
	    }

	    // Make sure what we got is valid
	    
	    //unqualify:
	    char *comp_name = strchr(qcomponent,'.') + 1;

	    //make sure the component is in the system

	    struct arc_component *comp = sys_find_component(sys,comp_name);

	    if(!comp)
	    {
		fprintf(stderr,"%s is not a valid component,",comp_name);
		fprintf(stderr,"skipping the command without executing\n");
		continue;
	    }
	    
	    int sum = 0;
	    for(int i=0;i<num_flags;i++)
	    {
		
		int flag_num = -1;
		if(strcmp(view_flags[i], "NAME_ONLY")==0)    
		{
		    flag_num = 0;
		}
		else if(strcmp(view_flags[i], "INTERFACES")==0)
		{
		    flag_num = 1;
		}
		else if(strcmp(view_flags[i], "PROPERTIES")==0)
		{
		    flag_num = 2;
		}
		else if(strcmp(view_flags[i], "PROP_VALUE")==0)
		{
		    flag_num = 4;
		}
		else
		{
		    fprintf(stderr,"Could not recognize view flag %s ",
			    view_flags[i]);
		    fprintf(stderr,"skipping command without executing\n");
		    continue;
		}
		sum+=flag_num;	
	    }
	    
	    struct component_view *c_v = component_view_for(&s_v,comp->comp_name);
	    if(!c_v)
	    {
		fprintf(stderr,"Could not find component view for: %s",
			comp->comp_name);
		fprintf(stderr,"skipping the command without executing\n");
		continue;
	    }

	    
	    c_v->view_flags = sum;
	    pthread_mutex_lock(&model.sys_lock);
	    system_view_update(&s_v,sys);
	    system_view_to_dot(&s_v,dot_file);
	    pthread_mutex_unlock(&model.sys_lock);
	    
	}
	else if( c == 'e')
	{
	    pthread_mutex_lock(&model.sys_lock);
	    char buffer[1024*1024];
	    struct string_buffer sys_s_b;
	    string_buffer_init(&sys_s_b,buffer,1024*1024);
	    arc_dump_system(&sys_s_b,sys);
	    pthread_mutex_unlock(&model.sys_lock);
	    printf("%s\n",sys_s_b.b_buff);
	}

	else if( c == 'q')
	{
	    done=true;
	}
	else
	{
	    printf("Could not parse command %s\n", line_buff);
	}
    }

    //TODO: Wait for stuff to be done.
    //usleep(1000000);
    //cleanup
    pthread_mutex_lock(&a_c.e.cmds[0]->lock);
    if(ini_cmd.conn)
    {
	connection_destroy(ini_cmd.conn);
	free(ini_cmd.conn);
    }
    pthread_mutex_unlock(&a_c.e.cmds[0]->lock);
    pthread_mutex_lock(&a_c.k_b.model->sys_lock);
    destroy_e_map(&maze);
    destroy_e_map(&visited);
    pthread_mutex_unlock(&a_c.k_b.model->sys_lock);
    
    monitor_stop_probe(&a_c.m,a_c.m.probes[0]);
    
    system_view_destroy(&s_v);
    cleanup_sys(a_c.k_b.model->sys);
    //thread_pool_destroy(&t_p);
    //platform_destroy(&p);
    destroy_monitor(&a_c.m);
    destroy_analyzer(&a_c.a);
    destroy_planner(&a_c.p);
    executor_destroy(&a_c.e);
    destroy_knowledge_base(&a_c.k_b);    
    task_engine_destroy();
    
    return 0;
}
