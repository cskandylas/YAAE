#include "dijkstra_map.h"

#include <stdlib.h>
#include <stdint.h>
#include <limits.h>
#include <stdio.h>


struct dijkstra_cell
{
    int r;
    int c;
    int d;
    struct dijkstra_cell *next;
};

struct dijkstra_cell_stack
{
    struct dijkstra_cell *head, *tail;
    uint32_t size;
};


static void dijkstra_cell_stack_init(struct dijkstra_cell_stack *dc_s)
{
    dc_s->head = dc_s->tail = NULL;
    dc_s->size = 0;
}

static void dijkstra_cell_stack_push_back(struct dijkstra_cell_stack *dc_s, int r, int c, int d)
{
    struct dijkstra_cell *cell = malloc(sizeof(struct dijkstra_cell));
    cell->r=r;
    cell->c=c;
    cell->d=d;
    cell->next = NULL;
    
    
    if(!dc_s->head)
    {
	dc_s->head = dc_s->tail = cell;
    }
    else
    {
	dc_s->tail->next = cell;
	dc_s->tail = cell;
    }
    dc_s->size++;
}

static struct dijkstra_cell *dijkstra_cell_stack_pop(struct dijkstra_cell_stack *dc_s)
{
    struct dijkstra_cell *top = NULL;

    if(dc_s->head)
    {
	top = dc_s->head;
	dc_s->head = dc_s->head->next;
	dc_s->size--;
    }

    return top;
}



static void dijkstra_map_create(struct dijkstra_map *map, int width, int height)
{
    map->width=width;
    map->height=height;
    map->distances=malloc(map->height*sizeof(int*));
    for(int r=0;r<map->height;r++)
    {
	map->distances[r]=malloc(map->width*sizeof(int));
    }

    for(int r=0;r<map->height;r++)
    {
	for(int c=0;c<map->width;c++)
	{
	    map->distances[r][c] = INT_MAX;
	}
    }
    
}

void destroy_dijkstra_map(struct dijkstra_map *d_map)
{
    for(int r=0;r<d_map->height;r++)
    {
	free(d_map->distances[r]);
    }

    free(d_map->distances);

}

static void dijkstra_cell_stack_add_adj(struct dijkstra_cell_stack *dc_s, struct dijkstra_cell *d_c,
					struct e_map *e_map ,struct dijkstra_map *d_map)
{
    int r = d_c->r;
    int c = d_c->c;
    int n_d = d_c->d + 1; 
    int l_c = d_c->c-1;
    int r_c = d_c->c+1;
    int u_r = d_c->r-1;
    int d_r = d_c->r+1;

    if(l_c >= 0 && e_map->tiles[r][l_c] == '.' && d_map->distances[r][l_c] == INT_MAX)
    {
	dijkstra_cell_stack_push_back(dc_s, r, l_c, n_d);
    }

    if(r_c < e_map->width && e_map->tiles[r][r_c] == '.' && d_map->distances[r][r_c] == INT_MAX)
    {
	dijkstra_cell_stack_push_back(dc_s, r, r_c, n_d);
    }

    if(u_r >= 0 && e_map->tiles[u_r][c] == '.' && d_map->distances[u_r][c] == INT_MAX)
    {
	dijkstra_cell_stack_push_back(dc_s, u_r, c, n_d);
    }

    if(d_r < e_map->height && e_map->tiles[d_r][c] == '.' && d_map->distances[d_r][c] == INT_MAX)
    {
	dijkstra_cell_stack_push_back(dc_s, d_r, c, n_d);
    }
}



void build_dijkstra_map(struct dijkstra_map *d_map, struct e_map *e_map, int s_r, int s_c)
{
    
    dijkstra_map_create(d_map, e_map->width, e_map->height);
    

    struct dijkstra_cell_stack dc_s;
    dijkstra_cell_stack_init(&dc_s);

    dijkstra_cell_stack_push_back(&dc_s, s_r, s_c, 0);
    
    while(dc_s.size > 0)
    {
	struct dijkstra_cell *d_c = dijkstra_cell_stack_pop(&dc_s);
	d_map->distances[d_c->r][d_c->c] = d_c->d;
	dijkstra_cell_stack_add_adj(&dc_s, d_c, e_map, d_map);
	free(d_c);
    }
    
}

void print_dijkstra_map(struct dijkstra_map *d_map, struct e_map *e_map)
{
    for(int r=0;r<d_map->height;r++)
    {
	for(int c=0;c<d_map->width;c++)
	{
	    if(d_map->distances[r][c] != INT_MAX)
	    {
		if(d_map->distances[r][c] < 10)
		{
		    printf("%d",d_map->distances[r][c]);
		}
		else if(d_map->distances[r][c] < 36)
		{
		    printf("%c",'A'+d_map->distances[r][c]-10);
		}
		else if(d_map->distances[r][c] < 62)
		{
		    printf("%c",'a'+d_map->distances[r][c]-36);
		}
		else if(d_map->distances[r][c] < 76)
		{
		    if('!'+d_map->distances[r][c] - 62 != '#')
		    {
			printf("%c",'!'+d_map->distances[r][c] - 62);
		    }
		    else
		    {
			printf("|");
		    }
		}
		else
		{
		    printf("~");
		}
	    }
	    else
	    {
		char ch = e_map->tiles[r][c];
		if(ch == '?')
		{
		    printf(" ");
		}
		else
		{
		    printf("%c",ch);
		}
	    }
	}
	printf("\n");
    }    
}
