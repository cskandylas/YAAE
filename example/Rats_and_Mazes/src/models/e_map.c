#include "e_map.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "file_utils.h"
#include "tokenize.h"


void init_e_map(struct e_map *map, int width, int height)
{
    map->width=width;
    map->height=height;
    map->tiles=malloc(map->height*sizeof(char*));
    for(int r=0;r<map->height;r++)
    {
	map->tiles[r]=malloc(map->width);
    }
}

void destroy_e_map(struct e_map *map)
{
    for(int r=0;r<map->height;r++)
    {
	free(map->tiles[r]);
    }

    free(map->tiles);
}

void fill_e_map(struct e_map *map, char f)
{
    for(int r=0;r<map->height;r++)
    {
	for(int c=0;c<map->width;c++)
	{
	    map->tiles[r][c] = f;
	}
    }
}


void print_e_map_unmod(struct e_map *map)
{
    printf("+");
    for(int r=0;r<map->height;r++)
    {
	printf("%c",'-');
    }
    printf("+");
    printf("\n");
    for(int r=0;r<map->height;r++)
    {
	printf("|");
	for(int c=0;c<map->width;c++)
	{
	    if(map->tiles[r][c]>=0)
		printf("%c",map->tiles[r][c]);
	    else
		printf("%c",map->tiles[r][c]);
	}
	
	printf("|\n");
    }
    printf("+");
    for(int r=0;r<map->height;r++)
    {
	printf("%c",'-');
    }
    printf("+");
    printf("\n");
}


void print_e_map(struct e_map *map)
{
    for(int r=0;r<map->height;r++)
    {
	for(int c=0;c<map->width;c++)
	{
	    if(map->tiles[r][c]>=0)
		printf("%c",'a'+map->tiles[r][c]);
	    else
		printf("%c",'Z'+map->tiles[r][c]);
	}
	printf("\n");
    }
}


void write_e_map_to_file(struct e_map *map, char *fname)
{

    FILE *fp = fopen(fname, "w");
    fprintf(fp,"%d,%d\n",map->width,map->height);
    for(int r=0;r<map->height;r++)
    {
	for(int c=0;c<map->width;c++)
	{
	    if(map->tiles[r][c]>=0)
		fprintf(fp,"%c",map->tiles[r][c]);
	    else
		fprintf(fp,"%c",map->tiles[r][c]);
	}
	fprintf(fp,"\n");
    }
    fclose(fp);
}

struct e_map *read_e_map_from_file(char *fname)
{
    char *text = read_file(fname);
    struct e_map *e_map = malloc(sizeof(struct e_map));
    
    struct tokenizer l_tok;
    tokenizer_init(&l_tok,text,"\n");


    char *first_line = next_token(&l_tok);
    int width = atoi(first_line);
    char *height_part= strchr(first_line,',')+1;
    int height=atoi(height_part);

    init_e_map(e_map,width,height);
    
    //printf("Dims: [%d,%d]\n",width,height);
    
    free(first_line);
    char *line = "" ;
    int curr_line=0;
    while(line)
    {
	line = next_token(&l_tok);

	if(line)
	{
	    for(int c=0;c<width;c++)
	    {
		e_map->tiles[curr_line][c] = line[c];
	    }
	    curr_line++;
	    free(line);
	}
    }
    
    free(text);

    return e_map;
}

struct e_map *make_copy(struct e_map *map)
{
    struct e_map *copy=malloc(sizeof(struct e_map));
    copy->width=map->width;
    copy->height=map->height;
    copy->tiles=malloc(sizeof(char *)*copy->height);
    for(int r=0;r<copy->height;r++)
    {
	copy->tiles[r]=malloc(copy->width);
    }

    for(int r=0;r<map->height;r++)
	for(int c=0;c<map->width;c++)
	    copy->tiles[r][c]=map->tiles[r][c];
    return copy;
}


bool e_map_find_tile(struct e_map *map, char tile, int *row, int *col)
{
    for(int r=0;r<map->height;r++)
    {
	for(int c=0;c<map->width;c++)
	{
	    if(map->tiles[r][c] == tile)
	    {
		*row = r;
		*col = c;
		return true;
	    }
	}
    }
    return false;
}

bool e_map_explore(struct e_map *map, struct e_map *visited, int *row, int *col)
{

//    print_e_map_unmod(map);
//    print_e_map_unmod(visited);
    
    //Find an unvisited cell that is pathable
    for(int r=0;r<map->height;r++)
    {
	for(int c=0;c<map->width;c++)
	{
	    if(map->tiles[r][c] == '.' && visited->tiles[r][c] == '#')
	    {
		//printf("Pathable and unvisited:%d,%d\n",r,c);
		
		map->tiles[r][c] = 'X';
		visited->tiles[r][c]='X';
		//print_e_map_unmod(map);
		//print_e_map_unmod(visited);
		
		map->tiles[r][c] = '.';
		visited->tiles[r][c]='#';
		*row = r;
		*col = c;
		return true;
	    }
	}
    }
    return false;
}
