#include <stdbool.h>
#ifndef E_MAP_H
#define E_MAP_H

struct e_map
{
    char **tiles;
    int width;
    int height;
};


void init_e_map(struct e_map *map, int width, int height);
void destroy_e_map(struct e_map *map);
void fill_e_map(struct e_map *map, char f);
void print_e_map(struct e_map *map);
void print_e_map_unmod(struct e_map *map);
void write_e_map_to_file(struct e_map *map, char *fname);
struct e_map *read_e_map_from_file(char *fname);
struct e_map *make_copy(struct e_map *map);

bool e_map_find_tile(struct e_map *map, char tile, int *row, int *col);
bool e_map_explore(struct e_map *map, struct e_map *visited, int *row, int *col);

#endif
