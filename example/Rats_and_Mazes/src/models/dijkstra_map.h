#include "e_map.h"

#ifndef DIJSKTRA_MAP_H
#define DIJSKTRA_MAP_H


struct dijkstra_map
{
    int **distances;
    int width;
    int height;
};

void build_dijkstra_map(struct dijkstra_map *d_map, struct e_map *e_map, int s_x, int s_y);
void destroy_dijkstra_map(struct dijkstra_map *d_map);
void print_dijkstra_map(struct dijkstra_map *d_map, struct e_map *e_map);
#endif

