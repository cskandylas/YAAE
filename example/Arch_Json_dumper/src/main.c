

#define _XOPEN_SOURCE 600
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>


#include "arc_component.h"
#include "arc_system.h"
#include "arc_model.h"
#include "arc_parse.h"
#include "file_utils.h"
#include "string_buffer.h"
#include "arc_json_export.h"
#include "arc_parse.h"


#define LINE_LEN 128
#define MAX_VIEW_FLAGS 8

void cleanup_sys(struct arc_system *sys)
{
    
    if(sys)
    {
	for(int i=0;i<sys->num_components;i++)
	{
	    if(sys->components[i])
	    {
		
		for(int j=0;j<sys->components[i]->num_interfaces;j++)
		{
		    if(sys->components[i]->interfaces[j])
		    {
			for(int k=0;k<sys->components[i]->interfaces[j]->num_properties;k++)
			{
			    if(sys->components[i]->interfaces[j]->properties[k])
			    {
				destroy_property(sys->components[i]->interfaces[j]->properties[k]);
				free(sys->components[i]->interfaces[j]->properties[k]);
			    }
			}
			destroy_interface(sys->components[i]->interfaces[j]);
			free(sys->components[i]->interfaces[j]);
		    }
		}

		for(int j=0;j<sys->components[i]->num_properties;j++)
		{
		    if(sys->components[i]->properties[j])
		    {
			destroy_property(sys->components[i]->properties[j]);
			free(sys->components[i]->properties[j]);
		    }
		}
		    
		destroy_component(sys->components[i]);
		free(sys->components[i]);
	    }
	}

	for(int i=0;i<sys->num_invocations;i++)
	{
	    if(sys->invocations[i])
		free(sys->invocations[i]);
	}

	for(int i=0;i<sys->num_properties;i++)
	{
	    if(sys->properties[i])
	    {
		destroy_property(sys->properties[i]);
		free(sys->properties[i]);
	    }
	}
    
	destroy_system(sys);
	free(sys);
    }
}


 int main(int argc, char *argv[])
 {

     if(argc < 2)
     {
	 fprintf(stderr, "YAAE <architecture file>\n");
	 exit(1);
     }


     

     char *arch_file = argv[1];
     char *dot_file = argv[2];

     char *text = read_file(arch_file);
     struct arc_system *sys=arc_parse(text);
     free(text);

     if(!sys)
     {
	 fprintf(stderr,"Could not parse architecture file, exiting\n");
	 exit(-1);
     }


     //struct arc_model model;
     //init_model(&model, sys);
     size_t buff_len = 1024*1024;
     char *big_text_buff = malloc(buff_len);
     struct string_buffer json_str_buff;
     string_buffer_init(&json_str_buff, big_text_buff,buff_len);
     arc_json_dump_system(&json_str_buff, sys);

     printf("%s\n",big_text_buff);
     
     return 0;
}


