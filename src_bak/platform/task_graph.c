#include "task_graph.h"
#include <stdlib.h>
#include <stdio.h>


void task_dep_node_init(struct task_dep_node *d_n, struct task *t)
{
    d_n->task = t;
    d_n->max_in_edges = INIT_TASK_NODE_EDGES;
    d_n->max_out_edges = INIT_TASK_NODE_EDGES;
    d_n->num_in_edges = 0;
    d_n->num_out_edges = 0;
    d_n->in_edges = malloc(d_n->max_in_edges*sizeof(uint32_t));
    d_n->out_edges = malloc(d_n->max_out_edges*sizeof(uint32_t));
    d_n->next = NULL;
}
//sets the max equal to num and allocates that much memory instead.
void task_dep_node_init_explicit(struct task_dep_node *d_n, struct task *t,
				 size_t max_in_edges, size_t max_out_edges)
{
    d_n->task = t;
    d_n->max_in_edges = max_in_edges;
    d_n->max_out_edges = max_out_edges;
    d_n->num_in_edges = 0;
    d_n->num_out_edges = 0;
    d_n->in_edges = malloc(d_n->max_in_edges*sizeof(uint32_t));
    d_n->out_edges = malloc(d_n->max_out_edges*sizeof(uint32_t));
    d_n->next = NULL;
}

void task_dep_node_add_in(struct task_dep_node *d_n, uint32_t t_id)
{
    if(d_n->num_in_edges >= d_n->max_in_edges)
    {
	//double the size
	d_n->max_in_edges *= 2;
	//realloc the array
	d_n->in_edges = realloc(d_n->in_edges,
				d_n->max_in_edges * sizeof(uint32_t));
	
    }
    //make sure the dependency does not already exist
    for(size_t i=0;i<d_n->num_in_edges;i++)
    {
	if(d_n->in_edges[i] == t_id)
	{
	    return;
	}
    }
    

    d_n->in_edges[d_n->num_in_edges] = t_id;
    d_n->num_in_edges++;
}

void task_dep_node_rem_in(struct task_dep_node *d_n, uint32_t t_id)
{
    for(size_t i = 0; i < d_n->num_in_edges;i++)
    {
	if(d_n->in_edges[i] == t_id)
	{
	    //fip with the last element
	    if(i < d_n->num_in_edges-1)
	    {
		d_n->in_edges[i] = d_n->in_edges[d_n->num_in_edges-1];
	    }
	    d_n->num_in_edges--;
	    return;
	}
    }
}

void task_dep_node_add_out(struct task_dep_node *d_n, uint32_t t_id)
{
    if(d_n->num_out_edges >= d_n->max_out_edges)
    {
	//double the size
	d_n->max_out_edges *= 2;
	//realloc the array
	d_n->out_edges = realloc(d_n->out_edges,
				d_n->max_out_edges * sizeof(uint32_t));
	
    }
    
    //make sure the dependency does not already exist
    for(size_t i=0;i<d_n->num_out_edges;i++)
    {
	if(d_n->out_edges[i] == t_id)
	{
	    return;
	}
    }

    d_n->out_edges[d_n->num_out_edges] = t_id;
    d_n->num_out_edges++;
}
void task_dep_node_rem_out(struct task_dep_node *d_n, uint32_t t_id)
{
    for(size_t i = 0; i < d_n->num_out_edges;i++)
    {
	if(d_n->out_edges[i] == t_id)
	{
	    //fip with the last element
	    if(i < d_n->num_out_edges-1)
	    {
		d_n->out_edges[i] = d_n->out_edges[d_n->num_out_edges-1];
	    }
	    d_n->num_out_edges--;
	    return;
	}
    }
}

void task_dep_node_destroy(struct task_dep_node *d_n)
{
    free(d_n->in_edges);
    free(d_n->out_edges);
}

void task_dep_node_print(struct task_dep_node *d_n)
{
    printf("Task id: %u\n",task_get_id(d_n->task));
    printf("In Edges: [");
    for(size_t i = 0; i<d_n->num_in_edges;i++)
    {
	printf(" %u",d_n->in_edges[i]);
    }
    printf(" ]\nOut Edges: [");
    for(size_t i = 0; i<d_n->num_out_edges;i++)
    {
	printf(" %u",d_n->out_edges[i]);
    }
    printf(" ]\n");
}


//Good in hash functions taken from: https://github.com/skeeto/hash-prospector
static uint32_t triple32inc(uint32_t x)
{
    x++;
    x ^= x >> 17;
    x *= 0xed5ad4bb;
    x ^= x >> 11;
    x *= 0xac4c1b51;
    x ^= x >> 15;
    x *= 0x31848bab;
    x ^= x >> 14;
    return x;
}

static uint32_t triple32inc_r(uint32_t x)
{
    x ^= x >> 14 ^ x >> 28;
    x *= 0x32b21703;
    x ^= x >> 15 ^ x >> 30;
    x *= 0x469e0db1;
    x ^= x >> 11 ^ x >> 22;
    x *= 0x79a85073;
    x ^= x >> 17;
    x--;
    return x;
}


void task_graph_init(struct task_graph *d_g, uint32_t init_size)
{
    d_g->num_cells = init_size;
    d_g->dep_nodes_hash = malloc(d_g->num_cells*sizeof(struct task_dep_node*));
    for(uint32_t i = 0; i<d_g->num_cells;i++)
    {
	d_g->dep_nodes_hash[i] = NULL;
    }
}

void task_graph_add(struct task_graph *d_g, struct task_dep_node *d_n)
{

    uint32_t task_id = task_get_id(d_n->task);
    //hash the task_id
    uint32_t loc = triple32inc(task_id) % d_g->num_cells;
    //add to the head of the list
    if(!d_g->dep_nodes_hash[loc])
    {
	d_g->dep_nodes_hash[loc] = d_n;
    }
    else
    {
	//make sure the element is not already there...
	struct task_dep_node *elem = d_g->dep_nodes_hash[loc];
	
	while(elem)
	{
	    uint32_t elem_tid = task_get_id(elem->task);
	    if(elem_tid == task_id)
	    {
		return;
	    }
	    elem = elem->next;
	}

	
	
	d_n->next = d_g->dep_nodes_hash[loc];
	d_g->dep_nodes_hash[loc] = d_n;
    }
}

void task_graph_rem(struct task_graph *d_g, struct task_dep_node *d_n)
{
    //hash the task_id

    uint32_t task_id = task_get_id(d_n->task);
    
    uint32_t loc = triple32inc(task_id) % d_g->num_cells;

    struct task_dep_node *elem = d_g->dep_nodes_hash[loc];
    struct task_dep_node *prev = NULL;
    while(elem)
    {
	uint32_t elem_tid = task_get_id(elem->task);
	if(elem_tid == task_id)
	{
	    if(prev)
	    {
		prev->next = elem->next;
	    }
	    else
	    {
		d_g->dep_nodes_hash[loc] = elem->next;
	    }
	    task_dep_node_destroy(d_n);
	    free(d_n);
	    return;
	}

	prev = elem;
	elem = elem->next;
    }
    
    
}
void task_graph_destroy(struct task_graph *d_g)
{

    for(uint32_t i=0;i<d_g->num_cells;i++)
    {
	struct task_dep_node *elem = d_g->dep_nodes_hash[i];

	while(elem)
	{
	    struct task_dep_node *to_free = elem;
	    elem = elem->next;
	    task_dep_node_destroy(to_free);
	    free(to_free);
	}
    }
    
    free(d_g->dep_nodes_hash);
}

struct task_dep_node *task_graph_get(struct task_graph *d_g, uint32_t t_id)
{
    uint32_t bucket = triple32inc(t_id) % d_g->num_cells;

    struct task_dep_node *elem = d_g->dep_nodes_hash[bucket];
    while(elem)
    {
	uint32_t elem_tid = task_get_id(elem->task);
	if(elem_tid == t_id)
	{
	    return elem;
	}
	elem = elem->next;
    }


    return NULL;
}


void task_graph_print(struct task_graph *d_g)
{
    for(uint32_t i = 0; i< d_g->num_cells; i++)
    {
	struct task_dep_node *elem = d_g->dep_nodes_hash[i];
	printf("[%u] -> [",i);
	uint32_t elem_tid = task_get_id(elem->task);
	while(elem)
	{
	    printf(" %u",elem_tid);
	    elem = elem->next;
	}
	printf(" ]\n");
	
    }
}


/*
int test_task_dependency_graph(void)
{
    
    size_t num_tasks = 100;
    struct task *tasks = malloc(num_tasks * get_task_struct_size());

    for(size_t i = 0; i < num_tasks;i++)
    {
	struct task *task = &tasks[i];
	
	create_task(&tasks[i],i,NULL,NULL,&tasks[i],NULL);
    }
    
    struct task_graph d_g;
    task_graph_init(&d_g,47);

    struct task_dep_node dep_nodes[num_tasks];
    for(size_t i = 0; i< num_tasks;i++)
    {	
	task_dep_node_init(&dep_nodes[i],&tasks[i]);
	task_graph_add(&d_g, &dep_nodes[i]);
    }

    for(size_t i = 0; i< num_tasks;i++)
    {
	task_dep_node_add_in(&dep_nodes[i],tasks[i/3].task_id);
	task_dep_node_add_in(&dep_nodes[i],tasks[i/4].task_id);
	if(i*3 < 100)
	{
	    task_dep_node_add_out(&dep_nodes[i],tasks[i*3].task_id);
	}

	if(i*4 < 100)
	{
	    task_dep_node_add_out(&dep_nodes[i],tasks[i*4].task_id);
	}
    }

    task_graph_print(&d_g);

    for(size_t i = 0; i< num_tasks;i++)
    {
	task_dep_node_print(&dep_nodes[i]);
    }
    
    return 0;
}
*/
