#include "mem_pool.h"

#include <stdlib.h>
#include <stdint.h>


void mem_pool_init(struct mem_pool *m_p, size_t pool_size, size_t elem_size)
{

    if(elem_size >= sizeof(struct mem_pool_free_node))
    {
	m_p->pool = malloc(pool_size*elem_size);
	m_p->pool_size = pool_size;
	m_p->elem_size = elem_size;
	m_p->head = NULL;
	mem_pool_reset(m_p);
    }
    else
    {
	m_p->pool=NULL;
	m_p->pool_size=0;
	m_p->pool_size=elem_size=0;
	m_p->head=NULL;
    }
	   
}

void *mem_pool_alloc(struct mem_pool *m_p)
{
    //Find the first free space in the pool
    struct mem_pool_free_node *next_free = m_p->head;

    //if there's no space left, return NULL
    if(!next_free)
    {
	return NULL;
    }
    
    m_p->head = m_p->head->next;
    return next_free;
}

void mem_pool_free(struct mem_pool *m_p, void *ptr)
{
    //comparing pointers with <= is undefined behavior
    uintptr_t base = (uintptr_t)m_p->pool;
    uintptr_t end  = (uintptr_t)((unsigned char*)base + m_p->pool_size*m_p->elem_size);
    uintptr_t ptr_i = (uintptr_t) ptr;
    //This is valid C but does it preserve semantics ?
    if(!(base <= ptr_i && ptr_i < end))
    {
	return;
    }

    //freeing is just pointing the head of the free-list to the node
    struct mem_pool_free_node *node = ptr;
    node->next = m_p->head;
    m_p->head = node;
    
}

void mem_pool_reset(struct mem_pool *m_p)
{
    size_t i;
    for(i=0;i < m_p->pool_size; i++)
    {
	//this is a bit ugly but necessary to be stricly C compliant
	void *ptr = (unsigned char*)m_p->pool + i * m_p->elem_size;
	struct mem_pool_free_node *node = ptr;
	node->next = m_p->head;
	m_p->head = node;
    }
}

void mem_pool_destroy(struct mem_pool *m_p)
{
    free(m_p->pool);
}
