#define _XOPEN_SOURCE 600

#include "connection_unix.h"
#include "connection.h"

#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <netdb.h> 
#include <netinet/in.h>
#include <netinet/ip.h> 
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/un.h>




struct unix_context
{
    char *sock_file;
    //char *client_sock_file;
    int fd;
    int client_fd;
    //This possibly should be moved to the connection ;)
    enum CONNECTION_ROLE c_r;
};


bool  unix_init (struct connection *conn, void *args)
{
    char *sock_file = args;
    
    int fd = socket(AF_UNIX, SOCK_STREAM, 0);

    if(fd == -1 )
    {
	fprintf(stderr, "Socket failed%d\n",fd);
	return false;
    }
    
    struct unix_context *unix_ctx = malloc(sizeof(struct unix_context));
    unix_ctx->sock_file = strdup(sock_file);
    unix_ctx->fd = fd;

    conn->ctx = unix_ctx;
    return true;
}

bool  unix_dial(struct connection *conn)
{
    struct unix_context *unix_ctx = conn->ctx;

    struct sockaddr_un addr;
    addr.sun_family = AF_UNIX;
    strcpy(addr.sun_path, unix_ctx->sock_file);
    int len = strlen(addr.sun_path) + sizeof(addr.sun_family);
    int res = connect(unix_ctx->fd, (struct sockaddr *)&addr, len);
	
    if( res == -1 )
    {
	fprintf(stderr, "Connect failed:%d\n",res);
	return false;
    }
    unix_ctx->c_r = CONN_R_DIAL;
    return true;
}

bool  unix_listen(struct connection *conn)
{

    struct unix_context *unix_ctx = conn->ctx;
    struct sockaddr_un addr;
    addr.sun_family = AF_UNIX;
    strcpy(addr.sun_path, unix_ctx->sock_file);
    unlink(addr.sun_path);
    int len = strlen(addr.sun_path) + sizeof(addr.sun_family);
    if (bind(unix_ctx->fd, (struct sockaddr *)&addr, len) == -1) {
	perror("Bind failed\n");
	return false;

    }

    
    //backlog of 1 since this is supposed to be an 1 to 1 conection
    if (listen(unix_ctx->fd, 1) == -1) {
	perror("Listen failed\n");
	return false;
    }


    
    socklen_t slen = sizeof(addr);
    unix_ctx->client_fd = accept(unix_ctx->fd, (struct sockaddr *)&addr, &slen);
    if (unix_ctx->client_fd == -1)
    {
	perror("Accept failed\n");
	return false;
    }
    
    unix_ctx->c_r = CONN_R_LISTEN;

    return true;
}


ssize_t unix_send(struct connection *conn, void *buff, size_t len)
{
    struct unix_context *unix_ctx = conn->ctx;
    if(unix_ctx->c_r == CONN_R_DIAL)
    {
	return send(unix_ctx->fd, buff, len, 0);
    }
    else
    {
	return send(unix_ctx->client_fd, buff, len, 0);
    }
}

ssize_t unix_recv(struct connection *conn, void *buff, size_t len)
{
    struct unix_context *unix_ctx = conn->ctx;
    if(unix_ctx->c_r == CONN_R_DIAL)
    {
	return recv(unix_ctx->fd, buff, len, 0);
    }
    else
    {
	return recv(unix_ctx->client_fd, buff, len, 0);
    }
}

bool  unix_destroy(struct connection *conn, void *args)
{
    struct unix_context *unix_ctx = conn->ctx;
    if(unix_ctx)
    {
	if(unix_ctx->c_r == CONN_R_LISTEN)
	{
	    close(unix_ctx->client_fd);
	}
	//this never runs
	free(unix_ctx->sock_file);
	
	//close returns zero on success so flip it.
	return !close(unix_ctx->fd);
    }
    return true;
}
