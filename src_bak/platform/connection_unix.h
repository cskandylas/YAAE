#include <stdbool.h>
#include <sys/types.h>



#ifndef CONNECTION_UNIX_H
#define CONNECTION_UNIX_H

struct connection;

bool  unix_init (struct connection *conn, void *args);
bool  unix_dial(struct connection *conn);
bool  unix_listen(struct connection *conn);
ssize_t unix_send(struct connection *conn, void *buff, size_t len);
ssize_t unix_recv(struct connection *conn, void *buff, size_t len);
bool  unix_destroy(struct connection *conn, void *args);


#endif
