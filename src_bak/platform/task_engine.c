#include "task_engine.h"

#include<stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>


static struct task_engine *t_e = NULL;

void task_engine_init(size_t num_workers, uint32_t hash_size, uint32_t pool_size)
{
    if(!t_e)
    {
	printf("Initializing task_engine...\n");
	t_e = malloc(sizeof(struct task_engine));
    }
    
    t_e->num_workers = num_workers;
    task_graph_init(&t_e->dep_graph,hash_size);

    mem_pool_init(&t_e->task_pool,pool_size,get_task_struct_size());
    pthread_mutex_init(&t_e->task_pool_lock,NULL);
    t_e->next_task_id = 0;
    
    t_e->workers = malloc(t_e->num_workers * sizeof(struct work_thief));
    
    for(size_t i = 0; i<t_e->num_workers; i++)
    {
	work_thief_init(&t_e->workers[i]);
    }

    pthread_mutex_init(&t_e->lock,NULL);
    
    //Engine Initialization done!
}

struct task *task_engine_create_task(task_run_fn run,task_done_fn done, void *ctx, void *cb_ctx)
{
    struct task *new_task =  NULL;
    pthread_mutex_lock(&t_e->task_pool_lock);
    new_task = mem_pool_alloc(&t_e->task_pool);
	
    if(new_task)
    {
	create_task(new_task,t_e->next_task_id,run,done,ctx,cb_ctx);
	t_e->next_task_id++;
    }
    
    pthread_mutex_unlock(&t_e->task_pool_lock);
    return new_task;
}

void task_engine_free_task(struct task *t)
{
    
    pthread_mutex_lock(&t_e->task_pool_lock);
    uint32_t t_id = task_get_id(t);
    struct task_dep_node *d_n = task_graph_get(&t_e->dep_graph,t_id);
    if(d_n)
    {
	task_graph_rem(&t_e->dep_graph,d_n);
    }
    mem_pool_free(&t_e->task_pool, t);
    pthread_mutex_unlock(&t_e->task_pool_lock);
}


void task_engine_sched(struct task *task)
{
    
    //"Scheduling task",
    size_t min=UINT32_MAX;
    size_t selected = 0;
    //Find which worker to assign the task to.
    pthread_mutex_lock(&t_e->lock);
    for(size_t i=0;i<t_e->num_workers;i++)
    {
	size_t queue_size = work_thief_queue_size(&t_e->workers[i]);
	if(queue_size == 0 && !t_e->workers[i].running->task)
	{
	    selected = i;
	    break;
	}
	//printf("%u queue_size %zu\n",t_e->workers[i].thread_id,queue_size);
	if(queue_size < min)
	{
	    min=queue_size;
	    selected = i;
	}
	
    }
    pthread_mutex_unlock(&t_e->lock);
    //printf("Scheduled task %u on (%zu)%u\n",task->task_id,selected, t_e->workers[selected].thread_id);
    work_thief_add_task(&t_e->workers[selected],task);
    
}

bool task_engine_try_steal(struct work_thief *w_t)
{
    
    uint32_t victim_pos = rand()%t_e->num_workers;
    struct work_thief *victim = &t_e->workers[victim_pos];
    if(victim == w_t)
    {
	return false;
    }
    else
    {
	/*
	size_t victim_q_size = work_thief_queue_size(victim);
	if(victim_q_size < 2)
	{
	    return false;
	}
	*/
	struct task *loot = work_thief_tail_steal(victim);
	if(loot)
	{
	    //printf("Successfully stole task: %u\n",loot->task_id);
	    work_thief_add_task(w_t, loot);
	    return true;
	}
    }
    return false;
}

void task_engine_add_task(struct task *task, size_t num_deps, ...)
{

    struct task_dep_node *d_n = malloc(sizeof(struct task_dep_node));
    task_dep_node_init(d_n,task);
    
    pthread_mutex_lock(&t_e->lock);
    
    va_list v_a;
    va_start(v_a, num_deps);
    for(size_t i = 0 ; i < num_deps;i++)
    {
	
	uint32_t t_id = va_arg(v_a,uint32_t);
	struct task_dep_node *dep = task_graph_get(&t_e->dep_graph,t_id);
	if(dep)
	{
	    //Handle dependencies
	    task_dep_node_add_in(d_n,t_id);
	    task_dep_node_add_out(dep,task_get_id(task));
	}
	
    }
    va_end(v_a);
    task_graph_add(&t_e->dep_graph,d_n);
    pthread_mutex_unlock(&t_e->lock);
    //If there are no dependencies, no need to wait
    if(num_deps == 0)
    {
	task_engine_sched(task);
    }

}

void task_engine_done(struct task *task)
{
    pthread_mutex_lock(&t_e->lock);
    uint32_t task_id = task_get_id(task);
    struct task_dep_node *d_n = task_graph_get(&t_e->dep_graph,task_id);
    for(size_t i=0; i< d_n->num_out_edges; i++)
    {
	struct task_dep_node *d = task_graph_get(&t_e->dep_graph,d_n->out_edges[i]);
	if(d)
	{
	    task_dep_node_rem_in(d, task_id);
	    if(d->num_in_edges == 0)
	    {
		//Think about this, is it correct? Is there a better way?
		pthread_mutex_unlock(&t_e->lock);
		task_engine_sched(d->task);
		pthread_mutex_lock(&t_e->lock);
		
	    }
	}
    }
    
    pthread_mutex_unlock(&t_e->lock);
}


void task_engine_destroy()
{    
    pthread_mutex_lock(&t_e->lock);
    task_graph_destroy(&t_e->dep_graph);
    pthread_mutex_unlock(&t_e->lock);

    for(size_t i=0;i<t_e->num_workers;i++)
    {
	//work_thief_destroy(&t_e->workers[i]);
    }
    //free(t_e->workers);   
}

