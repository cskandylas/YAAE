#include "connection.h"
#include "connection_unix.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


static struct connection known_connection_handlers[] =
{
    {
	CONN_T_TCP,
	NULL,      //init
	NULL,      //init args
	NULL,      //dial
	NULL,      //listen
	NULL,      //send
	NULL,      //recv
	NULL,      //destroy
	NULL,      //destroy args
    },

    {
	CONN_T_UDP,
	NULL,      //init
	NULL,      //init args
	NULL,      //dial
	NULL,      //listen
	NULL,      //send
	NULL,      //recv
	NULL,      //destroy
	NULL,      //destroy args
    },
    {
	CONN_T_UNIX,
	unix_init,
	NULL,
	unix_dial,
	unix_listen,
	unix_send,
	unix_recv,
	unix_destroy,
	NULL,
    }	
};


bool connection_init(struct connection *conn, enum CONNECTION_TYPE c_t, char *init_args)
{

    //can < 0 happen? 
    if( c_t >= CONN_T_NUM_KNOWN_TYPES || c_t < 0 || c_t == CONN_T_UDP)
    {
	fprintf(stderr, "Don't know how to handle this type of connection!\n");
	fprintf(stderr, "Use: connection_init_with_params instead\n");
	return false;
    }
    else
    {
	conn->init_fn = known_connection_handlers[c_t].init_fn;
	conn->init_args = init_args;
	conn->destroy_fn   = known_connection_handlers[c_t].destroy_fn;
	conn->destroy_args = known_connection_handlers[c_t].destroy_args;
	conn->dial_fn = known_connection_handlers[c_t].dial_fn;
	conn->listen_fn = known_connection_handlers[c_t].listen_fn;
	conn->send_fn = known_connection_handlers[c_t].send_fn;
	conn->recv_fn = known_connection_handlers[c_t].recv_fn;

	return conn->init_fn(conn, init_args);
	
    }
    
    
    
}

//In case we want to deal with with a connection we don't know how to handle
bool connection_init_with_params(struct connection *conn, uint8_t c_t,
				 c_init cinit, void *init_args,
				 c_destroy cdestroy, void *destroy_args,
				 c_dial cdial, c_listen clisten,
				 c_send csend, c_recv crecv)
{

    conn->c_t = c_t;
    conn->init_fn = cinit;
    conn->init_args = init_args;
    conn->destroy_fn   = cdestroy;
    conn->destroy_args = destroy_args;
    conn->dial_fn = cdial;
    conn->listen_fn = clisten;
    conn->send_fn = csend;
    conn->recv_fn = crecv;

    return conn->init_fn(conn, init_args);
}


bool connection_dial(struct connection *conn)
{
    return conn->dial_fn(conn);
}
bool connection_listen(struct connection *conn)
{
    return conn->listen_fn(conn);
}

ssize_t connection_send(struct connection *conn, void *buff, size_t num_bytes)
{
    return conn->send_fn(conn,buff,num_bytes);
}

ssize_t connection_recv(struct connection *conn, void *buff, size_t num_bytes)
{
    return conn->recv_fn(conn,buff,num_bytes);
}

bool connection_destroy(struct connection *conn)
{
    
    if(conn->ctx)
    {
	free(conn->ctx);
	conn->ctx=NULL;
    }
    return  conn->destroy_fn(conn, conn->destroy_args);

}
