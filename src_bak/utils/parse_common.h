
#include <stddef.h>

#ifndef PARSE_COMMON
#define PARSE_COMMON


int consume(char *token, char *expected,size_t *cur_token);

char* parse_name(char **tokens, size_t num_tokens, size_t *cur_token);
int parse_int(char **tokens, size_t num_tokens, size_t *cur_token);
double parse_double(char **tokens, size_t num_tokens, size_t *cur_token);
char* parse_string(char **tokens, size_t num_tokens, size_t *cur_token);


#endif
