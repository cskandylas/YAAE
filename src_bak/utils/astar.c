#include "astar.h"

#include "heap.h"
#include "hashtable.h"

#include <stdlib.h>
#include <stdio.h>


/*
int manhattan_dist(struct grid_coord cell_a ,struct grid_coord cell_b)
{
    return abs(cell_a.x-cell_b.x)+abs(cell_a.y-cell_b.y);
}


struct grid_coord  *astar(struct grid *grid,struct grid_coord start,struct grid_coord goal)

{

    printf("Width:%d, Height:%d\n",grid->width,grid->height);
    printf("Searching for a path from (%d,%d) to (%d,%d) \n",start.x,start.y,goal.x,goal.y);
    //priority is cost, payload is grid coord
    struct heap open_list;
    heap_init(&open_list, 64);

    //grid_coord to grid_coord
    struct hash_table_t parents;
    hash_table_init(&parents,1024);       

    //DIR_OFF[0] = LEFT, DIR_OFF[1] = RIGHT, DIR_OFF[2]=UP, DIR_OFF[3]=DOWN
    struct grid_coord DIR_OFF[4];
    DIR_OFF[0].x=-1;
    DIR_OFF[0].y=0;
    DIR_OFF[1].x=1;
    DIR_OFF[1].y=0;
    DIR_OFF[2].x=0;
    DIR_OFF[2].y=-1;
    DIR_OFF[3].x=0;
    DIR_OFF[3].y=1;
    
    
    hash_table_insert(&parents,&start,sizeof(struct grid_coord),&grid->cells[start.x][start.y],sizeof(struct grid_coord));

    int cost=0;
    heap_insert(&open_list,0,&start);
    grid->cost[start.x][start.y]=0;
    
    while(open_list.cur_size>0)
    {
	//printf("open_list size:%d\r",open_list.cur_size);
	
	struct grid_coord *current=heap_top(&open_list);
	printf("current element (%d,%d)\r",current->x,current->y);
	
	if(current->x==goal.x && current->y==goal.y)
        {
	    printf("Path found!\n");
            break;
        }

	int cost=grid->cost[current->x][current->y];

	for(int i=0;i<4;i++)
	{
	    if(current->x+DIR_OFF[i].x >= 0 && current->x+DIR_OFF[i].x < grid->width &&
	       current->y+DIR_OFF[i].y >= 0 && current->y+DIR_OFF[i].y < grid->height &&
		grid->colision[current->x+DIR_OFF[i].x][current->y+DIR_OFF[i].y]==CELL_EMPTY)
	    {
		
		struct grid_coord *next=&grid->cells[current->x+DIR_OFF[i].x][current->y+DIR_OFF[i].y];
		int next_cost=grid->cost[next->x][next->y];
		if(cost<next_cost)
		{
		    int priority=cost+manhattan_dist(*next,goal);
		    
		    heap_insert(&open_list,priority,next);
		    struct grid_coord *next_parent=hash_table_get(&parents,next,sizeof(struct grid_coord),NULL);
		    if(!next_parent)
		    {
			hash_table_insert(&parents,next,sizeof(struct grid_coord),current,sizeof(struct grid_coord));
		    }
		    else
		    {
			//deep copy
			*next_parent=*current;
		    }
		    
		    grid->cost[next->x][next->y]=priority;	     
		}
	    }
	}

    }


    for(int i=0;i<grid->width;i++)
    {
	for(int j=0;j<grid->height;j++)
	{
	    printf("%d ",grid->cost[i][j]);
	}
	printf("\n");
    }
    

    printf("Path reconstruction:");
    struct grid_coord *curr=hash_table_get(&parents,&goal,sizeof(struct grid_coord),NULL);
    while(curr!=NULL && !(curr->x==start.x && curr->y==start.y))
    {
	printf("(%d,%d)\n",curr->x,curr->y);
	curr=hash_table_get(&parents,curr,sizeof(struct grid_coord),NULL);
    }
    
}
*/
