#include "parse_common.h"

#include <string.h>
#include <stdlib.h>
#include <ctype.h>


inline int consume(char *token, char *expected,size_t *cur_token)
{
    if(strcmp(token,expected)==0)
    {
	(*cur_token)++;
	return 1;
    }
    else
    {
	#ifdef DEBUG
	fprintf(stderr,"Found [%s], expected: [%s]\n",token,expected);
	#endif
    }

    return 0;
}


char* parse_name(char **tokens, size_t num_tokens, size_t *cur_token)
{
    if(isalpha(tokens[*cur_token][0]))
    {
	char *ret=malloc(strlen(tokens[*cur_token])+1);
	strcpy(ret,tokens[*cur_token]);
	(*cur_token)++;
	return ret;
    }
    return NULL;
}

int parse_int(char **tokens, size_t num_tokens, size_t *cur_token)
{
    int val=atoi(tokens[*cur_token]);
    if(val!=0 || strcmp(tokens[*cur_token],"0")==0)
    {
	(*cur_token)++;
    }
    return val;
}

double parse_double(char **tokens, size_t num_tokens, size_t *cur_token)
{
    double val=atof(tokens[*cur_token]);
    if(val!=0.0 || strcmp(tokens[*cur_token],"0.0")==0)
    {
	(*cur_token)++;
    }
    return val;
}

char* parse_string(char **tokens, size_t num_tokens, size_t *cur_token)
{
    int len=strlen(tokens[*cur_token]);
    if(tokens[*cur_token][0]=='\'' && tokens[*cur_token][len-1]=='\'')
    {
	char *ret=malloc(len-1);
	strncpy(ret,&tokens[*cur_token][1],len-2);
	ret[len-2]='\0';

	(*cur_token)++;
	
	return ret;
    }
    return NULL;
}
