#include "rbtree.h"

#include <stdlib.h>
#include <stdio.h>


#define RB_OK 0
#define RB_DUPLICATE -1
#define RB_MEM_ERROR -2
#define RB_NO_DELETE -3


void traverse_in(struct rb_node *node)
{
   
	if(!node)
		return;
	traverse_in(node->left);
	printf("%d  ",*(int*)node->data);
	traverse_in(node->right);

	
}


void rb_tree_init(struct rb_tree *tree, rb_cmp_func cmp)
{
    tree->root=NULL;
    tree->cmp=cmp;
    tree->size=0;
}

static void rotate_left(struct rb_tree *tree,struct rb_node *node)
{
    struct rb_node *right=node->right;
    node->right=right->left;
    if(right->left != NULL)
    {
	right->left->parent=node;
    }

    right->parent= node->parent;

    if( node->parent != NULL)
    {
	if(node == node->parent->left)
	{
	    node->parent->left=right;
	}
	else
	{
	    node->parent->right=right;
	}
    }
    else
    {
	tree->root=right;
    }
    
    right->left=node;
    node->parent=right;
}

static void rotate_right(struct rb_tree *tree,struct rb_node *node)
{
    struct rb_node *left=node->left;
    node->left=left->right;
    if(left->right != NULL)
    {
	left->right->parent=node;
    }

    left->parent=node->parent;

    if(node->parent != NULL)
    {
	if( node == node->parent->right)
	{
	    node->parent->right = left;
	}
	else
	{
	    node->parent->left = left;
	}
    }
    else
    {
	tree->root=left;
    }
    
    left->right=node;
    node->parent=left;
}

static void fixup_insert(struct rb_tree *tree, struct rb_node *node)
{
    //Node used to decide the fix
    struct rb_node *decision=NULL;
    //Move up till we find root
    while(node != tree->root && node->parent->color == RED)
    {
	//our parent if a left child
	if(node->parent == node->parent->parent->left)
	{
	    decision = node->parent->parent->right;
	    if(decision && decision-> color == RED)
	    {
		//recolor
		node->parent->color=BLACK;
		decision->color=BLACK;
		node->parent->parent->color=RED;

		//pushed the problem up to granda so fix it next
		node=node->parent->parent;
	    }
	    else
	    {
		//if node is the right child rotate left
		if(node == node->parent->right)
		{
		    node=node->parent;
		    rotate_left(tree,node);
		}
		//node is the left child, flip colors and rotate grandpa
		node->parent->color=BLACK;
		node->parent->parent->color=RED;
		rotate_right(tree,node->parent->parent);
	    }
	}
	else
	{
	    decision=node->parent->parent->left;
	    if(decision && decision->color == RED)
	    {
		//recolor
		node->parent->color=BLACK;
		decision->color=BLACK;
		node->parent->parent->color=RED;

		//pushed the problem up to granpa so fix it next
		node=node->parent->parent;
	    }
	    else
	    {
		//if node is the left child
		if(node == node->parent->left)
		{
		    node=node->parent;
		    rotate_right(tree,node);
		}
		//node is the left child, flip colors and rotate grandpa
		node->parent->color=BLACK;
		node->parent->parent->color=RED;
		rotate_left(tree,node->parent->parent);
	    }
	}
	
    }
    //Fix the root color
    tree->root->color=BLACK;
}


int rb_tree_insert(struct rb_tree *tree, void *data)
{

    //Find where to put the new data if anywhere
    struct rb_node *curr_node=tree->root;
    struct rb_node *curr_parent=NULL;


    int cmp_res=0;
    while(curr_node!=NULL)
    {
	cmp_res=tree->cmp(curr_node->data,data);
	if(cmp_res==0)
	{
	    //Duplicate found, abort
	    return RB_DUPLICATE;
	}
	curr_parent=curr_node;
	if(cmp_res<0)
	{
	    curr_node=curr_node->left;
	}
	else
	{
	    curr_node=curr_node->right;
	}
	
    }
    
    //create a new node and assign data to it
    struct rb_node *node=malloc(sizeof(struct rb_node));
    if(!node)
    {
	return RB_MEM_ERROR;
    }
    node->data=data;
    node->color=RED;
    node->left=node->right=NULL;
    node->parent=curr_parent;

    if(curr_parent==NULL)
    {
	//tree was empty just assign new node to the root
	tree->root=node;
    }
    else
    {
	if(cmp_res < 0)
	{
	    curr_parent->left=node;
	}
	else
	{
	    curr_parent->right=node;
	}
    }

    //Fixup to preserve rb-tree properties
    fixup_insert(tree,node);

    tree->size++;
    
    return RB_OK;
}


void* rb_tree_find(struct rb_tree *tree, void *data)
{
    struct rb_node *tnode=tree->root;

    int cmp_res=-1;
    //traverse the binary tree
    while(tnode!=NULL)
    {
	cmp_res=tree->cmp(tnode->data,data);
	if(cmp_res == 0)
	{
	    //found the data
	    return tnode->data;
	}
	else if(cmp_res<0)
	{
	    tnode=tnode->left;
	}
	else
	{
	    tnode=tnode->right;
	}
    }

    //find failed
    return NULL;
}

static struct rb_node *find_node(struct rb_tree *tree, void *data)
{
    struct rb_node *tnode=tree->root;

    int cmp_res=-1;
    //traverse the binary tree
    while(tnode!=NULL)
    {
	cmp_res=tree->cmp(tnode->data,data);
	if(cmp_res == 0)
	{
	    //found the data
	    return tnode;
	}
	else if(cmp_res<0)
	{
	    tnode=tnode->left;
	}
	else
	{
	    tnode=tnode->right;
	}
    }

    //find failed
    return NULL;
}

static inline void update_parent(struct rb_tree *tree, struct rb_node *parent,
				 struct rb_node* old, struct rb_node *new)
{
    if(parent == NULL)
    {
	if(tree->root == old)
	{
	    tree->root = new;
	    return;
	}
    }

    if(parent->left == old)
    {
	parent->left = new;
    }
    
    if(parent->right == old)
    {
	parent->right = new;
    }
}

static inline void update_child(struct rb_node *child,
			   struct rb_node *old, struct rb_node *new)
{
    if(child == NULL)
    {
	return;
    }
    
    if(child->parent == old)
    {
	child->parent = new;
    }
}

static void fixup_delete(struct rb_tree *tree, struct rb_node *child,
			 struct rb_node *node)
{

    if(!node)
    {
	return;
    }
    
    struct rb_node *sibling;

    if( node->right == child)
    {
	sibling=node->left;
    }
    else
    {
	sibling=node->right;
    }
    
    
    uint8_t move_up=1;

    while(move_up)
    {
	if(node==NULL)
	{
	    //node, i.e. child's parent is root, everything is okay, just return
	    return;
	}

	if(sibling && sibling->color == RED)
	{
	    //update colors and rotate
	    node->color=RED;
	    sibling->color=BLACK;
	    if(node->right==child)
	    {
		rotate_right(tree,node);
	    }
	    else
	    {
		rotate_left(tree,node);
	    }
	    //update the sibling
	    if(node)
	    {
		if(node->right == child)
		{
		    sibling=node->left;
		}
		else
		{
		    sibling=node->right;
		}
	    }
	}
	    if(node->color == BLACK
	       && sibling && sibling->color == BLACK
	       && sibling->left && sibling->left->color == BLACK
	       && sibling->right && sibling->right->color == BLACK)
	    {
		//fix sibling's color
		sibling->color = RED;
		
		//update child and node
		child=node;
		node=node->parent;
		//update sibling
		if(node)
		{
		    if(node->right == child)
		    {
			sibling=node->left;
		    }
		    else
		    {
			sibling=node->right;
		    }
		}
	    }
	    else
	    {
		//we're done
		move_up=0;
	    }
    }
    
    if(node && sibling && sibling->left && sibling->right)
    {
	if(node->color == RED && sibling->color == BLACK
	   && sibling->left->color == BLACK && sibling->right->color == BLACK)
	{
	    //all nodes are black just fix the color and exit
	    sibling->color = RED;
	    node->color = BLACK;
	    return;
	}
	
	//rotate based on which sibling is red then update sibling
	if( node->right == child && sibling->color == BLACK
	    && sibling->right->color == RED && sibling->left->color == BLACK)
	{
	    sibling->color = RED;
	    sibling->right->color = BLACK;
	    rotate_left(tree,sibling);
	    //update sibling
	    if(node->right == child)
	    {
		sibling = node->left;
	    }
	    else
	    {
		sibling = node->right; 
	    }
	}
	else if(node->left == child && sibling->color == BLACK &&
		sibling->left->color == RED && sibling->right->color == BLACK)
	{
	    sibling->color=RED;
	    sibling->left->color=BLACK;
	    rotate_right(tree,sibling);
	    //update sibling
	    if(node->right == child)
	    {
		sibling = node->left;
	    }
	    else
	    {
		sibling = node->right; 
	    }
	}
	
	//rotate and flip node an sibling colors
	sibling->color=node->color;
	node->color=BLACK;
	if(node->right == child)
	{
	    sibling->left->color=BLACK;
	    rotate_right(tree,node);
	}
	else
	{
	    sibling->right->color = BLACK;
	    rotate_left(tree,node);
	}
    }
}

int rb_tree_delete(struct rb_tree *tree, void *data)
{

    struct rb_node *del_node=find_node(tree,data);
    if(del_node == NULL)
    {
	return RB_NO_DELETE;
    }

    if(del_node->left!=NULL && del_node->right!=NULL)
    {
	//find the smallest value in the right subtree
	struct rb_node *smallest=del_node->right;
	
	while(smallest->left != NULL)
	{
	    smallest=smallest->left;
	}
	//swap colors between del_node and smallest
	uint8_t tmp_c=del_node->color;
	del_node->color=smallest->color;
	smallest->color=tmp_c;

	//update parent pointers
	update_parent(tree,del_node->parent,del_node,smallest);
	if(del_node->right != smallest)
	{
	    update_parent(tree,smallest->parent,smallest,del_node);
	}
	//update child pointers
	update_child(smallest->left,smallest,del_node);
	update_child(smallest->left,smallest,del_node);
	update_child(smallest->right,smallest,del_node);
	update_child(smallest->right,smallest,del_node);
	update_child(del_node->left,del_node,smallest);
	if(del_node->right != smallest)
	{
	    update_child(del_node->right,del_node,smallest);
	}
	//Might have happened after above update
	if(del_node->right == smallest)
	{
	    del_node->right = del_node;
	    smallest->parent = smallest;
	}

	//update node pointers
	//parent
	struct rb_node *tmp_node;
	tmp_node=del_node->parent;      
	del_node->parent=smallest->parent;  
	smallest->parent=tmp_node;
	//left
	tmp_node=del_node->left;
	del_node->left=smallest->left;
	smallest->left=tmp_node;
	//right
	tmp_node=del_node->right;
	del_node->right=smallest->right;
	smallest->right=tmp_node;
    }

    struct rb_node *child;
    if(del_node->left != NULL)
    {
	child=del_node->left;
    }
    else
    {
	child=del_node->right;
    }

    //replace del_node with its child
    update_parent(tree,del_node->parent,del_node,child);
    update_child(child,del_node,del_node->parent);

    if(child)
    {
	if(del_node->color == BLACK)
	{
	    if(child->color == RED)
	    {
		    child->color = BLACK;
	    }
	    else
	    {
		fixup_delete(tree,child,del_node->parent);
	    }

	}
    }

    //clear pointers
    del_node->parent=NULL;
    del_node->left=NULL;
    del_node->right=NULL;
    del_node->color=0;
    free(del_node);
    tree->size--;
    return RB_OK;
}

void rb_tree_delete_branch(struct rb_node *tnode)
{
    //just clearing tree stucture, data passed to the tree is callers problem
    if(tnode)
    {
	if(tnode->left)
	    rb_tree_delete_branch(tnode->left);
	if(tnode->right)
	    rb_tree_delete_branch(tnode->right);
	free(tnode);
	
    }
}


void rb_tree_destroy(struct rb_tree *tree)
{
    
    rb_tree_delete_branch(tree->root);
}


