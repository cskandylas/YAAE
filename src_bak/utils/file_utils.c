#include <stdio.h>
#include <stdlib.h>


char *read_file(char *filename)
{

    FILE *ifp=fopen(filename,"r");
    if(!ifp)
    {
	fprintf(stderr,"Could not find %s!\n",filename);
    }
    fseek(ifp, 0, SEEK_END);
    long numbytes = ftell(ifp);
    fseek(ifp, 0, SEEK_SET);	
    char *text=malloc(numbytes+1);
    size_t num_read=0;
    if(!text)
    {
	fprintf(stderr,"Could not allocate memory buffer\n");
	exit(-1);
	
    }
    num_read=fread(text, sizeof(char), numbytes, ifp);
    fclose(ifp);
    text[numbytes]='\0'; 
    
    return text;
}


size_t num_lines(char *text)
{
    size_t num_lines=0;
    while(*text!='\0')
    {
	if(*text=='\n')
	{
	    num_lines++;
	}
	text++;
    }

    return num_lines;
}
