#include "heap.h"

#include <stdlib.h>


static inline int parent(int idx)
{
    return (idx - 1) / 2;
}

static inline int leftchild(int idx)
{
  return  2 * idx + 1;
}

static inline int rightchild(int idx)
{
    return  2 * idx + 2;
}


void heap_init(struct heap *heap,size_t initial_size)
{
    heap->max_size=initial_size;
    heap->cur_size=0;
    heap->heap_array=malloc(heap->max_size*sizeof(struct heap_entry));
}

void heap_insert(struct heap *heap, int priority, void *payload)
{
    
    if(heap->cur_size >= heap->max_size)  
    {
	heap->max_size *= 2;
	heap->heap_array = realloc(heap->heap_array, heap->max_size*sizeof(struct heap_entry));
    }
    
    heap->heap_array[heap->cur_size].priority=priority;
    heap->heap_array[heap->cur_size].payload=payload;
    heap->cur_size++;

    //fixup
    int idx=heap->cur_size-1;
    while(idx>0 && priority<heap->heap_array[parent(idx)].priority )
    {
	heap->heap_array[idx] = heap->heap_array[parent(idx)];
	idx=parent(idx);
	heap->heap_array[idx].priority=priority;
	heap->heap_array[idx].payload=payload;
    }
}

void* heap_peek(struct heap *heap)
{
    if(heap->cur_size>0)
    {
	//&heap_array[0] == heap_array
	return heap->heap_array->payload;
    }
    return NULL;
}

int heap_peek_priority(struct heap *heap)
{

    if(heap->cur_size>0)
    {
	//&heap_array[0] == heap_array
	return heap->heap_array->priority;
    }
    return -1;
    
}

void *heap_top(struct heap *heap)
{

    //early return due to empty heap
    if(heap->cur_size <= 0)
	return NULL;
    void *ret=heap->heap_array->payload;
    
    heap->heap_array[0]=heap->heap_array[heap->cur_size-1];
    struct heap_entry replace=heap->heap_array[heap->cur_size-1];
    int idx=0;
    heap->cur_size--;

    
    //fixdown
    while( leftchild(idx)<heap->cur_size)
    {
	int smallest_idx = leftchild(idx);
	if(rightchild(idx) <= heap->cur_size &&
	   heap->heap_array[rightchild(idx)].priority<heap->heap_array[leftchild(idx)].priority)
	   smallest_idx = rightchild(idx);

	if(replace.priority<heap->heap_array[smallest_idx].priority)
	    break;

	heap->heap_array[idx] = heap->heap_array[smallest_idx];
	idx = smallest_idx;
	heap->heap_array[idx] = replace;
    }
    return ret; 
    
}

void heap_destroy(struct heap *heap)
{
    free(heap->heap_array);
}
