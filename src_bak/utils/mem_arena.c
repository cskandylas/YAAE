#include "mem_arena.h"

#include <stdlib.h>
#include <stddef.h>


#include <stdio.h>
#include <string.h>

void mem_arena_init(struct mem_arena *m_a, size_t n_bytes)
{
    m_a->bytes = malloc(n_bytes);
    memset(m_a->bytes, 0, n_bytes);
    m_a->n_bytes = n_bytes;
    m_a->n_alloc = 0;
}
void mem_arena_destroy(struct mem_arena *m_a)
{
    free(m_a->bytes);
}
void *mem_arena_alloc(struct mem_arena *m_a, size_t a_size)
{

    
    //printf("%zu %zu %zu\n",m_a->n_alloc,m_a->n_bytes, a_size);
    
    if ( (m_a->n_alloc + a_size) >= m_a->n_bytes )
    {
	return NULL;
    }

    //gotta love C ;) This erases type correctly
    
    void *ret = &m_a->bytes[m_a->n_alloc];
    m_a->n_alloc += a_size;
    
    return ret;
}


void *mem_arena_free(struct mem_arena *m_a, void *p)
{
    //yeah, we're not gonna do anything here, cheers
}
