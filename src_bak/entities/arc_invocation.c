#include "arc_invocation.h"
#include <stdio.h>

void init_invocation(struct arc_invocation *invo, struct arc_interface *iface,
		     struct arc_component *from, struct arc_component *to)
{
    invo->iface=iface;
    invo->from=from;
    invo->to=to;
}


void print_invocation(struct arc_invocation *invo)
{
    printf("Invocation %s->%s:%s\n",invo->from->comp_name,invo->to->comp_name,
	   invo->iface->if_name);
}
