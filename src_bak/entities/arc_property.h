#ifndef ARC_PROPERTY
#define ARC_PROPERTY


enum property_type {INT=0,DOUBLE,STRING,INVALID};


union property_value
{
    int v_int;
    double v_dbl;
    char *v_str;
};

struct arc_property
{
    char *name;
    enum property_type type;
    //union property_value value;
    union
    {
	int v_int;
	double v_dbl;
	char *v_str;
    };
};

void init_int_property(struct arc_property *property, char *name, int value);
void init_dbl_property(struct arc_property *property, char *name, double value);
void init_str_property(struct arc_property *property, char *name, char *value);
struct arc_property *clone_property(struct arc_property *property);

void destroy_property(struct arc_property *property);
void destroy_int_property(struct arc_property *property);
void destroy_dbl_property(struct arc_property *property);
void destroy_str_property(struct arc_property *property);

void print_property(struct arc_property *property);



#endif
