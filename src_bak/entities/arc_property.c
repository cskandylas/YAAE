#include "arc_property.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void init_int_property(struct arc_property *property, char *name, int value)
{
    property->name=malloc(strlen(name)+1);
    strcpy(property->name,name);
    property->v_int=value;
    property->type=INT;
}
void init_dbl_property(struct arc_property *property, char *name, double value)
{
    property->name=malloc(strlen(name)+1);
    strcpy(property->name,name);
    property->v_dbl=value;
    property->type=DOUBLE;
}
void init_str_property(struct arc_property *property, char *name, char *value)
{
    property->name=malloc(strlen(name)+1);
    strcpy(property->name,name);
    
    property->v_str=malloc(strlen(value)+1);
    strcpy(property->v_str,value);
    property->type=STRING;
}

void print_property(struct arc_property *property)
{
    printf("Property ");
    if(property->type==INT)
	printf("%s:%d\n",property->name,property->v_int);
    else if(property->type==DOUBLE)
	printf("%s:%f\n",property->name,property->v_dbl);
    else
	printf("%s:%s\n",property->name,property->v_str);
}

void destroy_int_property(struct arc_property *property)
{
    free(property->name);
}

void destroy_dbl_property(struct arc_property *property)
{
    free(property->name);
}
void destroy_str_property(struct arc_property *property)
{
    free(property->name);
    
    if(property->type == STRING && property->v_str)
    {
	free(property->v_str);
    }
    
}

void destroy_property(struct arc_property *property)
{
    if(property->type==INT)
	destroy_int_property(property);
    else if(property->type==DOUBLE)
	destroy_dbl_property(property);
    else
	destroy_str_property(property);
}

struct arc_property *clone_property(struct arc_property *property)
{
    struct arc_property *clone=malloc(sizeof(struct arc_property));
    clone->name=malloc(strlen(property->name)+1);
    strcpy(clone->name,property->name);
    if(property->type==INT)
	clone->v_int=property->v_int;
    else if(property->type==DOUBLE)
	clone->v_dbl=property->v_dbl;
    else
    {
	clone->v_str=malloc(strlen(property->v_str)+1);
	strcpy(clone->v_str,property->v_str);
	clone->v_str[strlen(property->v_str)]='\0';
    }
    return clone;
}

