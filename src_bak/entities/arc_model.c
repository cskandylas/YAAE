#define _XOPEN_SOURCE 600


#include "arc_model.h"
#include <stdlib.h>
#include <string.h>
#include "tokenize.h"


void init_model(struct arc_model *model, struct arc_system *sys)
{
    model->sys = sys;
    pthread_mutex_init(&model->sys_lock,NULL);
}


void destroy_model(struct arc_model *model)
{
    pthread_mutex_destroy(&model->sys_lock);
}



bool arc_model_add_component(struct arc_model *a_m, char *name)
{
    struct arc_component *n_c = NULL;
    pthread_mutex_lock(&a_m->sys_lock);
    struct arc_system *sys = a_m->sys;
    n_c = sys_find_component(sys,name);
    pthread_mutex_unlock(&a_m->sys_lock);
    if(n_c)
    {
	//component already exists
	return false;
    }
    
    n_c = malloc(sizeof(struct arc_component));
    init_component(n_c,name);
    pthread_mutex_lock(&a_m->sys_lock);
    sys_add_component(sys,n_c);
    pthread_mutex_unlock(&a_m->sys_lock);
    
    return true;
}

bool arc_model_rem_component(struct arc_model *a_m, char *name)
{
    pthread_mutex_lock(&a_m->sys_lock);
    struct arc_component *c = sys_rem_component(a_m->sys,name);
    pthread_mutex_unlock(&a_m->sys_lock);

    if(c)
    {
	destroy_component(c);
	free(c);
    }
    return true;
}




bool arc_model_add_interface(struct arc_model *a_m, char *component, char *name)
{
    struct arc_component *c = NULL;
    pthread_mutex_lock(&a_m->sys_lock);
    c = sys_find_component(a_m->sys,component);
    pthread_mutex_unlock(&a_m->sys_lock);
    if(!c)
    {
	//component doesn't exist, can't add interface to it.
	return false;
    }
    struct arc_interface *i = NULL;
    pthread_mutex_lock(&a_m->sys_lock);
    i = comp_find_iface(c,name);
    pthread_mutex_unlock(&a_m->sys_lock);
    
    if(i)
    {
	//interface already exists
	return false;
    }

    
    i = malloc(sizeof(struct arc_interface));
    init_interface(i,name);
    pthread_mutex_lock(&a_m->sys_lock);
    comp_add_iface(c, i);
    pthread_mutex_unlock(&a_m->sys_lock);
    return true;   
}

bool arc_model_rem_interface(struct arc_model *a_m, char *component, char *name)
{
    struct arc_component *c = NULL;
    pthread_mutex_lock(&a_m->sys_lock);
    c = sys_find_component(a_m->sys,component);
    pthread_mutex_unlock(&a_m->sys_lock);
    if(!c)
    {
	return false;
    }

    struct arc_interface *i = NULL;
    pthread_mutex_lock(&a_m->sys_lock);
    i = comp_rem_iface(c,name);
    pthread_mutex_unlock(&a_m->sys_lock);

    if(i)
    {
	destroy_interface(i);
	free(i);
    }
    return true;
    
    
}


bool unqual_invocation(char *qualified, char **component, char **interface)
{
    int len = strlen(qualified);
    if( len < 3)
    {
	return false;
    }
    
    
    char *colon = strchr(qualified, ':');
    if(!colon)
    {
	//no ':' in string, it can't be unqualified
	return false;
    }

    //allocate memory and copy the component part
    *component = malloc(colon-qualified+1);
    strncpy(*component, qualified, colon-qualified);
    (*component)[colon-qualified]='\0';
    //allocate memory and copy the interface part
    *interface = malloc(len - (colon-qualified)+1);
    strncpy(*interface, colon+1, len - (colon-qualified));
    (*interface)[len-(colon-qualified)]='\0';
    return true;
}

bool arc_model_add_invocation(struct arc_model *a_m, char *qual_from, char *qual_to)
{
    
    char *from_s = NULL;
    char *from_if_s = NULL;
    char *to_s = NULL;
    char *to_if_s = NULL;
    
    if(!unqual_invocation(qual_from, &from_s, &from_if_s))
    {
	return false;
    }
    if(!unqual_invocation(qual_to, &to_s, &to_if_s))	
    {
	return false;
    }
    
    struct arc_invocation *i = NULL;
    pthread_mutex_lock(&a_m->sys_lock);
    i = sys_find_invocation(a_m->sys, from_s, to_s, to_if_s);
    pthread_mutex_unlock(&a_m->sys_lock);
    if(i)
    {
	//invocation already exists
	return false;
    }
    
    struct arc_component *from = NULL;
    struct arc_component *to = NULL;
    
    struct arc_interface *from_if = NULL;
    struct arc_interface *to_if = NULL;

    pthread_mutex_lock(&a_m->sys_lock);
    from = sys_find_component(a_m->sys, from_s);
    to = sys_find_component(a_m->sys, to_s);
    pthread_mutex_unlock(&a_m->sys_lock);
    if(!from || !to)
    {
	//to or from component not in the system
	return false;
    }

    pthread_mutex_lock(&a_m->sys_lock);
    from_if = comp_find_iface(from, from_if_s);
    to_if = comp_find_iface(to, to_if_s);
    pthread_mutex_unlock(&a_m->sys_lock);

    if(!to_if) //add from_if when I revise invocations
    {
	return false;
    }

    i = malloc(sizeof(struct arc_invocation));
    init_invocation(i,to_if,from,to);

    pthread_mutex_lock(&a_m->sys_lock);
    sys_add_invocation(a_m->sys,i);
    pthread_mutex_unlock(&a_m->sys_lock);

    return true;
}

bool arc_model_rem_invocation(struct arc_model *a_m, char *qual_from, char *qual_to)
{

    char *from_s = NULL;
    char *from_if_s = NULL;
    char *to_s = NULL;
    char *to_if_s = NULL;
    
    if(!unqual_invocation(qual_from, &from_s, &from_if_s))
    {
	return false;
    }
    if(!unqual_invocation(qual_to, &to_s, &to_if_s))	
    {
	return false;
    }
    
    pthread_mutex_lock(&a_m->sys_lock);
    sys_rem_invocation(a_m->sys,from_s, to_s,to_if_s);
    pthread_mutex_unlock(&a_m->sys_lock);
    return true;
}

bool arc_model_assign_property_int(struct arc_model *a_m, char *qual_name, int val)
{

    bool ret;
    struct tokenizer qualifier;
    tokenizer_init(&qualifier, qual_name, ":");
    int num_qnames = count_tokens(&qualifier);

    struct arc_property *p=NULL;
    
    if(num_qnames == 1)
    {
	char *p_name = next_token(&qualifier);
	
	pthread_mutex_lock(&a_m->sys_lock);
	p = sys_find_property(a_m->sys, p_name);
	pthread_mutex_unlock(&a_m->sys_lock);
	
	if(!p)
	{
	    p = malloc(sizeof(struct arc_property));
	    init_int_property(p,p_name,val);
	    pthread_mutex_lock(&a_m->sys_lock);
	    sys_add_property(a_m->sys,p);
	    pthread_mutex_unlock(&a_m->sys_lock);
	    ret = true;
	}
	else
	{
	    
	    if(p->type == INT)
	    {
		pthread_mutex_lock(&a_m->sys_lock);
		p->v_int = val;
		pthread_mutex_unlock(&a_m->sys_lock);
		ret = true;
	    }
	    else
	    {
		ret = false;
	    }
	}
	
	free(p_name);
    }
    else if(num_qnames == 2)
    {
	char *c_name = next_token(&qualifier);
	struct arc_component *c = NULL;
	pthread_mutex_lock(&a_m->sys_lock);
	c = sys_find_component(a_m->sys, c_name);
	pthread_mutex_unlock(&a_m->sys_lock);
	if(!c)
	{
	    ret = false;   
	}
	else
	{
	    char *p_name = next_token(&qualifier);
	    pthread_mutex_lock(&a_m->sys_lock);
	    p = comp_find_property(c, p_name);
	    pthread_mutex_unlock(&a_m->sys_lock);
	    if(!p)
	    {
		p = malloc(sizeof(struct arc_property));
		init_int_property(p,p_name,val);
		pthread_mutex_lock(&a_m->sys_lock);
		comp_add_property(c,p);
		pthread_mutex_unlock(&a_m->sys_lock);
		ret = true;
	    }
	    else
	    {
		if(p->type == INT)
		{
		    pthread_mutex_lock(&a_m->sys_lock);
		    p->v_int = val;
		    pthread_mutex_unlock(&a_m->sys_lock);
		    ret = true;
		}
		else
		{
		    ret = false;
		}
	    }
	    free(p_name);	    
	}
	free(c_name);
    }
    else
    {
	char *c_name = next_token(&qualifier);
	struct arc_component *c = NULL;
	pthread_mutex_lock(&a_m->sys_lock);
	c = sys_find_component(a_m->sys, c_name);
	pthread_mutex_unlock(&a_m->sys_lock);
	if(!c)
	{
	    ret = false;   
	}
	else
	{
	    char *i_name = next_token(&qualifier);
	    struct arc_interface *i = NULL;
	    pthread_mutex_lock(&a_m->sys_lock);
	    i = comp_find_iface(c, i_name);
	    pthread_mutex_unlock(&a_m->sys_lock);
	    if(!i)
	    {
		ret = false;
	    }
	    else
	    {
		char *p_name = next_token(&qualifier);
		pthread_mutex_lock(&a_m->sys_lock);
		p = iface_find_property(i, p_name);
		pthread_mutex_unlock(&a_m->sys_lock);
		if(!p)
		{
		    p = malloc(sizeof(struct arc_property));
		    init_int_property(p,p_name,val);
		    pthread_mutex_lock(&a_m->sys_lock);
		    iface_add_property(i,p);
		    pthread_mutex_unlock(&a_m->sys_lock);
		    ret = true;
		}
		else
		{
		    if(p->type == INT)
		    {
			pthread_mutex_lock(&a_m->sys_lock);
			p->v_int = val;
			pthread_mutex_unlock(&a_m->sys_lock);
			ret = true;
		    }
		    else
		    {
			ret = false;
		    }
		}
		free(p_name);	    
	    }
	    free(i_name);
	}
	free(c_name);
    }
    return ret;
}
bool arc_model_assign_property_dbl(struct arc_model *a_m, char *qual_name, double val)
{

    bool ret;
    struct tokenizer qualifier;
    tokenizer_init(&qualifier, qual_name, ":");
    int num_qnames = count_tokens(&qualifier);

    struct arc_property *p=NULL;
    
    if(num_qnames == 1)
    {
	char *p_name = next_token(&qualifier);
	
	pthread_mutex_lock(&a_m->sys_lock);
	p = sys_find_property(a_m->sys, p_name);
	pthread_mutex_unlock(&a_m->sys_lock);
	
	if(!p)
	{
	    p = malloc(sizeof(struct arc_property));
	    init_dbl_property(p,p_name,val);
	    pthread_mutex_lock(&a_m->sys_lock);
	    sys_add_property(a_m->sys,p);
	    pthread_mutex_unlock(&a_m->sys_lock);
	    ret = true;
	}
	else
	{
	    
	    if(p->type == DOUBLE)
	    {
		pthread_mutex_lock(&a_m->sys_lock);
		p->v_dbl = val;
		pthread_mutex_unlock(&a_m->sys_lock);
		ret = true;
	    }
	    else
	    {
		ret = false;
	    }
	}
	
	free(p_name);
    }
    else if(num_qnames == 2)
    {
	char *c_name = next_token(&qualifier);
	struct arc_component *c = NULL;
	pthread_mutex_lock(&a_m->sys_lock);
	c = sys_find_component(a_m->sys, c_name);
	pthread_mutex_unlock(&a_m->sys_lock);
	if(!c)
	{
	    ret = false;   
	}
	else
	{
	    char *p_name = next_token(&qualifier);
	    pthread_mutex_lock(&a_m->sys_lock);
	    p = comp_find_property(c, p_name);
	    pthread_mutex_unlock(&a_m->sys_lock);
	    if(!p)
	    {
		p = malloc(sizeof(struct arc_property));
		init_dbl_property(p,p_name,val);
		pthread_mutex_lock(&a_m->sys_lock);
		comp_add_property(c,p);
		pthread_mutex_unlock(&a_m->sys_lock);
		ret = true;
	    }
	    else
	    {
		if(p->type == DOUBLE)
		{
		    pthread_mutex_lock(&a_m->sys_lock);
		    p->v_dbl = val;
		    pthread_mutex_unlock(&a_m->sys_lock);
		    ret = true;
		}
		else
		{
		    ret = false;
		}
	    }
	    free(p_name);	    
	}
	free(c_name);
    }
    else
    {
	char *c_name = next_token(&qualifier);
	struct arc_component *c = NULL;
	pthread_mutex_lock(&a_m->sys_lock);
	c = sys_find_component(a_m->sys, c_name);
	pthread_mutex_unlock(&a_m->sys_lock);
	if(!c)
	{
	    ret = false;   
	}
	else
	{
	    char *i_name = next_token(&qualifier);
	    struct arc_interface *i = NULL;
	    pthread_mutex_lock(&a_m->sys_lock);
	    i = comp_find_iface(c, i_name);
	    pthread_mutex_unlock(&a_m->sys_lock);
	    if(!i)
	    {
		ret = false;
	    }
	    else
	    {
		char *p_name = next_token(&qualifier);
		pthread_mutex_lock(&a_m->sys_lock);
		p = iface_find_property(i, p_name);
		pthread_mutex_unlock(&a_m->sys_lock);
		if(!p)
		{
		    p = malloc(sizeof(struct arc_property));
		    init_dbl_property(p,p_name,val);
		    pthread_mutex_lock(&a_m->sys_lock);
		    iface_add_property(i,p);
		    pthread_mutex_unlock(&a_m->sys_lock);
		    ret = true;
		}
		else
		{
		    if(p->type == DOUBLE)
		    {
			pthread_mutex_lock(&a_m->sys_lock);
			p->v_int = val;
			pthread_mutex_unlock(&a_m->sys_lock);
			ret = true;
		    }
		    else
		    {
			ret = false;
		    }
		}
		free(p_name);	    
	    }
	    free(i_name);
	}
	free(c_name);
    }
    return ret;
}
bool arc_model_assign_property_str(struct arc_model *a_m, char *qual_name, char *val)
{
    bool ret = false;

    struct tokenizer qualifier;
    tokenizer_init(&qualifier, qual_name, ":");
    int num_qnames = count_tokens(&qualifier);

    struct arc_property *p=NULL;
    
    if(num_qnames == 1)
    {
	char *p_name = next_token(&qualifier);
	
	pthread_mutex_lock(&a_m->sys_lock);
	p = sys_find_property(a_m->sys, p_name);
	pthread_mutex_unlock(&a_m->sys_lock);
	
	if(!p)
	{
	    p = malloc(sizeof(struct arc_property));
	    init_str_property(p,p_name,val);
	    pthread_mutex_lock(&a_m->sys_lock);
	    sys_add_property(a_m->sys,p);
	    pthread_mutex_unlock(&a_m->sys_lock);
	    ret = true;
	}
	else
	{
	    if(p->type == STRING)
	    {
		pthread_mutex_lock(&a_m->sys_lock);
		if(p->v_str)
		{
		    free(p->v_str);
		    
		}
		p->v_str = malloc(strlen(val)+1);
		p->v_str[strlen(val)]='\0';
		strcpy(p->v_str, val);
		pthread_mutex_unlock(&a_m->sys_lock);
		ret = true;
	    }
	    else
	    {
		ret = false;
	    }
	}
	
	free(p_name);
    }
    else if(num_qnames == 2)
    {
	char *c_name = next_token(&qualifier);
	struct arc_component *c = NULL;
	pthread_mutex_lock(&a_m->sys_lock);
	c = sys_find_component(a_m->sys, c_name);
	pthread_mutex_unlock(&a_m->sys_lock);
	if(!c)
	{
	    ret = false;   
	}
	else
	{
	    char *p_name = next_token(&qualifier);
	    pthread_mutex_lock(&a_m->sys_lock);
	    p = comp_find_property(c, p_name);
	    pthread_mutex_unlock(&a_m->sys_lock);
	    if(!p)
	    {
		p = malloc(sizeof(struct arc_property));
		init_str_property(p,p_name,val);
		pthread_mutex_lock(&a_m->sys_lock);
		comp_add_property(c,p);
		pthread_mutex_unlock(&a_m->sys_lock);
		ret = true;
	    }
	    else
	    {
		if(p->type == STRING)
		{
		    pthread_mutex_lock(&a_m->sys_lock);
		    if(p->v_str)
		    {
			free(p->v_str);
		    }
		    p->v_str = malloc(strlen(val)+1);
		    p->v_str[strlen(val)]='\0';
		    strcpy(p->v_str,val);
		    pthread_mutex_unlock(&a_m->sys_lock);
		    ret = true;
		}
		else
		{
		    ret = false;
		}
	    }
	    free(p_name);	    
	}
	free(c_name);
    }
    else
    {
	
	char *c_name = next_token(&qualifier);
	struct arc_component *c = NULL;
	pthread_mutex_lock(&a_m->sys_lock);
	c = sys_find_component(a_m->sys, c_name);
	pthread_mutex_unlock(&a_m->sys_lock);
	if(!c)
	{
	    
	    ret = false;   
	}
	else
	{
	    char *i_name = next_token(&qualifier);
	    struct arc_interface *i = NULL;
	    pthread_mutex_lock(&a_m->sys_lock);
	    i = comp_find_iface(c, i_name);
	    pthread_mutex_unlock(&a_m->sys_lock);
	    if(!i)
	    {
		
		ret = false;
	    }
	    else
	    {
		char *p_name = next_token(&qualifier);
		pthread_mutex_lock(&a_m->sys_lock);
		p = iface_find_property(i, p_name);
		pthread_mutex_unlock(&a_m->sys_lock);
		if(!p)
		{
		    p = malloc(sizeof(struct arc_property));
		    init_str_property(p,p_name,val);
		    pthread_mutex_lock(&a_m->sys_lock);
		    iface_add_property(i,p);
		    pthread_mutex_unlock(&a_m->sys_lock);
		    ret = true;
		}
		else
		{
		    if(p->type == STRING)
		    {
			pthread_mutex_lock(&a_m->sys_lock);
			if(p->v_str)
			{
			    free(p->v_str);
			}
			p->v_str = malloc(strlen(val)+1);
			p->v_str[strlen(val)]='\0';		    
			strcpy(p->v_str, val);
			pthread_mutex_unlock(&a_m->sys_lock);
			ret = true;
		    }
		    else
		    {
			ret = false;
		    }
		}
		free(p_name);	    
	    }
	    free(i_name);
	}
	free(c_name);
    }
    return ret;
}

bool arc_model_remove_property(struct arc_model *a_m, char *qual_name)
{
    bool ret = false;
    struct tokenizer qualifier;
    tokenizer_init(&qualifier, qual_name, ":");
    int num_qnames = count_tokens(&qualifier);

    struct arc_property *p=NULL;
    
    if(num_qnames == 1)
    {
	char *p_name = next_token(&qualifier);
	
	pthread_mutex_lock(&a_m->sys_lock);
	p = sys_rem_property(a_m->sys, p_name);
	pthread_mutex_unlock(&a_m->sys_lock);
	
	if(p)
	{
	    destroy_property(p);
	    free(p);
	    ret = true;
	}	
	free(p_name);
    }
    else if(num_qnames == 2)
    {
	char *c_name = next_token(&qualifier);
	struct arc_component *c = NULL;
	pthread_mutex_lock(&a_m->sys_lock);
	c = sys_find_component(a_m->sys, c_name);
	pthread_mutex_unlock(&a_m->sys_lock);
	if(!c)
	{
	    ret = false;   
	}
	else
	{
	    char *p_name = next_token(&qualifier);
	    pthread_mutex_lock(&a_m->sys_lock);
	    p = comp_rem_property(c, p_name);
	    pthread_mutex_unlock(&a_m->sys_lock);
	    if(p)
	    {
		destroy_property(p);
		free(p);
		ret = true;
	    }
	    free(p_name);	    
	}
	free(c_name);
    }
    else
    {
	char *c_name = next_token(&qualifier);
	struct arc_component *c = NULL;
	pthread_mutex_lock(&a_m->sys_lock);
	c = sys_find_component(a_m->sys, c_name);
	pthread_mutex_unlock(&a_m->sys_lock);
	if(!c)
	{
	    ret = false;   
	}
	else
	{
	    char *i_name = next_token(&qualifier);
	    struct arc_interface *i = NULL;
	    pthread_mutex_lock(&a_m->sys_lock);
	    i = comp_find_iface(c, i_name);
	    pthread_mutex_unlock(&a_m->sys_lock);
	    if(!i)
	    {
		ret = false;
	    }
	    else
	    {
		char *p_name = next_token(&qualifier);
		pthread_mutex_lock(&a_m->sys_lock);
		p = iface_rem_property(i, p_name);
		pthread_mutex_unlock(&a_m->sys_lock);
		if(p)
		{
		    destroy_property(p);
		    free(p);
		    ret = true;
		}
		free(p_name);	    
	    }
	    free(i_name);
	}
	free(c_name);
    }
    return ret;
}

const struct arc_prop_type_value arc_model_get_property(struct arc_model *a_m, char *qual_name)
{
    struct arc_prop_type_value ret = { .type = INVALID, .v_int = 0 };

    struct tokenizer qualifier;
    tokenizer_init(&qualifier, qual_name, ":");
    int num_qnames = count_tokens(&qualifier);
    struct arc_property *p=NULL;
    
    if(num_qnames == 1)
    {
	char *p_name = next_token(&qualifier);
	
	pthread_mutex_lock(&a_m->sys_lock);
	p = sys_find_property(a_m->sys, p_name);
	pthread_mutex_unlock(&a_m->sys_lock);
	
	if(p)
	{
	    ret.type = p->type;
	    if(ret.type == INT)
	    {
		ret.v_int = p->v_int;
	    }
	    else if(ret.type == DOUBLE)
	    {
		ret.v_dbl = p->v_dbl;
	    }
	    else if(ret.type == STRING)
	    {
		ret.v_str= strdup(p->v_str);
	    }
	    
	}	
	free(p_name);
    }
    else if(num_qnames == 2)
    {
	char *c_name = next_token(&qualifier);
	struct arc_component *c = NULL;
	pthread_mutex_lock(&a_m->sys_lock);
	c = sys_find_component(a_m->sys, c_name);
	pthread_mutex_unlock(&a_m->sys_lock);
	if(c)
	{
	    char *p_name = next_token(&qualifier);
	    pthread_mutex_lock(&a_m->sys_lock);
	    p = comp_find_property(c, p_name);
	    pthread_mutex_unlock(&a_m->sys_lock);
	    if(p)
	    {
		ret.type = p->type;
		if(ret.type == INT)
		{
		    ret.v_int = p->v_int;
		}
		else if(ret.type == DOUBLE)
		{
		    ret.v_dbl = p->v_dbl;
		}
		else if(ret.type == STRING)
		{
		    ret.v_str= strdup(p->v_str);
		}
	    }
	    free(p_name);
	}	
	free(c_name);
    }
    else if(num_qnames == 2)
    {
	char *c_name = next_token(&qualifier);
	struct arc_component *c = NULL;
	pthread_mutex_lock(&a_m->sys_lock);
	c = sys_find_component(a_m->sys, c_name);
	pthread_mutex_unlock(&a_m->sys_lock);
	if(c)
	{
	    char *p_name = next_token(&qualifier);
	    pthread_mutex_lock(&a_m->sys_lock);
	    p = comp_find_property(c, p_name);
	    if(p)
	    {
		ret.type = p->type;
		if(ret.type == INT)
		{
		    ret.v_int = p->v_int;
		}
		else if(ret.type == DOUBLE)
		{
		    ret.v_dbl = p->v_dbl;
		}
		else if(ret.type == STRING)
		{
		    ret.v_str = strdup(p->v_str);
		}
	    }
	    pthread_mutex_unlock(&a_m->sys_lock);
	    free(p_name);
	}
	free(c_name);
	
    }
    else if(num_qnames == 2)
    {
	char *c_name = next_token(&qualifier);
	struct arc_component *c = NULL;
	pthread_mutex_lock(&a_m->sys_lock);
	c = sys_find_component(a_m->sys, c_name);
	pthread_mutex_unlock(&a_m->sys_lock);
	if(c)
	{
	    char *p_name = next_token(&qualifier);
	    pthread_mutex_lock(&a_m->sys_lock);
	    p = comp_find_property(c, p_name);
	    
	    if(p)
	    {
		ret.type = p->type;
		if(ret.type == INT)
		{
		    ret.v_int = p->v_int;
		}
		else if(ret.type == DOUBLE)
		{
		    ret.v_dbl = p->v_dbl;
		}
		else if(ret.type == STRING)
		{
		    ret.v_str = strdup(p->v_str);
		}
	    }
	    pthread_mutex_unlock(&a_m->sys_lock);
	    free(p_name);	    
	}
	free(c_name);
    }
    else
    {
	char *c_name = next_token(&qualifier);
	struct arc_component *c = NULL;
	pthread_mutex_lock(&a_m->sys_lock);
	c = sys_find_component(a_m->sys, c_name);
	pthread_mutex_unlock(&a_m->sys_lock);
	if(c)
	{
	    char *i_name = next_token(&qualifier);
	    struct arc_interface *i = NULL;
	    pthread_mutex_lock(&a_m->sys_lock);
	    i = comp_find_iface(c, i_name);
	    pthread_mutex_unlock(&a_m->sys_lock);
	    if(i)
	    {
		char *p_name = next_token(&qualifier);
		pthread_mutex_lock(&a_m->sys_lock);
		p = iface_find_property(i, p_name);

		if(p)
		{
		    ret.type = p->type;
		    if(ret.type == INT)
		    {
			ret.v_int = p->v_int;
		    }
		    else if(ret.type == DOUBLE)
		    {
			ret.v_dbl = p->v_dbl;
		    }
		    else if(ret.type == STRING)
		    {
			ret.v_str = p->v_str;
		    }
		}
		pthread_mutex_unlock(&a_m->sys_lock);
		free(p_name);	    
	    }
	    free(i_name);
	}
	free(c_name);
    }
    return ret;
}
