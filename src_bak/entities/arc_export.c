#include "arc_export.h"

#include <stdbool.h>

bool arc_dump_property(struct string_buffer *s_b, struct arc_property *a_p)
{
    if(a_p->type==INT)
    {
	return string_buffer_add_fmt(s_b, "Property %s = %d\n",a_p->name, a_p->v_int);
    }
    else if(a_p->type==DOUBLE)
    {
	return string_buffer_add_fmt(s_b, "Property %s = %f\n",a_p->name, a_p->v_dbl);
    }
    else
    {
	return string_buffer_add_fmt(s_b, "Property %s = '%s'\n",a_p->name, a_p->v_str);
    }
    
    
}

bool arc_dump_interface(struct string_buffer *s_b, struct arc_interface *a_i)
{
    bool res = false;
    res = string_buffer_add_fmt(s_b, "Interface %s {\n",a_i->if_name);
    
    //make sure we've not failed to add to the buffer
    for(size_t i = 0; i < a_i->num_properties && res;i++)
    {
	res = arc_dump_property(s_b, a_i->properties[i]);
    }
    
    if(res)
    {
	return string_buffer_add_str(s_b, "}\n");
    }
    return false;
}


bool arc_dump_component(struct string_buffer *s_b, struct arc_component *a_c)
{
    bool res = false;
    res = string_buffer_add_fmt(s_b, "Component %s {\n",a_c->comp_name);
    
    //make sure we've not failed to add to the buffer
    for(size_t i = 0; i < a_c->num_interfaces && res;i++)
    {
	res = arc_dump_interface(s_b, a_c->interfaces[i]);
    }

    //make sure we've not failed to add to the buffer
    for(size_t i = 0; i < a_c->num_properties && res;i++)
    {
	res = arc_dump_property(s_b, a_c->properties[i]);
    }
    
    if(res)
    {
	return string_buffer_add_str(s_b, "}\n");
    }
    return false;
}

bool arc_dump_invocation(struct string_buffer *s_b, struct arc_invocation *a_i)
{
    bool res = false;
    res = string_buffer_add_str(s_b, "Invocation {\n");
    res = string_buffer_add_fmt(s_b, "Source %s {\n",a_i->from->comp_name);
    res = string_buffer_add_fmt(s_b, "Target %s {\n",a_i->to->comp_name);
    res = string_buffer_add_fmt(s_b, "Interface %s {\n",a_i->iface->if_name);
    
    if(res)
    {
	return string_buffer_add_str(s_b, "}\n");
    }
    return false;
}
bool arc_dump_system(struct string_buffer *s_b, struct arc_system *a_s)
{
    bool res = false;
    res = string_buffer_add_fmt(s_b, "System %s {\n",a_s->sys_name);
    
    //make sure we've not failed to add to the buffer
    for(size_t i = 0; i < a_s->num_components && res;i++)
    {
	res = arc_dump_component(s_b, a_s->components[i]);
    }

    //make sure we've not failed to add to the buffer
    for(size_t i = 0; i < a_s->num_invocations && res;i++)
    {
	res = arc_dump_invocation(s_b, a_s->invocations[i]);
    }
    
    //make sure we've not failed to add to the buffer
    for(size_t i = 0; i < a_s->num_properties && res;i++)
    {
	res = arc_dump_property(s_b, a_s->properties[i]);
    }
    
    if(res)
    {
	return string_buffer_add_str(s_b, "}\n");
    }
    return false;
}
