#define _XOPEN_SOURCE 600

#include "arc_condition.h"
#include <string.h>
#include <stdlib.h>
#include <stdio.h>


void init_simple_condition(struct arc_simple_condition *cond,
			   struct arc_property *prop,
			   enum ARC_COMPARISON_OPERATOR op,
			   union property_value value)
{
    cond->prop=prop;
    cond->op=op;
    if(prop->type == INT)
    {  
       cond->v_int=value.v_int;
    }
    else if(prop->type == DOUBLE)
    {
	cond->v_dbl = value.v_dbl;
    }
    else
    {
	cond->v_str = strdup(value.v_str);
    }
}

bool arc_simple_cond_eval(struct arc_simple_condition *cond,
			   struct arc_system *sys)
{
    
    //check for type
    if(cond->prop->type==INT)
    {
	int prop_value=cond->prop->v_int;
	int const_value=cond->v_int;
	switch(cond->op)
	{
	case ARC_COND_LT: return prop_value <  const_value;
	case ARC_COND_LE: return prop_value <= const_value;
	case ARC_COND_GT: return prop_value >  const_value;
	case ARC_COND_GE: return prop_value >= const_value;
	case ARC_COND_EQ: return prop_value == const_value;
	}
    }
    else if(cond->prop->type==DOUBLE)
    {
	double prop_value=cond->prop->v_dbl;
	double const_value=cond->v_dbl;
	switch(cond->op)
	{
	case ARC_COND_LT: return prop_value <  const_value;
	case ARC_COND_LE: return prop_value <= const_value;
	case ARC_COND_GT: return prop_value >  const_value;
	case ARC_COND_GE: return prop_value >= const_value;
	case ARC_COND_EQ: return prop_value == const_value;
	}
    }
    else if(cond->prop->type==STRING)
    {
	char *prop_value=cond->prop->v_str;
	char *const_value=cond->v_str;
	switch(cond->op)
	{
	case ARC_COND_LT: return strcmp(prop_value,const_value)<0;
	case ARC_COND_LE: return strcmp(prop_value,const_value)<=0;
	case ARC_COND_GT: return strcmp(prop_value,const_value)>0;
	case ARC_COND_GE: return strcmp(prop_value,const_value)>=0;
	case ARC_COND_EQ: return strcmp(prop_value,const_value)==0;
	}
    }
}

void destroy_simple_condition(struct arc_simple_condition *cond)
{
    if(cond->prop->type==STRING)
    {
	free(cond->v_str);
    }
}


void init_complext_condition(struct arc_complex_condition *cond,
			     struct arc_simple_condition *simple_conds,
			     enum ARC_BOOLEAN_OPERATOR *ops,
			     size_t num_simple_conditions)
{
    cond->simple_conds=simple_conds;
    cond->num_simple_conditions=num_simple_conditions;
    cond->ops=ops;
}




bool arc_complex_cond_eval(struct arc_complex_condition *cond, struct arc_system *sys)
{
    if(cond->num_simple_conditions>0)
    {
	bool res=arc_simple_cond_eval(&cond->simple_conds[0],sys);
//	printf("Initial condition evaluates to:%d\n",res);
	for(int i=1;i<cond->num_simple_conditions;i++)
	{
	    if(cond->ops[i-1]==ARC_COND_OR)
	    {
		res|= arc_simple_cond_eval(&cond->simple_conds[i],sys);
	    }
	    else if(cond->ops[i-1]==ARC_COND_AND)
	    {
		res&= arc_simple_cond_eval(&cond->simple_conds[i],sys);
	    }
//	    printf("Condition is now:%d\n",res);
	}
	return res;
    }
    else
    {
	fprintf(stderr,"No simple conditions passed, trivially returning true");
	return true;
    }

				  
}


void destroy_complex_condition(struct arc_complex_condition *cond)
{
    for(int i=0;i<cond->num_simple_conditions;i++)
    {
	destroy_simple_condition(&cond->simple_conds[i]);
    }
    free(cond->simple_conds);
    free(cond->ops);
}

