#include "arc_system.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

void  init_system(struct arc_system *sys, char *sys_name)
{
    sys->sys_name=malloc(strlen(sys_name)+1);
    strcpy(sys->sys_name,sys_name);
    
    sys->properties=malloc(NUM_PROPERTIES_INIT*sizeof(struct arc_property*));
    sys->num_properties=0;
    sys->max_properties=NUM_PROPERTIES_INIT;

    sys->invocations=malloc(NUM_INVOCATIONS_INIT*sizeof(struct arc_invocation*));
    sys->num_invocations=0;
    sys->max_invocations=NUM_INVOCATIONS_INIT;
    
    sys->components=malloc(NUM_COMPONENTS_INIT*sizeof(struct arc_component*));
    sys->num_components=0;
    sys->max_components=NUM_COMPONENTS_INIT;
}

void destroy_system(struct arc_system *sys)
{
    free(sys->sys_name);
    free(sys->properties);
    free(sys->components);
    free(sys->invocations);
}

void sys_add_property(struct arc_system *sys, struct arc_property *prop)
{
    if(sys->num_properties>=sys->max_properties)
    {
        sys->max_properties*=2;
	sys->properties=realloc(sys->properties,
				sys->max_properties*sizeof(struct arc_property*));
    }
    sys->properties[sys->num_properties]=prop;
    sys->num_properties++;
}

void sys_add_component(struct arc_system *sys, struct arc_component *comp)
{
    if(sys->num_components>=sys->max_components)
    {
        sys->max_components*=2;
	sys->components=realloc(sys->components,
				sys->max_components*sizeof(struct arc_component*));
	
    }
    sys->components[sys->num_components]=comp;
    sys->num_components++;
}

void sys_add_invocation(struct arc_system *sys, struct arc_invocation *invo)
{
    if(sys->num_invocations>=sys->max_invocations)
    {
        sys->max_invocations*=2;
	sys->invocations=realloc(sys->invocations,
				 sys->max_invocations*sizeof(struct arc_invocation*));
    }
    sys->invocations[sys->num_invocations]=invo;
    sys->num_invocations++;
    
}

struct arc_component* sys_find_component(struct arc_system *sys,
					 char *comp_name)
{
    for(int i=0;i<sys->num_components;i++)
    {
	struct arc_component *c = sys->components[i];
	
	if(c &&  strcmp(c->comp_name,comp_name)==0)
	{
	    return c;
	}
	
    }
    return NULL;
}

struct arc_invocation* sys_find_invocation(struct arc_system *sys, char *from,
					  char *to, char *if_name)
{
    for(int i=0;i<sys->num_invocations;i++)
    {

	struct arc_invocation *i_v = sys->invocations[i];
	if(i_v)
	{
	    if( strcmp(i_v->from->comp_name,from)==0
		&& strcmp(i_v->to->comp_name,to)==0
		&& strcmp(i_v->iface->if_name,if_name)==0
		)
		
	    {
		
		return sys->invocations[i];;
	    }
	}
	
    }
    return NULL;
}

struct arc_property* sys_find_property(struct arc_system *sys,
					 char *prop_name)
{
       for(int i=0;i<sys->num_properties;i++)
    {
	if(sys->properties[i] && sys->properties[i]->name
	   && strcmp(sys->properties[i]->name,prop_name)==0)
	{
	    return sys->properties[i];
	}
	
    }
    return NULL;
}


struct arc_property* sys_rem_property(struct arc_system *sys, char *prop_name)
{
    for(int i=0;i<sys->num_properties;i++)
    {
	if(sys->properties[i] && sys->properties[i]->name
	   && strcmp(sys->properties[i]->name,prop_name)==0)
	{
	    struct arc_property *ret=sys->properties[i];

	    if(i != sys->num_properties - 1 )
	    {
	        sys->properties[i] = sys->properties[sys->num_properties-1];
	    }
	    sys->num_properties--;
	    
	    return ret;
	}
	
    }
    return NULL;

}

struct arc_component*  sys_rem_component(struct arc_system *sys, char *comp_name)
{
    for(int i=0;i<sys->num_components;i++)
    {
	if(sys->components[i] && strcmp(sys->components[i]->comp_name,comp_name)==0)
	{
	    struct arc_component *ret=sys->components[i];
	    
	    for(int j=0;j<sys->num_invocations;j++)
	    {
		//If the removed component participated in this invocation remove it
		struct arc_invocation *invo=sys->invocations[j];
		if(invo && invo->from && invo->from==ret)
		{
		    sys_rem_invocation(sys,invo->from->comp_name,invo->to->comp_name,invo->iface->if_name);
		    free(invo);
		    sys->invocations[j]=NULL;
		}
		else if(invo && invo->to && invo->to==ret)
		{
		    sys_rem_invocation(sys,invo->from->comp_name,invo->to->comp_name,invo->iface->if_name);
		    free(invo);
		    sys->invocations[j]=NULL;
		}
	    }

	    for(int j=0;j<ret->num_properties;j++)
	    {
		if(ret->properties[j])
		{
		    struct arc_property *rem_prop=comp_rem_property(ret,ret->properties[j]->name);
		    destroy_property(rem_prop);
		    free(rem_prop);
		}
	    }
	    for(int j=0;j<ret->num_interfaces;j++)
	    {
		if(ret->interfaces[j])
		{
		    for(int k=0;k<ret->interfaces[j]->num_properties;k++)
		    {
			if(ret->interfaces[j]->properties[k])
			{
			    destroy_property(ret->interfaces[j]->properties[k]);
			    free(ret->interfaces[j]->properties[k]);
			}
		    }
		    
		    struct arc_interface *rem_iface=comp_rem_iface(ret,ret->interfaces[j]->if_name);
		    destroy_interface(rem_iface);
		    free(rem_iface);
		}
	    }

	    //Also remove any invocations the component took part in!
	    for(int j=0;j<sys->num_invocations;j++)
	    {
		struct arc_invocation *i_v=sys->invocations[j];
		if(i_v && (ret == i_v->from || ret == i_v->to))
		{
		    free(sys_rem_invocation(sys,i_v->from->comp_name,i_v->to->comp_name,i_v->iface->if_name));
		}
	    }
	    
	    //should I be destroying this here?
	    destroy_component(ret);

	    if(i != sys->num_components - 1 )
	    {
		sys->components[i] = sys->components[sys->num_components-1];
	    }
	    sys->num_components--;
	    
	    return ret;
	}
	
    }
    return NULL;
}

struct arc_invocation* sys_rem_invocation(struct arc_system *sys, char *from,
					  char *to, char *if_name)
{
    for(int i=0;i<sys->num_invocations;i++)
    {
	if(sys->invocations[i])
	{
	    if( strcmp(sys->invocations[i]->from->comp_name,from)==0
		&& strcmp(sys->invocations[i]->to->comp_name,to)==0
		&& strcmp(sys->invocations[i]->iface->if_name,if_name)==0
		)
	    {
		struct arc_invocation *ret=sys->invocations[i];


		if(i != sys->num_invocations - 1 )
		{
		    sys->invocations[i] = sys->invocations[sys->num_invocations-1];
		}
		sys->num_invocations--;
		
		return ret;
	    }
	}
	
    }
    return NULL;
}


void print_system(struct arc_system *sys)
{
    printf("SYS: %s{\n",sys->sys_name);

    for(int i=0;i<sys->num_components;i++)
    {
	if(sys->components[i])
	print_component(sys->components[i]);
    }
    
    for(int i=0;i<sys->num_invocations;i++)
    {
	if(sys->invocations[i])
	print_invocation(sys->invocations[i]);
    }
    
    for(int i=0;i<sys->num_properties;i++)
    {
	if(sys->properties[i])
	print_property(sys->properties[i]);
    }
    printf("}\n");
}

void sys_dump_to_file(struct arc_system *sys, char *fname)
{
    fprintf(stderr, "Dumping to file not supported yet!\n");
    return;
}



struct arc_system *clone_system(struct arc_system *sys)
{
    struct arc_system *copy=malloc(sizeof (struct arc_system));
    init_system(copy,sys->sys_name);
    for(int i=0;i<sys->num_properties;i++)
    {
	sys_add_property(copy,clone_property(sys->properties[i]));
    }

    for(int i=0;i<sys->num_components;i++)
    {
	sys_add_component(copy,clone_component(sys->components[i]));
    }

    for(int i=0;i<sys->num_invocations;i++)
    {
	
	struct arc_component *from=sys_find_component(copy,sys->invocations[i]->from->comp_name);
	if(!from)
	{
	    fprintf(stderr,"Could not find source component %s\n",sys->invocations[i]->from->comp_name);
	    exit(-1);
	}
	struct arc_component *to=sys_find_component(copy,sys->invocations[i]->to->comp_name);
	if(!to)
	{
	    fprintf(stderr,"Could not find dest component %s\n",sys->invocations[i]->to->comp_name);
	    exit(-1);
	}
		
	struct arc_interface *iface= comp_find_iface(to,sys->invocations[i]->iface->if_name);
	if(!to)
	{
	    fprintf(stderr,"Could not find interface %s\n",sys->invocations[i]->iface->if_name);
	    exit(-1);
	}
	
	struct arc_invocation *inv=malloc(sizeof(struct arc_invocation));
	init_invocation(inv,iface,from,to);
	sys_add_invocation(copy,inv);
    }

    return copy;
}
//assumes everything is allocated in the heap via malloc
void cleanup_sys_clone(struct arc_system *copy)
{
    for(int i=0;i<copy->num_properties;i++)
    {
	destroy_property(copy->properties[i]);
	free(copy->properties[i]);
    }
    for(int i=0;i<copy->num_components;i++)
    {
	for(int j=0;j<copy->components[i]->num_properties;j++)
	{
	    destroy_property(copy->components[i]->properties[j]);
	    free(copy->components[i]->properties[j]);
	}

	for(int j=0;j<copy->components[i]->num_interfaces;j++)
	{
	    for(int k=0;k<copy->components[i]->interfaces[j]->num_properties;k++)
	    {
		if(copy->components[i]->interfaces[j]->properties[k])
		{
		    destroy_property(copy->components[i]->interfaces[j]->properties[k]);
		    free(copy->components[i]->interfaces[j]->properties[k]);
		}
	    }
	    destroy_interface(copy->components[i]->interfaces[j]);
	    free(copy->components[i]->interfaces[j]);
	}
	
	
	destroy_component(copy->components[i]);
	free(copy->components[i]);
    }

    for(int i=0;i<copy->num_invocations;i++)
    {
	if(copy->invocations[i])
	{
	    free(copy->invocations[i]);
	}
    }

    destroy_system(copy);
}




