#include "arc_component.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

void init_component(struct arc_component *comp, char *comp_name)
{
    comp->comp_name=malloc(strlen(comp_name)+1);
    strcpy(comp->comp_name,comp_name);
    
    comp->properties=malloc(NUM_PROPERTIES_INIT*sizeof(struct arc_property*));
    comp->num_properties=0;
    comp->max_properties=NUM_PROPERTIES_INIT;

    comp->interfaces=malloc(NUM_INTERFACES_INIT*sizeof(struct arc_interface*));
    comp->num_interfaces=0;
    comp->max_interfaces=NUM_PROPERTIES_INIT;
}

void destroy_component(struct arc_component *comp)
{
    free(comp->comp_name);
    free(comp->properties);
    free(comp->interfaces);
}

void comp_add_iface(struct arc_component *comp, struct arc_interface *iface)
{

    if(comp->num_interfaces>=comp->max_interfaces)
    {
        comp->max_interfaces*=2;
	comp->interfaces=realloc(comp->interfaces,
				      comp->max_interfaces*sizeof(struct arc_interface*));
	
    }
    comp->interfaces[comp->num_interfaces]=iface;
    comp->num_interfaces++;
    
    
}

void comp_add_property(struct arc_component *comp, struct arc_property *prop)
{

    if(comp->num_properties>=comp->max_properties)
    {
        comp->max_properties*=2;
	comp->properties=realloc(comp->properties,
				      comp->max_properties*sizeof(struct arc_property*));
	
    }
    comp->properties[comp->num_properties]=prop;
    comp->num_properties++;
    
}

struct arc_interface* comp_find_iface(struct arc_component *comp, char *iface_name)
{
    if(!comp)
	return NULL;
    
    for(int i=0;i<comp->num_interfaces;i++)
    {
	if( comp->interfaces[i] && strcmp(comp->interfaces[i]->if_name,iface_name)==0)
	{
	    return comp->interfaces[i];
	}
    }
    return NULL;
}
struct arc_property* comp_find_property(struct arc_component *comp, char *prop_name)
{
    for(int i=0;i<comp->num_properties;i++)
    {
	if(comp->properties[i] && strcmp(comp->properties[i]->name,prop_name)==0)
	{
	    return comp->properties[i];
	}
    }
    return NULL;
}

struct arc_interface* comp_rem_iface(struct arc_component *comp, char *iface_name)
{
    for(int i=0;i<comp->num_interfaces;i++)
    {
	if(comp->interfaces[i])
	{
	    if(strcmp(comp->interfaces[i]->if_name,iface_name)==0)
	    {
		struct arc_interface *ret=comp->interfaces[i];

		if(i != comp->num_interfaces - 1 )
		{
		    comp->interfaces[i] = comp->interfaces[comp->num_interfaces-1];
		}
		comp->num_interfaces--;
		
		return ret;
	    }
	}
    }
    return NULL;
}
struct arc_property* comp_rem_property(struct arc_component *comp, char *prop_name)
{
    for(int i=0;i<comp->num_properties;i++)
    {
	if(comp->properties[i])
	{
	    if(strcmp(comp->properties[i]->name,prop_name)==0)
	    {
		struct arc_property *ret=comp->properties[i];

		if(i != comp->num_properties - 1 )
		{
		    comp->properties[i] = comp->properties[comp->num_properties-1];
		}
		comp->num_properties--;
		return ret;
	    }
	}
    }
    return NULL;    
}


void print_component(struct arc_component *comp)
{
    printf("Component %s{\n",comp->comp_name);


    for(int i=0;i<comp->num_interfaces;i++)
    {
	if(comp->interfaces[i])
	{
	print_interface(comp->interfaces[i]);
	}
    }

    for(int i=0;i<comp->num_properties;i++)
    {
	if(comp->properties[i])
	{
	print_property(comp->properties[i]);
	}
    }

    
    printf("}\n");

}

struct arc_component *clone_component(struct arc_component *comp)
{
    struct arc_component *copy=malloc(sizeof(struct arc_component));

    init_component(copy,comp->comp_name);

    for(int i=0;i<comp->num_properties;i++)
    {
	if(comp->properties[i])
	    comp_add_property(copy, clone_property(comp->properties[i]));
    }

    for(int i=0;i<comp->num_interfaces;i++)
    {
	if(comp->interfaces[i])
	    comp_add_iface(copy, clone_interface(comp->interfaces[i]));
    }

    return copy;
}
