#include "arc_parse.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "tokenize.h"
#include "parse_common.h"


static int parse_system(char **tokens, size_t num_tokens, size_t *cur_token, struct arc_system *system);
static int parse_components(char **tokens, size_t num_tokens, size_t *cur_token, struct arc_system *system);
static int parse_invocations(char **tokens, size_t num_tokens, size_t *cur_token, struct arc_system *system);
static int parse_properties(char **tokens, size_t num_tokens, size_t *cur_token, void *entity, enum ARC_ENTITY_TYPE type );
static int parse_interfaces(char **tokens, size_t num_tokens, size_t *cur_token, struct arc_component *comp);
static struct arc_component* parse_component(char **tokens, size_t num_tokens, size_t *cur_token);
static struct arc_invocation* parse_invocation(char **tokens, size_t num_tokens, size_t *cur_token,struct arc_system *system);
static struct arc_interface* parse_interface(char **tokens, size_t num_tokens, size_t *cur_token);
static struct arc_property* parse_property(char **tokens, size_t num_tokens, size_t *cur_token);



struct arc_system* arc_parse(char *txt)
{
    
    struct arc_system *sys = malloc( sizeof(struct arc_system));
    struct tokenizer whitespace_tok;
    tokenizer_init(&whitespace_tok,txt," \n\t\r");
    int num_tokens=count_tokens(&whitespace_tok);
    char **tokens=malloc(num_tokens*sizeof(char*));
    for(int i=0;i<num_tokens;i++)
    {
	tokens[i]=next_token(&whitespace_tok);
    }
    size_t cur_token=0;
    if( parse_system(tokens,(size_t)num_tokens,&cur_token,sys)==0)
    {
	for(int i=0;i<num_tokens;i++)
	{
	    free(tokens[i]);
	}
	free(tokens);
	return sys;
    }
    
    return NULL;
    
}



static int parse_system(char **tokens, size_t num_tokens, size_t *cur_token, struct arc_system *system)
{

    if(consume(tokens[*cur_token],"System",cur_token))
    {
	char *sys_name=parse_name(tokens,num_tokens,cur_token);
	
	if(!sys_name)
	{
	    return -1;
	}
	
	init_system(system,sys_name);
	if(consume(tokens[*cur_token],"{",cur_token))
	{
	    parse_components(tokens,num_tokens,cur_token,system);

	    parse_invocations(tokens,num_tokens,cur_token,system);

	    parse_properties(tokens,num_tokens,cur_token,system,ARC_ENT_SYSTEM);
	    if(consume(tokens[*cur_token],"}",cur_token))
	    {
		//printf("System %s parse successful\n",sys_name);
		free(sys_name);
		return 0;
	    }
	}
	else
	{
	    fprintf(stderr,"Failed to consume %s\n",tokens[*cur_token]);
	}
    }
    
    return -1;    
}

static int parse_components(char **tokens, size_t num_tokens, size_t *cur_token, struct arc_system *system)
{

    struct arc_component *comp=parse_component(tokens,num_tokens,cur_token);
    while(comp!=NULL)
    {
	sys_add_component(system,comp);
	comp=parse_component(tokens,num_tokens,cur_token);
    }

    return 0;
}
static int parse_invocations(char **tokens, size_t num_tokens, size_t *cur_token, struct arc_system *system)
{

    struct arc_invocation *invo=parse_invocation(tokens,num_tokens,cur_token,system);
    while(invo!=NULL)
    {
	sys_add_invocation(system,invo);
	invo=parse_invocation(tokens,num_tokens,cur_token,system);
    }
}
static int parse_properties(char **tokens, size_t num_tokens, size_t *cur_token, void *entity, enum ARC_ENTITY_TYPE type )
{

    struct arc_property *prop=parse_property(tokens,num_tokens,cur_token);
    while(prop!=NULL)
    {
	switch(type)
	{
	case ARC_ENT_SYSTEM:
	    sys_add_property(entity,prop);
	    break;
	case ARC_ENT_COMPONENT:
	    comp_add_property(entity,prop);
	    break;
	case ARC_ENT_INTERFACE:
	    iface_add_property(entity,prop);
	    break;
	}
	prop=parse_property(tokens,num_tokens,cur_token);
    }
    return 0;
}
static struct arc_component* parse_component(char **tokens, size_t num_tokens, size_t *cur_token)
{

    if(consume(tokens[*cur_token],"Component",cur_token))
    {
	char * comp_name=parse_name(tokens,num_tokens,cur_token);
	if(!comp_name)	    
	    return NULL;
	struct arc_component *comp=malloc(sizeof(struct arc_component));
	init_component(comp,comp_name);
	if(consume(tokens[*cur_token],"{",cur_token))
	{
	    parse_interfaces(tokens,num_tokens,cur_token,comp);
	    parse_properties(tokens,num_tokens,cur_token,comp,ARC_ENT_COMPONENT);
	    if(consume(tokens[*cur_token],"}",cur_token))
	    {
		free(comp_name);
		return comp;
	    }
	}
	free(comp_name);
	free(comp);
    }
    return NULL;
}

static int parse_interfaces(char **tokens, size_t num_tokens, size_t *cur_token, struct arc_component *comp)
{

    struct arc_interface *iface=parse_interface(tokens,num_tokens,cur_token);
    while(iface!=NULL)
    {
	comp_add_iface(comp,iface);
	iface=parse_interface(tokens,num_tokens,cur_token);
    }
}

static struct arc_interface* parse_interface(char **tokens, size_t num_tokens, size_t *cur_token)
{

    if(consume(tokens[*cur_token],"Interface",cur_token))
    {
	char *if_name = parse_name(tokens,num_tokens,cur_token);
	if(!if_name)
	{
	    return NULL;
	}
	struct arc_interface *iface=malloc(sizeof(struct arc_interface));
	init_interface(iface,if_name);
	if(consume(tokens[*cur_token],"{",cur_token))
	{
	    parse_properties(tokens,num_tokens,cur_token,iface,ARC_ENT_INTERFACE);
	    if(consume(tokens[*cur_token],"}",cur_token))
	    {
		free(if_name);
		return iface;
	    }
	}
	free(if_name);
	free(iface);
	return NULL;
    }
    return NULL;
}

static struct arc_invocation* parse_invocation(char **tokens, size_t num_tokens, size_t *cur_token, struct arc_system *system)
{

    if(consume(tokens[*cur_token],"Invocation",cur_token))
    {
	if(consume(tokens[*cur_token],"{",cur_token))
	{
	    if(consume(tokens[*cur_token],"Source",cur_token))
	    {
		char *src=parse_name(tokens,num_tokens,cur_token);
		if(!src)
		    return NULL;
		if(consume(tokens[*cur_token],"Target",cur_token))
		{
		    char *tgt=parse_name(tokens,num_tokens,cur_token);
		    if(!tgt)
			return NULL;
		    if(consume(tokens[*cur_token],"Interface",cur_token))
		    {
			char *if_name=parse_name(tokens,num_tokens,cur_token);
			if(!if_name)
			    return NULL;
			struct arc_invocation *invo=malloc(sizeof(struct arc_invocation));
			struct arc_component *src_comp=sys_find_component(system,src);
			struct arc_component *tgt_comp=sys_find_component(system,tgt);
			struct arc_interface *iface=comp_find_iface(tgt_comp,if_name);
			if(!src_comp)
			{
			    fprintf(stderr,"%s not found\n",src);
			    return NULL;
			}
			if(!tgt_comp)
			{
			    fprintf(stderr,"%s not found\n",tgt);
			    return NULL;
			}
			if(!if_name)
			{
			    fprintf(stderr,"%s not found\n",if_name);
			    return NULL;
			}
			init_invocation(invo,iface,src_comp,tgt_comp);
			if(consume(tokens[*cur_token],"}",cur_token))
			{
			free(if_name);
			free(src);
			free(tgt);
			return invo;
			}
			
		    }
		}
	    }
		
	}
	return NULL;	       
    }
    return NULL;
    
}
static struct arc_property* parse_property(char **tokens, size_t num_tokens, size_t *cur_token)
{
    if(consume(tokens[*cur_token],"Property",cur_token))
    {
	char *prop_name = parse_name(tokens,num_tokens,cur_token);
	if(!prop_name)
	{
	    return NULL;
	}
	struct arc_property *prop=malloc(sizeof(struct arc_property));

	if(consume(tokens[*cur_token],"=",cur_token))
	{
	    size_t cur_pos=*cur_token;
	    int int_val=parse_int(tokens,num_tokens,cur_token);
	    if(*cur_token>cur_pos)
	    {
		init_int_property(prop,prop_name,int_val);
		free(prop_name);
		return prop;
	    }
	    double double_val=parse_double(tokens,num_tokens,cur_token);
	    if(*cur_token>cur_pos)
	    {
		init_dbl_property(prop,prop_name,double_val);
		free(prop_name);
		return prop;
	    }
	    char *str=parse_string(tokens,num_tokens,cur_token);
	    if(*cur_token>cur_pos)
	    {
		init_str_property(prop,prop_name,str);
		free(prop_name);
		free(str);
		return prop;
	    }
	}
	free(prop_name);
	free(prop);
	return NULL;
    }
    return NULL;
}



struct arc_property* parse_txt_property(char *txt,struct arc_system *sys)
{
    struct tokenizer dot_tok;
    tokenizer_init(&dot_tok,txt,".");
    int num_tokens=count_tokens(&dot_tok);
    struct arc_property *ret;
    if(num_tokens==1)
    {
	//"N_COMPONENTS" -> System property
	char *prop_name=next_token(&dot_tok);
	ret=sys_find_property(sys,prop_name);
	free(prop_name);
    }
    else if(num_tokens==2)
    {
	//Backend.N_FRONTENDS -> Component property
	char *comp_name=next_token(&dot_tok);
	struct arc_component *comp=sys_find_component(sys,comp_name);
	if(comp)
	{
	    char *prop_name=next_token(&dot_tok);
	    ret=comp_find_property(comp,prop_name);
	    free(comp_name);
	    free(prop_name);
	}
	else
	{
	    fprintf(stderr,"Component %s not found\n",comp_name);
	    free(comp_name);
	}
    }
    else if(num_tokens==3)
    {
	//Backend.login.username -> Interface property
	char *comp_name=next_token(&dot_tok);
	struct arc_component *comp=sys_find_component(sys,comp_name);
	if(comp)
	{
	    char *iface_name=next_token(&dot_tok);
	    struct arc_interface *iface=comp_find_iface(comp,iface_name);
	    if(iface)
	    {
	    char *prop_name=next_token(&dot_tok);
	    ret=iface_find_property(iface,prop_name);
	    free(comp_name);
	    free(iface_name);
	    free(prop_name);
	    }
	    else
	    {
		fprintf(stderr,"Interface %s not found\n",iface_name);
		free(comp_name);
		free(iface_name);
	    }
	}
	else
	{
	    fprintf(stderr,"Component %s not found\n",comp_name);
	    free(comp_name);
	}
    }

    return ret;
}

union property_value parse_txt_value(char *txt, enum property_type *type)
{
    union property_value value;
    double dbl_val=atof(txt);
    if(strchr(txt, '.') != NULL && (dbl_val!=0.0 || strcmp(txt,"0.0")==0))
    {
	value.v_dbl=dbl_val;
	*type = DOUBLE;
	return value;
    }

    int int_val=atoi(txt);
    if(int_val!=0 || strcmp(txt,"0")==0)
    {
	value.v_int=int_val;
	*type = INT;
	return value;
    }

    
    value.v_str=malloc(strlen(txt)+1);
    strcpy(value.v_str,txt);
    *type = STRING;
    return value;
    
}

enum ARC_COMPARISON_OPERATOR parse_cond_value(char *txt)
{
    if(strcmp(txt,"<=")==0)
	return ARC_COND_LE;
    else if(strcmp(txt,">=")==0)
	return ARC_COND_GE;
    else if(strcmp(txt,"=")==0)
	return ARC_COND_EQ;
    else if(strcmp(txt,"<")==0)
	return ARC_COND_LT;
    else if(strcmp(txt,">")==0)
	return ARC_COND_GT;
    else
	return ARC_COND_INVALID;
    
}

struct arc_simple_condition* parse_txt_condition(char *txt, struct arc_system *sys)
{
    struct arc_simple_condition *cond=malloc(sizeof(struct arc_simple_condition));

    struct tokenizer op_tok;
    tokenizer_init(&op_tok,txt," ");
    char *cond_txt=next_token(&op_tok);
    size_t count=0;
    while(cond_txt)
    {
	free(cond_txt);
	cond_txt=next_token(&op_tok);
	count++;
	
    }

    tokenizer_init(&op_tok,txt," ");

    if(count!=3)
	return NULL;

    char *prop_name=next_token(&op_tok);
    struct arc_property *prop=parse_txt_property(prop_name,sys);
    
    if(prop)
    {
	cond->prop=prop;
	char *op_name=next_token(&op_tok);
	enum ARC_COMPARISON_OPERATOR op=parse_cond_value(op_name);
	if(op!=ARC_COND_INVALID)
	{
	    cond->op=op;
	    char *const_value=next_token(&op_tok);
	    enum property_type type= INVALID;
	    union property_value value=parse_txt_value(const_value, &type );
	    if(type == INT)
	    {
		cond->v_int=value.v_int;
	    }
	    else if(type == DOUBLE)
	    {
		cond->v_dbl=value.v_dbl;
	    }
	    else if(type == STRING)
	    {
		cond->v_str=value.v_str;
	    }
	    
	    free(prop_name);
	    free(op_name);
	    free(const_value);
	    return cond;
	}
	else
	{
	    fprintf(stderr,"Invalid operator %s\n",prop_name);
	    free(prop_name);
	    free(op_name);
	    free(cond);
	}
	
    }
    else
    {
	fprintf(stderr,"Could not find property [%s]\n",prop_name);
	free(prop_name);
    }

    return cond;
}


struct arc_complex_condition* parse_txt_complex_condition(char *txt,
							  struct arc_system *sys)
{
    char *reset=txt;
    struct arc_complex_condition* cond=malloc(sizeof(struct arc_complex_condition));
    struct tokenizer op_tok;
    tokenizer_init(&op_tok,txt,"(&|)");
    char *cond_txt=next_token(&op_tok);
    size_t count=0;
    while(cond_txt)
    {
	
	free(cond_txt);
	cond_txt=next_token(&op_tok);
	count++;
	
    }

    if(count<1)
    {
	return NULL;
    }
    
    struct arc_simple_condition *simple_conds=malloc(count*sizeof(struct arc_simple_condition));
    enum ARC_BOOLEAN_OPERATOR *ops=malloc((count-1)*sizeof(enum ARC_BOOLEAN_OPERATOR));

    tokenizer_init(&op_tok,reset,"(&|)");
    for(size_t i=0;i<count;i++)
    {
	cond_txt=next_token(&op_tok);   
	struct arc_simple_condition *simple_cond=parse_txt_condition(cond_txt,sys);
	if(simple_cond)
	{
	memcpy(&simple_conds[i],simple_cond,sizeof(struct arc_simple_condition));
	free(simple_cond);
	}
	else
	{
	    fprintf(stderr,"Failed to make simple condition out of %s\n",cond_txt);
	}
	free(cond_txt);
	
    }
    

    tokenizer_init(&op_tok,reset,"(&|)");
    for(int i=0;i<count-1;i++)
    {
	//printf("Next delim: %c\n",next_delim(&op_tok,"&|"));
	char c=next_delim(&op_tok,"&|");
	if(c=='&')
	{
	    ops[i]=ARC_COND_AND;
	}
	else if(c=='|')
	{
	    ops[i]=ARC_COND_OR;
	}
    }
    cond->num_simple_conditions=count;
    cond->simple_conds=simple_conds;
    cond->ops=ops;
    
    return cond;
}

struct arc_simple_condition* parse_simple_cond(char **tokens, size_t num_tokens, size_t *cur_token, struct arc_system *sys)
{
    char *prop_name=parse_name(tokens,num_tokens,cur_token);
    if(prop_name)
    {
	struct arc_simple_condition *cond=NULL;
	struct arc_property *prop=parse_txt_property(prop_name,sys);
	if(prop)
	{
	    enum ARC_COMPARISON_OPERATOR op=parse_cond_value(tokens[*cur_token]);
	    if(op!=ARC_COND_INVALID)
	    {
		(*cur_token)++;
		enum property_type type = INVALID;
		union property_value const_value=parse_txt_value(tokens[*cur_token],&type);
		(*cur_token)++;
		cond=malloc(sizeof(struct arc_simple_condition));
		cond->prop=prop;
		cond->op=op;
		if(type == INT)
		{
		    cond->v_int=const_value.v_int;
		}
		else if(type == DOUBLE)
		{
		    cond->v_dbl=const_value.v_dbl;
		}
		else if(type == STRING)
		{
		    //TODO: CHECK IF THIS MAKESE SENSE!
		    cond->v_str= const_value.v_str;
		}
		
		free(prop_name);
		return cond;
	    }

	    
	}
	else
	{
	    fprintf(stderr,"Could not find property %s\n",prop_name);
	    free(prop_name);
	    return NULL;
	}
	
    }
    else
    {
	fprintf(stderr,"Could not resolve %s into a property name\n",tokens[*cur_token]);
	return NULL;
    }
}
struct arc_complex_condition* parse_complex_cond(char **tokens, size_t num_tokens, size_t *cur_token, struct arc_system *sys)
{
    
    size_t count=0;
    
    for(size_t  i= *cur_token ; i < num_tokens;i++)
    {
	
	if(strcmp(tokens[i],"|")==0 || strcmp(tokens[i],"&")==0)
	{
		count++;
	}
	else if(strcmp(tokens[i],")")==0)
	{
	    break;
	}
    }
//    printf("Operator count is:%lu\n",count);
    size_t num_simple_conditions=count+1;
    struct arc_complex_condition* cond=malloc(sizeof(struct arc_complex_condition));
    struct arc_simple_condition *simple_conds=malloc(num_simple_conditions*sizeof(struct arc_simple_condition));
    enum ARC_BOOLEAN_OPERATOR *ops=malloc((num_simple_conditions-1)*sizeof(enum ARC_BOOLEAN_OPERATOR));

    
    if(consume(tokens[*cur_token],"(",cur_token))
    {
	struct arc_simple_condition *s_cond=parse_simple_cond(tokens,num_tokens,cur_token,sys);
	if(!s_cond)
	{
	    return NULL;
	}
	memcpy(&simple_conds[0],s_cond,sizeof(struct arc_simple_condition));
	free(s_cond);
	size_t idx=1;
	bool done=false;
	while(!done)
	{
//	    printf("Curr token: %s\n",tokens[*cur_token]);
	    if(strcmp(tokens[*cur_token],")")==0)
	    {
		done=true;
		(*cur_token)++;
		break;
	    }
	    else if(strcmp(tokens[*cur_token],"|")==0)
	    {
		ops[idx-1]=ARC_COND_OR;
		(*cur_token)++;
	    }
	    else if(strcmp(tokens[*cur_token],"&")==0)
	    {
		ops[idx-1]=ARC_COND_AND;
		(*cur_token)++;
	    }
	    else
	    {
		fprintf(stderr,"Could not make simple condition out of %s\n",tokens[*cur_token]);
		return NULL;
	    }
	    struct arc_simple_condition *s_cond=parse_simple_cond(tokens,num_tokens,cur_token,sys);
	    if(s_cond)
	    {
		memcpy(&simple_conds[idx],s_cond,sizeof(struct arc_simple_condition));
		free(s_cond);
	    }
	    else
	    {
		fprintf(stderr,"Could not make simple condition out of %s\n",tokens[*cur_token]);
		return NULL;
	    }
	    idx++;
	}
	 cond->simple_conds=simple_conds;
	 cond->ops=ops;
	 cond->num_simple_conditions=num_simple_conditions;
	 return cond;
    }
    //we failed so we need to clean up allocated memory
    free(ops);
    free(cond);
    free(simple_conds);
    return NULL;
   
}
