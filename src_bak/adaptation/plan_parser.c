#define _XOPEN_SOURCE 500

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "arc_parse.h"
#include "arc_system.h"
#include "plan_parser.h"
#include "plan_recognizer.h"
#include "tokenize.h"
#include "file_utils.h"


static void strip_quotes(char *str)
{
    size_t len = strlen(str);
    if(len >= 2 && str[0] == '"' && str[len-1] == '"')
    {
	for(size_t i=0;i<len-2;i++)
	{
	    str[i]=str[i+1];
	}
	str[len-2]='\0';
    }

}


bool consume_p(struct token *token, enum PLAN_TOKEN_TYPE t_type, int *pos)
{
    if(token->t_type != t_type)
    {
	fprintf(stderr, "Expected [%s], got [%s] instead!\n", TOKEN_NAME[t_type], token->str);
	return false;
    }
    (*pos)++;

    return true;
}

bool eat_until(struct token_stream *t_s, enum PLAN_TOKEN_TYPE t_type, int *pos)
{

    if(*pos >= t_s->num_tokens)
    {
	    return false;
    }
    
    //keep eating tokens until we encouter a token of type t_type
    while( t_s->tokens[*pos].t_type != t_type)
    {
	(*pos)++;

	if(*pos >= t_s->num_tokens)
	{
	    return false;
	}
    }

    return true;
}

bool parse_lines(struct token_stream *t_s, int *pos, struct arc_plan *plan)
{
    //deal with epsilon
    if(*pos < t_s->num_tokens)
    {
	parse_line(t_s,pos,plan);
	parse_lines(t_s,pos,plan);
    }

    //no more stuff, we're good to go
    return true;
	
    
}

bool parse_line(struct token_stream *t_s, int *pos, struct arc_plan *plan)
{
    //here we need to do a bit of lookahead to pick a rule
    //need an extra trick to "unroll OP into its components"
    if(t_s->tokens[*pos].t_type == TOKEN_PLUS
       || t_s->tokens[*pos].t_type == TOKEN_MINUS)
    {
	if(!parse_genop(t_s,pos,plan))
	{
	    fprintf(stderr, "Error parsing genopt, token %s at pos %d",
		    t_s->tokens[*pos].str, *pos);
	}
    }
    else
    {
	if(!parse_propop(t_s, pos,plan))
	{
	    fprintf(stderr, "Error parsing propop, token %s at pos %d",
		    t_s->tokens[*pos].str, *pos);
	}
    }

    return parse_ffi(t_s,pos,plan);
}

bool parse_propop(struct token_stream *t_s, int *pos, struct arc_plan *plan)
{
    //Lookahead to decide what type of property we're dealing with
    //Note QSYS is just an alias for ID

    //QSYS PUPDATE
    if(t_s->tokens[*pos].t_type == TOKEN_ID)
    {
	if(!consume_p(&t_s->tokens[*pos],TOKEN_ID,pos))
	{
	    return false;
	}

	/*Note that all of them expect a PUPDATE rule so we can group this
	  together but that makes parsing harder, so we distribute
	*/
	if(!parse_pupdate(t_s,pos,plan))
	{
	    fprintf(stderr,"Could not parse pupdate starting at: %d:%s\n",*pos,
		    t_s->tokens[*pos].str);
	    return false;
	}
	
	//we have successfully parsed a system propery op command
	//  -6 -5    -4    -3    -2   -1
	// Foo  [   prop   ]   =  bar
	
	struct arc_plan_action *plan_action;
	plan_action = malloc(sizeof(struct arc_plan_action));
	if(t_s->tokens[*pos-1].t_type != TOKEN_NIL)
	{
	    plan_action->op=ADD_SYS_PROP;
	    plan_action->num_args=3;
	    plan_action->args=malloc(sizeof(char*)*3);
	
	    plan_action->args[0]=strdup(t_s->tokens[*pos-4].str);
	    if(t_s->tokens[*pos-1].t_type == TOKEN_NUMBER)
	    {
		plan_action->args[1]=strdup("DOUBLE");
	    }
	    else
	    {
		plan_action->args[1]=strdup("STRING");
	    }
	    strip_quotes(t_s->tokens[*pos-1].str);
	    //printf("PROP_STR:%s\n",t_s->tokens[*pos-1].str);
	    plan_action->args[2]=strdup(t_s->tokens[*pos-1].str);
	}
	else
	{
	    plan_action->op=REM_SYS_PROP;
	    plan_action->num_args=1;
	    plan_action->args=malloc(sizeof(char*)*1);
	    plan_action->args[0]=strdup(t_s->tokens[*pos-4].str);
	    //printf("%s\n",plan_action->args[0]);
	    
	}
	
	plan->actions[plan->parse_idx]=plan_action;
	plan->parse_idx++;
	
    }
    else if(t_s->tokens[*pos].t_type == TOKEN_QCOMP)
    {
	if(!consume_p(&t_s->tokens[*pos],TOKEN_QCOMP,pos))
	{
	    return false;
	}

	if(!parse_pupdate(t_s,pos,plan))
	{
	    fprintf(stderr,"Could not parse pupdate starting at: %d:%s\n",*pos,
		    t_s->tokens[*pos].str);
	    return false;
	}

	//we have successfully parsed a component property op command
	struct arc_plan_action *plan_action;
	plan_action = malloc(sizeof(struct arc_plan_action));	
	if(t_s->tokens[*pos-1].t_type != TOKEN_NIL)
	{
	    plan_action->op=ADD_COMP_PROP;
	    plan_action->num_args=4;
	    plan_action->args=malloc(sizeof(char*)*4);
	    char *comp_unqual = strchr(t_s->tokens[*pos-6].str,'.')+1;
	    plan_action->args[0]=strdup(comp_unqual);
	    plan_action->args[1]=strdup(t_s->tokens[*pos-4].str);
	    if(t_s->tokens[*pos-1].t_type == TOKEN_NUMBER)
	    {
		plan_action->args[2]=strdup("DOUBLE");
	    }
	    else
	    {
		plan_action->args[2]=strdup("STRING");
	    }
	    strip_quotes(t_s->tokens[*pos-1].str);
	    //printf("PROP_STR:%s\n",t_s->tokens[*pos-1].str);
	    plan_action->args[3]=strdup(t_s->tokens[*pos-1].str);
	    
	}
	else
	{
	    plan_action->op=REM_COMP_PROP;
	    plan_action->num_args=2;
	    plan_action->args=malloc(sizeof(char*)*2);
	    char *comp_unqual = strchr(t_s->tokens[*pos-6].str,'.')+1;
	    plan_action->args[0]=strdup(comp_unqual);
	    plan_action->args[1]=strdup(t_s->tokens[*pos-4].str);
	}
	if(plan->parse_idx == plan->num_actions)
	{
	    printf("Panic!\n");
	    printf("[%zu/%zu]",plan->parse_idx,plan->num_actions);
	    exit(-1);
	}
	
	plan->actions[plan->parse_idx]=plan_action;
	plan->parse_idx++;
	
    }
    else if(t_s->tokens[*pos].t_type == TOKEN_QIFACE)
    {
	if(!consume_p(&t_s->tokens[*pos],TOKEN_QIFACE,pos))
	{
	    return false;
	}

	if(!parse_pupdate(t_s,pos,plan))
	{
	    fprintf(stderr,"Could not parse pupdate starting at: %d:%s\n",*pos,
		    t_s->tokens[*pos].str);
	    return false;
	}

	//we have successfully parsed an interface property op command
	struct arc_plan_action *plan_action;
	plan_action = malloc(sizeof(struct arc_plan_action));	
	if(t_s->tokens[*pos-1].t_type != TOKEN_NIL)
	{
	    plan_action->op=ADD_IFACE_PROP;
	    plan_action->num_args=5;
	    plan_action->args=malloc(sizeof(char*)*5);
	    char *comp_unqual = strchr(t_s->tokens[*pos-6].str,'.')+1;
	    
	    char *iface_unqual = strchr(comp_unqual,'.')+1;
	    plan_action->args[1]=strdup(iface_unqual);
	    
	    /*
	      pretty hacky, pointer comparison here is valid because
	      they're guaranteed to be in the same buffer since the
	      iface unqual pointer is returned by strchr note that we
	      don't really trample memory since we strdup
	    */
	    int dot_pos=iface_unqual-comp_unqual-1;
	    comp_unqual[dot_pos]='\0';
	    
	    plan_action->args[2]=strdup(t_s->tokens[*pos-4].str);
	    plan_action->args[0]=strdup(comp_unqual);
	    
	    if(t_s->tokens[*pos-1].t_type == TOKEN_NUMBER)
	    {
		plan_action->args[3]=strdup("DOUBLE");
	    }
	    else
	    {
		plan_action->args[3]=strdup("STRING");
	    }
	    strip_quotes(t_s->tokens[*pos-1].str);
	    //printf("PROP_STR:%s\n",t_s->tokens[*pos-1].str);
	    plan_action->args[4]=strdup(t_s->tokens[*pos-1].str);
	}
	else
	{
	    plan_action->op=REM_IFACE_PROP;
	    plan_action->num_args=3;
	    plan_action->args=malloc(sizeof(char*)*3);
	    char *comp_unqual = strchr(t_s->tokens[*pos-6].str,'.')+1;
	    
	    char *iface_unqual = strchr(comp_unqual,'.')+1;
	    plan_action->args[1]=strdup(iface_unqual);
	    
	    /*
	      pretty hacky, pointer comparison here is valid because
	      they're guaranteed to be in the same buffer since the
	      iface unqual pointer is returned by strchr note that we
	      don't really trample memory since we strdup
	    */
	    int dot_pos=iface_unqual-comp_unqual-1;
	    comp_unqual[dot_pos]='\0';
	    plan_action->args[2]=strdup(t_s->tokens[*pos-4].str);
	    plan_action->args[0]=strdup(comp_unqual);
	}
	
	plan->actions[plan->parse_idx]=plan_action;
	plan->parse_idx++;
	
    }
    else
    {
	fprintf(stderr,"Could not parse propop starting at: %d:%s\n",*pos,
		t_s->tokens[*pos].str);
	return false;
    }

    

    
    return true;   
	
}

bool parse_pupdate(struct token_stream *t_s, int *pos, struct arc_plan *plan)
{
    if(!consume_p(&t_s->tokens[*pos],TOKEN_L_BRACKET,pos))
    {
	return false;
    }

    if(!consume_p(&t_s->tokens[*pos],TOKEN_ID,pos))
    {
	return false;
    }

    if(!consume_p(&t_s->tokens[*pos],TOKEN_R_BRACKET,pos))
    {
	return false;
    }

    if(!consume_p(&t_s->tokens[*pos],TOKEN_EQUALS,pos))
    {
	return false;
    }

    return parse_pvalue(t_s, pos,plan);
    
}

bool parse_pvalue(struct token_stream *t_s, int *pos, struct arc_plan *plan)
{
    if(t_s->tokens[*pos].t_type == TOKEN_NIL)
    {
	if(!consume_p(&t_s->tokens[*pos],TOKEN_NIL,pos))
	{
	    return false;
	}
    }
    else if(t_s->tokens[*pos].t_type == TOKEN_NUMBER)
    {
	if(!consume_p(&t_s->tokens[*pos],TOKEN_NUMBER,pos))
	{
	    return false;
	}
    }
    else if(t_s->tokens[*pos].t_type == TOKEN_STRING)
    {

	//The if checks here are kinda extraneuous ;)
	if(!consume_p(&t_s->tokens[*pos],TOKEN_STRING,pos))
	{
	    return false;
	}
    }
    else
    {
	fprintf(stderr,"Could not parse pvalue starting at: %d:%s\n",*pos,
		t_s->tokens[*pos].str);
	return false;
    }

    

    
    return true;
}

bool parse_ffi(struct token_stream *t_s, int *pos, struct arc_plan *plan)
{
    if(!consume_p(&t_s->tokens[*pos],TOKEN_L_CBRACE,pos))
    {
	return false;
    }


    if(!eat_until(t_s, TOKEN_R_CBRACE, pos))
    {
	return false;
    }

    if(!consume_p(&t_s->tokens[*pos],TOKEN_R_CBRACE,pos))
    {
	return false;
    }

    return true;
}

bool parse_genop(struct token_stream *t_s, int *pos, struct arc_plan *plan)
{
    if(!parse_op(t_s,pos,plan))
    {
	fprintf(stderr,"Could not parse op starting at: %d:%s\n",*pos,
		t_s->tokens[*pos].str);
	return false;
    }
    if(!parse_genop1(t_s,pos,plan))
    {
	return false;
    }
     
    return true;
}

bool parse_genop1(struct token_stream *t_s, int *pos, struct arc_plan *plan)
{
    //lookahead to figure out the next rule
    //once more, QSYS aliases to ID since we don't do checking
    if(t_s->tokens[*pos].t_type == TOKEN_ID)
    {
	if(!consume_p(&t_s->tokens[*pos],TOKEN_ID,pos))
	{
	    return false;
	}
	if(!consume_p(&t_s->tokens[*pos],TOKEN_ID,pos))
	{
	    return false;
	}

	/*
	  we have successfully parsed a +/- component command
	  *pos - 3 points to the operator *pos -1 points to the
	  component
	*/
	
	struct arc_plan_action *plan_action;
	plan_action=malloc(sizeof(struct arc_plan_action));
	if(t_s->tokens[(*pos)-3].t_type == TOKEN_PLUS)
	{
	    plan_action->op=ADD_COMP;
	}
	else
	{
	    plan_action->op=REM_COMP;
	}
	plan_action->num_args=1;
	plan_action->args=malloc(sizeof(char*));
	plan_action->args[0]=strdup(t_s->tokens[(*pos)-1].str);
	plan->actions[plan->parse_idx]=plan_action;
	plan->parse_idx++;
	
    }
    else if(t_s->tokens[*pos].t_type == TOKEN_QCOMP)
    {
	if(!consume_p(&t_s->tokens[*pos],TOKEN_QCOMP,pos))
	{
	    return false;
	}

	return parse_genop2(t_s,pos,plan);
    }
    else
    {
	fprintf(stderr,"Could not parse genop1 starting at: %d:%s\n",*pos,
		t_s->tokens[*pos].str);
	return false;
    }

    return true;
}

bool parse_genop2(struct token_stream *t_s, int *pos, struct arc_plan *plan)
{
    //lookahed to pick between ID and ->
    if(t_s->tokens[*pos].t_type == TOKEN_ID)
    {
	if(!consume_p(&t_s->tokens[*pos],TOKEN_ID,pos))
	{
	    return false;
	}

	/*
	  we have successfully parsed an op interface command
	  *pos - 3 points to op, *pos -2 to qualified component
	  and *pos -1 to inteface
	*/


	struct arc_plan_action *plan_action=malloc(sizeof(struct arc_plan_action));
	if(t_s->tokens[(*pos)-3].t_type == TOKEN_PLUS)
	{
	    plan_action->op=ADD_IFACE;
	}
	else
	{
	    plan_action->op=REM_IFACE;
	}
	plan_action->num_args=2;
	plan_action->args=malloc(sizeof(char*)*2);
	//strip sysname, "unqualify component"
	//Is this too smart? I'm moving a pointer ;)
	char *comp_name= strchr(t_s->tokens[(*pos)-2].str,'.') + 1;
	plan_action->args[0]=strdup(comp_name);
	plan_action->args[1]=strdup(t_s->tokens[(*pos)-1].str);
	plan->actions[plan->parse_idx]=plan_action;
	plan->parse_idx++;
	
	
	
    }
    else
    {
	if(!consume_p(&t_s->tokens[*pos],TOKEN_CALLS,pos))
	{
	    return false;
	}

	if(!consume_p(&t_s->tokens[*pos],TOKEN_QCOMP,pos))
	{
	    return false;
	}

	if(!consume_p(&t_s->tokens[*pos],TOKEN_COLON,pos))
	{
	    return false;
	}

	if(!consume_p(&t_s->tokens[*pos],TOKEN_ID,pos))
	{
	    return false;
	}


	/* We have successfully parsed a op invocation command
	  -6  -5 -4 -3  -2 -1          
	   + a.b -> b.c :  d 
	 */
	
	// 
	//op from, to, iface
	

	struct arc_plan_action *plan_action;
	plan_action=malloc(sizeof(struct arc_plan_action));
	if(t_s->tokens[(*pos) - 6].t_type == TOKEN_PLUS)
	{
	    plan_action->op=ADD_INVO;
	}
	else
	{
	    plan_action->op=REM_INVO;
	}
	plan_action->num_args=3;
	plan_action->args=malloc(sizeof(char*)*3);

	
	char *from_unqual = strchr(t_s->tokens[(*pos)-5].str,'.') + 1;
	char *to_unqual = strchr(t_s->tokens[(*pos)-3].str,'.') + 1;
	plan_action->args[0]=strdup(from_unqual);
	plan_action->args[1]=strdup(to_unqual);
	plan_action->args[2]=strdup(t_s->tokens[(*pos)-1].str);
	plan->actions[plan->parse_idx]=plan_action;
	plan->parse_idx++;
    }

    return true;
}

bool parse_op(struct token_stream *t_s, int *pos, struct arc_plan *plan)
{
    if(t_s->tokens[*pos].t_type == TOKEN_PLUS)
    {
	return consume_p(&t_s->tokens[*pos],TOKEN_PLUS,pos);
    }
    else if(t_s->tokens[*pos].t_type == TOKEN_MINUS)
    {
	return consume_p(&t_s->tokens[*pos],TOKEN_MINUS,pos);
    }

    return false;
}

bool plan_parse(struct token_stream *t_s, struct arc_plan *plan, char *name, size_t how_many)
{

    //Do the wiring here

    plan_init(plan,name);
    plan->parse_idx=0;

	
    
    plan->actions=malloc(sizeof(struct arc_plan_action*)*how_many);
    plan->num_actions=how_many;
    int pos=0;
    return parse_lines(t_s, &pos,plan);
}


/* TODO:
   NEED MORE SYMBOLS IN IDS: at least '_' 
*/

/*
  BUG: SOMETHING IS VERY WRONG WHEN WE ADD INTERFACES WITH THE SAME NAMES, DEBUG TOMORROW
*/
int run_parser(int argc, char *argv[])
{

    if(argc != 2)
    {
	fprintf(stderr,"Usage: plan_parser plan_file\n");
	return EXIT_FAILURE;
    }

    char *text = read_file(argv[1]);

    

    struct token_stream t_s;
    token_stream_init(&t_s);


    struct arc_plan plan;
    
    if(build_token_stream(text, &t_s))
    {
	plan_parse(&t_s,&plan,argv[1],num_lines(text));
    }

    free(text);

    //If this worked at this point we have a filled in plan, let's test it.

    text = read_file("Tiny.arch");
    struct arc_system *sys=arc_parse(text);
    free(text);
    
    
    printf("Executing plan: %s on %s:%d",plan.plan_name,sys->sys_name,
	   plan_execute(&plan,sys));

    print_system(sys);
    

    return 0;
}
