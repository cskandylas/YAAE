#include "knowledge_base.h"


#include <stdlib.h>
#include <string.h>
#include <stdio.h>

void init_knowledge_base(struct knowledge_base *k_b, struct arc_model *model)
{
    k_b->model = model;
    
    k_b->num_res=0;
    k_b->max_res=ARC_KB_RES_INIT_MAX;
    k_b->named_res=malloc(k_b->max_res*sizeof(struct named_res*));
      
    k_b->num_plans=0;
    k_b->max_plans=ARC_KB_PLANS_INIT_MAX;
    k_b->known_plans=malloc(k_b->max_plans*sizeof(struct arc_plan*));

    k_b->num_strats=0;
    k_b->max_strats=ARC_KB_STRATS_INIT_MAX;
    k_b->known_strats=malloc(k_b->max_strats*sizeof(struct arc_strategy*));

}

void destroy_knowledge_base(struct knowledge_base *k_b)
{
    free(k_b->known_plans);
    free(k_b->known_strats);
    for(int i=0;i<k_b->num_res;i++)
    {
	// TODO: POSSIBLE BUG HERE!
	if(k_b->named_res[i])
	{
	    named_res_destroy(k_b->named_res[i]);
	}
	
    }
    free(k_b->named_res);
}

void knowledge_base_add_plan(struct knowledge_base *k_b, struct arc_plan *plan)
{
//    printf("Adding plan: %s\n",plan->plan_name);
    if(k_b->num_plans>=k_b->max_plans)
    {
        k_b->max_plans*=2;
	k_b->known_plans=realloc(k_b->known_plans,
				 k_b->num_plans*sizeof(struct arc_plan*));
    }
    k_b->known_plans[k_b->num_plans]=plan;
    k_b->num_plans++;
}

struct arc_plan* knowledge_base_get_plan(struct knowledge_base *k_b, char *plan_name)
{
    for(int i=0;i<k_b->num_plans;i++)
    {
	if(strcmp(k_b->known_plans[i]->plan_name,plan_name)==0)
	{
	    return k_b->known_plans[i];
	}
    }
    return NULL;
}


void knowledge_base_add_strat(struct knowledge_base *k_b, struct arc_strategy *strat)
{
    if(k_b->num_strats>=k_b->max_strats)
    {
        k_b->max_strats*=2;
	k_b->known_strats=realloc(k_b->known_strats,
				 k_b->num_strats*sizeof(struct arc_strategy*));
    }
    k_b->known_strats[k_b->num_strats]=strat;
    k_b->num_strats++;
}
struct arc_strategy* knowledge_base_get_strat(struct knowledge_base *k_b, char *strat_name)
{
        for(int i=0;i<k_b->num_strats;i++)
    {
	if(strcmp(k_b->known_strats[i]->name,strat_name)==0)
	{
	    return k_b->known_strats[i];
	}
    }
    return NULL;
}


void knowledge_base_add_res(struct knowledge_base *k_b, struct named_res *named_res)
{
    if(k_b->num_res>=k_b->max_res)
    {
        k_b->max_res*=2;
	k_b->named_res=realloc(k_b->named_res,
				 k_b->num_res*sizeof(struct named_res*));
    }
    k_b->named_res[k_b->num_res]=named_res;
    k_b->num_res++;  
}
void* knowledge_base_get_res(struct knowledge_base *k_b, char *name)
{
    for(int i=0;i<k_b->num_res;i++)
    {
	if(k_b->named_res[i])
	if(strcmp(k_b->named_res[i]->name,name)==0)
	{
	    return k_b->named_res[i]->res;
	}
    }

    return NULL;
}

size_t knowledge_base_num_res(struct knowledge_base *k_b, char *name)
{
    for(int i=0;i<k_b->num_res;i++)
    {
	if(k_b->named_res[i])
	if(strcmp(k_b->named_res[i]->name,name)==0)
	{
	    return k_b->named_res[i]->n_items;
	}
    }

    return 0;
}

void knowledge_base_rem_res(struct knowledge_base *k_b, char *name)
{
    for(int i=0;i<k_b->num_res;i++)
    {
	if(k_b->named_res[i])
	if(strcmp(k_b->named_res[i]->name,name)==0)
	{
	    free(k_b->named_res[i]->name);
	    free(k_b->named_res[i]);
	    k_b->named_res[i]=NULL;;
	}
    }
}

void knowledge_base_list_res(struct knowledge_base *k_b)
{
    for(int i=0;i<k_b->num_res;i++)
    {
	if(k_b->named_res[i])
	{
	    printf("%s:%p\n",k_b->named_res[i]->name,k_b->named_res[i]->res);
	    
	}
}
}
