#include "probe.h"
#include <stdio.h>


void init_probe(struct probe *probe, probe_init init, probe_run run,
		probe_destroy destroy, probe_done done, void *ctx)
{
    probe->ctx=ctx;
    probe->probe_init=init;
    probe->probe_run=run;
    probe->probe_destroy=destroy;
    probe->probe_done=done;
    probe->status=PROBE_CREATED;
    probe->mon = NULL;
    pthread_mutex_init(&probe->lock,NULL);
    probe->probe_init(probe);
}

void start_probe(struct probe *probe)
{
    probe_set_status(probe, PROBE_RUNNING);
    void *res = probe->probe_run(probe);       
}


void destroy_probe(struct probe *probe)
{
    if(probe->status==PROBE_STOPPED)
    {
	probe->probe_destroy(probe);
    }
    else
    {
	fprintf(stderr,"Please stop the probe before destroying it\n");
    }
}


void *probe_get_ctx(struct probe *probe)
{
    void *ret_ctx = NULL;
    pthread_mutex_lock(&probe->lock);
    ret_ctx = probe->ctx;
    pthread_mutex_unlock(&probe->lock);

    return ret_ctx;
}
void probe_set_status(struct probe *probe, enum PROBE_STATUS status)
{
    pthread_mutex_lock(&probe->lock);
    probe->status = status;
    pthread_mutex_unlock(&probe->lock);

}

enum PROBE_STATUS probe_get_status(struct probe *probe)
{
    enum PROBE_STATUS ret_status = PROBE_ST_UNDEF;
    pthread_mutex_lock(&probe->lock);
    ret_status = probe->status;
    pthread_mutex_unlock(&probe->lock);

    return ret_status;
}


struct monitor *probe_get_monitor(struct probe *probe)
{

    struct monitor *ret_mon = NULL;
    pthread_mutex_lock(&probe->lock);
    ret_mon = probe->mon;
    pthread_mutex_unlock(&probe->lock);

    return ret_mon;
    
}
void probe_set_monitor(struct probe *probe, struct monitor *mon)
{
    pthread_mutex_lock(&probe->lock);
    probe->mon = mon;
    pthread_mutex_unlock(&probe->lock);

}
