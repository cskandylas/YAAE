#include "plan.h"

void init_plan(struct plan *p, plan_run run, plan_done done, void *ctx)
{
    p->run = run;
    p->done = done;
    p->ctx = ctx;
    pthread_mutex_init(&p->lock, NULL);
    
}


void run_plan(struct plan *p)
{
    plan_set_status(p, PLAN_RUNNING);
    p->run(p);
}

void destroy_plan(struct plan *p)
{
    
}




void *plan_get_ctx(struct plan *p)
{
    void *ret = NULL;
    pthread_mutex_lock(&p->lock);
    ret = p->ctx;
    pthread_mutex_unlock(&p->lock);
    return ret;
}

void plan_set_status(struct plan *p, enum PLAN_STATUS status)
{
    pthread_mutex_lock(&p->lock);
    p->status = status;
    pthread_mutex_unlock(&p->lock);
}

enum PLAN_STATUS plan_get_status(struct plan *p)
{
    enum PLAN_STATUS a_s = PLAN_STATUS_UNDEF;
    pthread_mutex_lock(&p->lock);
    a_s = p->status;
    pthread_mutex_unlock(&p->lock);
    return a_s;
}

struct planner *plan_get_planner(struct plan *p)
{
    struct planner *pl = NULL;
    pthread_mutex_lock(&p->lock);
    pl = p->pl;
    pthread_mutex_unlock(&p->lock);
    return pl;
}

void plan_set_planner(struct plan *p, struct planner *pl)
{
    pthread_mutex_lock(&p->lock);
    p->pl = pl;
    pthread_mutex_unlock(&p->lock);
}

