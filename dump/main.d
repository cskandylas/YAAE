#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "arc_property.h"
#include "arc_interface.h"
#include "arc_component.h"
#include "arc_invocation.h"
#include "arc_system.h"
#include "arc_plan.h"
#include "arc_parse.h"
#include "arc_condition.h"
#include "arc_strategy.h"
#include "knowledge_base.h"
#include "monitoring.h"
#include "report_probe.h"
#include "analysis.h"
#include "arc_diff.h"
#include "rbtree.h"
#include "tokenize.h"
#include "file_utils.h"
#include "plan_parser.h"

void cleanup_sys(struct arc_system *sys)
{
    
    if(sys)
    {
	for(int i=0;i<sys->num_components;i++)
	{
	    if(sys->components[i])
	    {
		
		for(int j=0;j<sys->components[i]->num_interfaces;j++)
		{
		    if(sys->components[i]->interfaces[j])
		    {
			for(int k=0;k<sys->components[i]->interfaces[j]->num_properties;k++)
			{
			    if(sys->components[i]->interfaces[j]->properties[k])
			    {
				destroy_property(sys->components[i]->interfaces[j]->properties[k]);
				free(sys->components[i]->interfaces[j]->properties[k]);
			    }
			}
			destroy_interface(sys->components[i]->interfaces[j]);
			free(sys->components[i]->interfaces[j]);
		    }
		}

		for(int j=0;j<sys->components[i]->num_properties;j++)
		{
		    if(sys->components[i]->properties[j])
		    {
			destroy_property(sys->components[i]->properties[j]);
			free(sys->components[i]->properties[j]);
		    }
		}
		    
		destroy_component(sys->components[i]);
		free(sys->components[i]);
	    }
	}

	for(int i=0;i<sys->num_invocations;i++)
	{
	    if(sys->invocations[i])
		free(sys->invocations[i]);
	}

	for(int i=0;i<sys->num_properties;i++)
	{
	    if(sys->properties[i])
	    {
		destroy_property(sys->properties[i]);
		free(sys->properties[i]);
	    }
	}
    
	destroy_system(sys);
	free(sys);
    }
}

void cleanup_plan(struct arc_plan *plan)
{
    for(int i=0;i<plan->num_actions;i++)
    {
	if(plan->actions[i])
	{
	    plan_action_cleanup(plan->actions[i]);
	    free(plan->actions[i]);
	}
    }
    plan_destroy(plan);     
}

void cleanup_strategy(struct arc_strategy *strat)
{

    destroy_complex_condition(strat->strat_cond);
    free(strat->strat_cond);
    
    for(int i=0;i<strat->num_cond_plans;i++)
    {	
	if(strat->cond_plans[i])
	{
	    destroy_complex_condition(strat->cond_plans[i]->condition);
	    free(strat->cond_plans[i]->condition);
	    free(strat->cond_plans[i]);
	}
    }
    destroy_strategy(strat);
    free(strat);
}


int component_compare(const void *component_a,const void *component_b)
{
    const struct arc_component *comp_a=component_a;
    const struct arc_component *comp_b=component_b;
    return strcmp(comp_a->comp_name,comp_b->comp_name);
}

void traverse_comp_tree(struct rb_node *node)
{
   
	if(!node)
		return;
	traverse_comp_tree(node->left);
	printf("%s  \n",((struct arc_component*)node->data)->comp_name);
	traverse_comp_tree(node->right);

	
}


int main(int argc, char *argv[])
{

    return run_parser(argc, argv);

    if(argc!=3)
    {
	fprintf(stderr, "YAAE architecture tactic\n");
	exit(1);
    }
	
    
	char *text = read_file(argv[1]);
	struct arc_system *sys=arc_parse(text);
	free(text);
	
	if(!sys)
	{
	    fprintf(stderr,"Could not parse architecture file, exiting\n");
	    exit(-1);
	}
	
	struct knowledge_base k_b;
	init_knowledge_base(&k_b,sys);
	
	text=read_file(argv[2]);

	struct tokenizer line_tok;
	struct arc_plan *plan=malloc(sizeof(struct arc_plan));
	//plan_parse(plan,plan_text);
	//knowledge_base_add_plan(&k_b,plan);
	free(text);
	
	
	

	print_system(sys);

	/*
	text=read_file(argv[3]);

	struct arc_strategy *strat=strategy_parse(text,sys,&k_b);
	knowledge_base_add_strat(&k_b,strat);
	free(text);
	strategy_execute(strat);
	*/
	
	cleanup_sys(sys);
	destroy_knowledge_base(&k_b);
    
    return 0;
}
