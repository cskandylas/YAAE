#include <stdio.h>
#include <stdlib.h>

#include "plan_parser.h"
#include "plan_recognizer.h"
#include "tokenize.h"

char *read_file(char *filename)
{

    FILE *ifp=fopen(filename,"r");
    if(!ifp)
    {
	fprintf(stderr,"Could not find %s!\n",filename);
    }
    fseek(ifp, 0, SEEK_END);
    long numbytes = ftell(ifp);
    fseek(ifp, 0, SEEK_SET);	
    char *text=malloc(numbytes+1);
    size_t num_read=0;
    if(!text)
    {
	fprintf(stderr,"Could not allocate memory buffer\n");
	exit(-1);
	
    }
    num_read=fread(text, sizeof(char), numbytes, ifp);
    fclose(ifp);
    text[numbytes]='\0';
    
    return text;
}

bool consume(struct token *token, enum PLAN_TOKEN_TYPE t_type, int *pos)
{
    if(token->t_type != t_type)
    {
	fprintf(stderr, "Expected [%s], got [%s] instead!\n", TOKEN_NAME[t_type], token->str);
	return false;
    }
    (*pos)++;

    return true;
}

bool eat_until(struct token_stream *t_s, enum PLAN_TOKEN_TYPE t_type, int *pos)
{
    //keep eating tokens until we encouter a token of type t_type
    while( t_s->tokens[*pos].t_type != t_type)
    {
	(*pos)++;

	if(*pos == t_s->num_tokens)
	{
	    return false;
	}
    }

    return true;
}

bool parse_lines(struct token_stream *t_s, int *pos, struct arc_plan *plan)
{
    //deal with epsilon
    if(*pos < t_s->num_tokens)
    {
	parse_line(t_s,pos);
	parse_lines(t_s,pos);
    }

    //no more stuff, we're good to go
    return true;
	
    
}

bool parse_line(struct token_stream *t_s, int *pos, struct arc_plan *plan)
{
    //here we need to do a bit of lookahead to pick a rule
    //need an extra trick to "unroll OP into its components"
    if(t_s->tokens[*pos].t_type == TOKEN_PLUS
       || t_s->tokens[*pos].t_type == TOKEN_MINUS)
    {
	if(!parse_genop(t_s,pos))
	{
	    fprintf(stderr, "Error parsing genopt, token %s at pos %d",
		    t_s->tokens[*pos], *pos);
	}
    }
    else
    {
	if(!parse_propop(t_s, pos))
	{
	    fprintf(stderr, "Error parsing propop, token %s at pos %d",
		    t_s->tokens[*pos], *pos);
	}
    }

    return parse_ffi(t_s,pos);
}

bool parse_propop(struct token_stream *t_s, int *pos, struct arc_plan *plan)
{
    //Lookahead to decide what type of property we're dealing with
    //Note QSYS is just an alias for ID

    //QSYS PUPDATE
    if(t_s->tokens[*pos].t_type == TOKEN_ID)
    {
	if(!consume(&t_s->tokens[*pos],TOKEN_ID,pos))
	{
	    return false;
	}
	
    }
    else if(t_s->tokens[*pos].t_type == TOKEN_QCOMP)
    {
	if(!consume(&t_s->tokens[*pos],TOKEN_QCOMP,pos))
	{
	    return false;
	}
	
    }
    else if(t_s->tokens[*pos].t_type == TOKEN_QIFACE)
    {
	if(!consume(&t_s->tokens[*pos],TOKEN_QIFACE,pos))
	{
	    return false;
	}
    }
    else
    {
	fprintf(stderr,"Could not parse propop starting at: %d:%s\n",*pos,
		t_s->tokens[*pos].str);
	return false;
    }

    //Note that all of them expect a PUPDATE rule so we can group this together
    if(!parse_pupdate(t_s,pos))
    {
	fprintf(stderr,"Could not parse pupdate starting at: %d:%s\n",*pos,
		t_s->tokens[*pos].str);
	return false;
    }

    
    return true;   
	
}

bool parse_pupdate(struct token_stream *t_s, int *pos, struct arc_plan *plan)
{
    if(!consume(&t_s->tokens[*pos],TOKEN_L_BRACKET,pos))
    {
	return false;
    }

    if(!consume(&t_s->tokens[*pos],TOKEN_ID,pos))
    {
	return false;
    }

    if(!consume(&t_s->tokens[*pos],TOKEN_R_BRACKET,pos))
    {
	return false;
    }

    if(!consume(&t_s->tokens[*pos],TOKEN_EQUALS,pos))
    {
	return false;
    }

    return parse_pvalue(t_s, pos);
    
}

bool parse_pvalue(struct token_stream *t_s, int *pos, struct arc_plan *plan)
{
    if(t_s->tokens[*pos].t_type == TOKEN_NIL)
    {
	if(!consume(&t_s->tokens[*pos],TOKEN_NIL,pos))
	{
	    return false;
	}
    }
    else if(t_s->tokens[*pos].t_type == TOKEN_NUMBER)
    {
	if(!consume(&t_s->tokens[*pos],TOKEN_NUMBER,pos))
	{
	    return false;
	}
    }
    else if(t_s->tokens[*pos].t_type == TOKEN_STRING)
    {

	//The if checks here are kinda extraneuous ;)
	if(!consume(&t_s->tokens[*pos],TOKEN_STRING,pos))
	{
	    return false;
	}
    }
    else
    {
	fprintf(stderr,"Could not parse pvalue starting at: %d:%s\n",*pos,
		t_s->tokens[*pos].str);
	return false;
    }

    return true;
}

bool parse_ffi(struct token_stream *t_s, int *pos, struct arc_plan *plan)
{
    if(!consume(&t_s->tokens[*pos],TOKEN_L_CBRACE,pos))
    {
	return false;
    }


    if(!eat_until(t_s, TOKEN_R_CBRACE, pos))
    {
	return false;
    }

    if(!consume(&t_s->tokens[*pos],TOKEN_R_CBRACE,pos))
    {
	return false;
    }

    return true;
}

bool parse_genop(struct token_stream *t_s, int *pos, struct arc_plan *plan)
{
    if(!parse_op(t_s,pos))
    {
	fprintf(stderr,"Could not parse op starting at: %d:%s\n",*pos,
		t_s->tokens[*pos].str);
	return false;
    }
    if(!parse_genop1(t_s,pos))
    {
	return false;
    }
     
    return true;
}

bool parse_genop1(struct token_stream *t_s, int *pos, struct arc_plan *plan)
{
    //lookahead to figure out the next rule
    //once more, QSYS aliases to ID since we don't do checking
    if(t_s->tokens[*pos].t_type == TOKEN_ID)
    {
	if(!consume(&t_s->tokens[*pos],TOKEN_ID,pos))
	{
	    return false;
	}
	if(!consume(&t_s->tokens[*pos],TOKEN_ID,pos))
	{
	    return false;
	}
    }
    else if(t_s->tokens[*pos].t_type == TOKEN_QCOMP)
    {
	if(!consume(&t_s->tokens[*pos],TOKEN_QCOMP,pos))
	{
	    return false;
	}

	return parse_genop2(t_s,pos);
    }
    else
    {
	fprintf(stderr,"Could not parse genop1 starting at: %d:%s\n",*pos,
		t_s->tokens[*pos].str);
	return false;
    }

    return true;
}

bool parse_genop2(struct token_stream *t_s, int *pos, struct arc_plan *plan)
{
    //lookahed to pick between ID and ->
    if(t_s->tokens[*pos].t_type == TOKEN_ID)
    {
	if(!consume(&t_s->tokens[*pos],TOKEN_ID,pos))
	{
	    return false;
	}
    }
    else
    {
	if(!consume(&t_s->tokens[*pos],TOKEN_CALLS,pos))
	{
	    return false;
	}

	if(!consume(&t_s->tokens[*pos],TOKEN_QCOMP,pos))
	{
	    return false;
	}

	if(!consume(&t_s->tokens[*pos],TOKEN_COLON,pos))
	{
	    return false;
	}

	if(!consume(&t_s->tokens[*pos],TOKEN_ID,pos))
	{
	    return false;
	}
    }

    return true;
}

bool parse_op(struct token_stream *t_s, int *pos, struct arc_plan *plan)
{
    if(t_s->tokens[*pos].t_type == TOKEN_PLUS)
    {
	return consume(&t_s->tokens[*pos],TOKEN_PLUS,pos);
    }
    else if(t_s->tokens[*pos].t_type == TOKEN_MINUS)
    {
	return consume(&t_s->tokens[*pos],TOKEN_MINUS,pos);
    }

    return false;
}

bool plan_parse(struct token_stream *t_s, struct arc_plan *plan)
{


    
    
    
    int pos=0;
    return parse_lines(t_s, &pos);
}


int main(int argc, char *argv[])
{

    if(argc != 2)
    {
	fprintf(stderr,"Usage: plan_parser plan_file\n");
	return EXIT_FAILURE;
    }

    char *text = read_file(argv[1]);

    struct token_stream t_s;
    token_stream_init(&t_s);

    if(build_token_stream(text, &t_s))
    {
	plan_parse(&t_s);
    }


    ///Time of truth... wow!
    

    //~= 700 lines, not bad :)

    /*
      THINGS TO IMPROVE BEFORE PATCHING IT IN THE ADAPTATION FRAMEWORK:
      - ERROR CHECKING NEEDS A BIT MORE WORK AND CLEANUP (NO CRASHES BUT NEED BETTER DIAGNOSTICS (for errors at end of line)
      Y CHECK GRAMMAR AND SEE IF WE CAN MOVE FFI TO THE LINE RULE
      ? MAYBE MAKE build_token_stream CHARACTER BASED INSTEAD OF TOKENIZED?
        THIS WAY WE CAN HAVE THINGS LIKE a.b.c[foo] in addition to a.b.c [ foo ] 
    */


    
    
    free(text);
    

    return 0;
}
