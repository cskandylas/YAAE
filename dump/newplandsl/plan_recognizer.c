#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

#include "plan_recognizer.h"
#include "tokenize.h"

bool recognize_number(char *str)
{

    char *dot_pos=strchr(str,'.');
    
    if(!dot_pos)
    {
	return false;
    }
    else
    {

	
	char *p=str;
	while(p != dot_pos)
	{
	    if(!isdigit(*p))
		return false;
	    p++;
	}

	p=dot_pos+1;
	while(*p != '\0')
	{
	    if(!isdigit(*p))
	    {
		return false;
	    }
	    p++;
	}
    }
			 
    return true;
}

bool recognize_id(char *str)
{
    char *p=str;
    while(*p!='\0')
    {
	if(!isalnum(*p))
	{
	    return false;
	}

	p++;
    }

    return true;
}

bool recognize_str(char *str)
{
    
    int len= strlen(str);

    if(str[0]!='"' || str[len-1]!='"')
    {
	return false;
    }
    else
    {
	char *p=str;
	p++;
	while(*p!='"')
	{
	    if(!isprint(*p) || *p=='.')
	    {
		return false;
	    }
	    p++;
	}
	return true;
    }
}

bool recognize_qualified_component(char *str)
{
    char *dot = strchr(str,'.');
    if(!dot)
    {
	return false;
    }
    else
    {
	char *p=str;
	while(p != dot)
	{
	    if(!isalnum(*p))
		return false;
	    p++;
	}

	
	
	p=dot+1;
	while(*p != '\0')
	{
	    if(!isalnum(*p))
	    {
		return false;
	    }
	    p++;
	}
    }

    return true;
}

bool recognize_qualified_interface(char *str)
{
    char *first_dot = strchr(str,'.');
    if(!first_dot)
    {
	return false;
    }
    else
    {
	char *p=str;
	while(p != first_dot)
	{
	    if(!isalnum(*p))
		return false;
	    p++;
	}

	p = first_dot + 1;
	
	char *second_dot = strchr(p,'.');
	if(!second_dot)
	{
	    return false;
	}
	
	while(*p != '.')
	{
	    if(!isalnum(*p))
	    {
		return false;
	    }
	    p++;
	}


	p = second_dot+1;
	    
	while(*p != '\0')
	{
	    if(!isalnum(*p))
	    {
		return false;
	    }
	    p++;
	}	

    }

    return true;
}

enum PLAN_TOKEN_TYPE recognize(char *token)
{

    if(strcmp(token, "[") == 0)
    {
	return TOKEN_L_BRACKET;
    }
    else if(strcmp(token, "]") == 0)
    {
	return TOKEN_R_BRACKET;
    }
    else if(strcmp(token, "{") == 0)
    {
	return TOKEN_L_CBRACE;
    }
    else if(strcmp(token, "}") == 0)
    {
	return TOKEN_R_CBRACE;
    }
    else if(strcmp(token, "=") == 0)
    {
	return TOKEN_EQUALS;
    }
    else if(strcmp(token, "NIL") == 0)
    {
	return TOKEN_NIL;
    }
    else if(strcmp(token, "->") == 0)
    {
	return TOKEN_CALLS;
    }
    else if(strcmp(token, ":") == 0)
    {
	return TOKEN_COLON;
    }
    else if(strcmp(token, "+") ==0)
    {
	return TOKEN_PLUS;
    }
    else if(strcmp(token, "-") ==0)
    {
	return TOKEN_MINUS;
    }
    else if(recognize_number(token))
    {
	return TOKEN_NUMBER;
    }
    else if(recognize_id(token))
    {
	return TOKEN_ID;
    }
    else if(recognize_str(token))
    {
	return TOKEN_STRING;
    }
    else if(recognize_qualified_component(token))
    {
	return TOKEN_QCOMP;
    }
    else if(recognize_qualified_interface(token))
    {
	return TOKEN_QIFACE;
    }
    
    return TOKEN_UNKNOWN;
}

void token_init(struct token *token, char *str, enum PLAN_TOKEN_TYPE t_type)
{
    token->str=str;
    token->t_type=t_type;
}

void token_stream_init(struct token_stream *t_s)
{
    t_s->tokens=malloc(TOKEN_STREAM_INIT_SIZE*sizeof(struct token));
    t_s->num_tokens=0;
    t_s->max_tokens=TOKEN_STREAM_INIT_SIZE;
}

void token_stream_add_token(struct token_stream *t_s, struct token *token)
{

    if(t_s->num_tokens >= t_s->max_tokens)
    {
	t_s->max_tokens*=2;
	//TODO this is bad if realloc fails.
	t_s->tokens=realloc(t_s->tokens,t_s->max_tokens*sizeof(struct token));
    }

    //copy token
    t_s->tokens[t_s->num_tokens]=*token;
    t_s->num_tokens++;    
}

void token_stream_destroy(struct token_stream *t_s)
{
    free(t_s->tokens);
}

bool build_token_stream(char *text, struct token_stream *t_s)
{
    //walk the text with a tokenizer and add a token per recognized word, fail on TOKEN_UNKNOWN

    struct tokenizer plan_tok;    
    tokenizer_init(&plan_tok,text,"\n ");
    char *token = next_token(&plan_tok);

    int pos=0;
    while(token)
    {
	enum PLAN_TOKEN_TYPE t_type = recognize(token);
	//printf("%s is %s\n", token, TOKEN_NAME[t_type]);
	if(t_type == TOKEN_UNKNOWN)
	{
	    fprintf(stderr,"Could not recognize token: %s at %d\n",token,pos);

	    return false;
	}
	
	struct token new_token;
	token_init(&new_token, token, t_type);
	
	token_stream_add_token(t_s,&new_token);
	
	//The tokenizer allocs memory :(
	token=next_token(&plan_tok);
	pos++;
    }

    return t_s;
}
