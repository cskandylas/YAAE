#include "line_draw.h"


void draw_straight_line(WINDOW *win,int from_y,int from_x,int to_y,int to_x)
{
    if(from_y == to_y)
    {
	if(from_x<to_x)
	    mvwhline(win, from_y, from_x, ACS_HLINE, to_x-from_x);
	else
	    mvwhline(win, to_y, to_x, ACS_HLINE, from_x-to_x);	
    }
    else if(from_x==to_x)
    {
	if(from_y<to_y)
	    mvwvline(win, from_y, from_x, ACS_VLINE, to_y-from_y);
	else
	    mvwvline(win, to_y, to_x, ACS_VLINE, from_y-to_y);
    }
    else
    {
	fprintf(stderr,"That is not a straight line my man");
    }
}


enum RELATIVE_POS relative(int Cx,int Cy, int Ox,int Oy)
{
    if(Cx<Ox && Cy==Oy)
	return REL_LE;
    else if(Cx>Ox && Cy == Oy)
	return REL_GE;
    else if(Cx==Ox && Cy < Oy)
	return REL_EL;
    else if(Cx==Ox && Cy > Oy)
	return REL_EG;
    else return REL_OTHER;
}


void fix_segment(WINDOW *win, struct line_segment seg0, struct line_segment seg1)
{
    //Common coordinates
    int Cx=0,Cy=0;
    //Other coordinates in seg0
    int OtherS0x=0,OtherS0y=0;
    //Other coordinates in seg1
    int OtherS1x=0,OtherS1y=0;
    
    //from,from
    if(seg0.from_y==seg1.from_y && seg0.from_x==seg1.from_x)
    {
	Cx=seg0.from_x;
	Cy=seg0.from_y;
	OtherS0x=seg0.to_x;
	OtherS0y=seg0.to_y;
	OtherS1x=seg1.to_x;
	OtherS1y=seg1.to_y;
    }
    //to to
    else if(seg0.to_y==seg1.to_y && seg0.to_x==seg1.to_x)
    {
	Cx=seg0.to_x;
	Cy=seg0.to_y;
	OtherS0x=seg0.from_x;
	OtherS0y=seg0.from_y;
	OtherS1x=seg1.from_x;
	OtherS1y=seg1.from_y;
    }
    //to,from
    else if(seg0.to_y==seg1.from_y && seg0.to_x==seg1.from_x)
    {
	Cx=seg0.to_x;
	Cy=seg0.to_y;
	OtherS0x=seg0.from_x;
	OtherS0y=seg0.from_y;
	OtherS1x=seg1.to_x;
	OtherS1y=seg1.to_y;
    }
    //from,to
    else if(seg0.from_y==seg1.to_y && seg0.from_x==seg1.to_x)
    {
	Cx=seg0.from_x;
	Cy=seg0.from_y;
	OtherS0x=seg0.to_x;
	OtherS0y=seg0.to_y;
	OtherS1x=seg1.from_x;
	OtherS1y=seg1.from_y;
    }

    enum RELATIVE_POS CS0=relative(Cx,Cy,OtherS0x,OtherS0y);
    enum RELATIVE_POS CS1=relative(Cx,Cy,OtherS1x,OtherS1y);
    fprintf(stderr,"%d,%d %d,%d %d,%d %d-%d",Cx,Cy,OtherS0x,OtherS0y,OtherS1x,OtherS1y,CS0,CS1);
    if( (CS0==REL_EL && CS1 == REL_GE ) || (CS1==REL_EL && CS0 == REL_GE ))
    {
	mvwaddch(win,Cy,Cx,ACS_URCORNER);
    }
    else if((CS0==REL_LE && CS1 == REL_EG ) || (CS1==REL_LE && CS0 == REL_EG ))
    {
	mvwaddch(win,Cy,Cx,ACS_LLCORNER);
    }
    else if((CS0==REL_LE && CS1 == REL_EL ) || (CS1==REL_LE && CS0 == REL_EL ))
    {
	mvwaddch(win,Cy,Cx,ACS_ULCORNER);
    }
    else if((CS0==REL_EG && CS1 == REL_GE ) || (CS1==REL_EG && CS0 == REL_GE ))
    {
	mvwaddch(win,Cy,Cx,ACS_LRCORNER);
    }

}
