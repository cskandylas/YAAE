#include <curses.h>

#ifndef LINE_DRAW_H
#define LINE_DRAW_H

enum RELATIVE_POS{REL_LE=0,REL_EL,REL_GE,REL_EG,REL_OTHER};

struct line_segment
{
    int from_y;
    int from_x;
    int to_y;
    int to_x;
    
};

struct ortho_line
{
    struct line_segment *segments;
    int num_segments;
};

void draw_straight_line(WINDOW *win,int from_y,int from_x,int to_y,int to_x);
void draw_ortho_line(WINDOW *win, struct ortho_line line);
void fix_segment(WINDOW *win, struct line_segment seg0, struct line_segment seg1);
enum RELATIVE_POS relative(int Cx,int Cy, int Ox,int Oy);

#endif 
