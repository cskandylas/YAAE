#include <curses.h>

#include "arc_property.h"
#include "arc_interface.h"
#include "arc_component.h"
#include "arc_system.h"
#include <stdint.h>

#ifndef ARC_DRAW_H
#define ARC_DRAW_H


/* Design decisions:
 * 1) Each component has a total width of COMPONENT_CELL_WIDTH cells
 * 2) Interfaces by extension have the same width 
 * 3) Properties are therefore of width COMPONENT_CELL_WIDTH-2 
 * 4) 
 *
 *
 */

#define COMPONENT_CELL_WIDTH 20
#define SPACE_BETWEEN_COMPONENTS 5

struct draw_node
{
    void *entity;
    WINDOW *window;
    int xpos, ypos;
    struct draw_node *children;
    size_t num_children;
};


struct draw_tree
{
    //Root of the tree which represents the whole Managed system
    struct draw_node *root;
    int region_w;
    int region_h;
    
};


void build_component_subtree(int y, int x, struct arc_component *comp,struct draw_node *comp_node);
void destroy_component_subtree(struct draw_node *comp_node);
void draw_component_subtree(struct draw_node *comp_node);

struct draw_tree* build_system_tree(struct arc_system *sys);
void destroy_system_tree(struct draw_tree *tree);
void draw_system_tree(struct draw_tree *tree);

WINDOW*  create_property_window(int y,int x,struct arc_property *prop);
WINDOW*  create_interface_window(int y,int x, struct arc_interface *iface);
WINDOW*  create_component_window(int y,int x, struct  arc_component *comp);

//void draw_system(int y,int x, struct arc_system *sys);
#endif
