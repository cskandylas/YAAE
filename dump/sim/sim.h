

#ifndef ARC_SIMULATION
#define ARC_SIMULATION

//Simulate a strategy exection on a model

void simulate(struct arc_system *sys, struct arc_strategy *strat);

#endif
