#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "arc_property.h"
#include "arc_interface.h"
#include "arc_component.h"
#include "arc_invocation.h"
#include "arc_system.h"
#include "arc_plan.h"
#include "arc_parse.h"
#include "arc_condition.h"
#include "arc_strategy.h"
#include "knowledge_base.h"
#include "sec_clause.h"
#include "sec_analysis.h"
#include "lag.h"
#include "lag_resources.h"
#include "fsm.h"
#include "fsm_resources.h"
#include "monitoring.h"
#include "report_probe.h"
#include "analysis.h"
#include "arc_diff.h"
#include "rbtree.h"
#include "tokenize.h"

#include "attack_rule_analysis.h"


void cleanup_sys(struct arc_system *sys)
{
    
    if(sys)
    {
	for(int i=0;i<sys->num_components;i++)
	{
	    if(sys->components[i])
	    {
		
		for(int j=0;j<sys->components[i]->num_interfaces;j++)
		{
		    if(sys->components[i]->interfaces[j])
		    {
			for(int k=0;k<sys->components[i]->interfaces[j]->num_properties;k++)
			{
			    if(sys->components[i]->interfaces[j]->properties[k])
			    {
				destroy_property(sys->components[i]->interfaces[j]->properties[k]);
				free(sys->components[i]->interfaces[j]->properties[k]);
			    }
			}
			destroy_interface(sys->components[i]->interfaces[j]);
			free(sys->components[i]->interfaces[j]);
		    }
		}

		for(int j=0;j<sys->components[i]->num_properties;j++)
		{
		    if(sys->components[i]->properties[j])
		    {
			destroy_property(sys->components[i]->properties[j]);
			free(sys->components[i]->properties[j]);
		    }
		}
		    
		
		
		destroy_component(sys->components[i]);
		free(sys->components[i]);
	    }
	}

	for(int i=0;i<sys->num_invocations;i++)
	{
	    if(sys->invocations[i])
		free(sys->invocations[i]);
	}

	for(int i=0;i<sys->num_properties;i++)
	{
	    if(sys->properties[i])
	    {
		destroy_property(sys->properties[i]);
		free(sys->properties[i]);
	    }
	}
    
    
	destroy_system(sys);
	free(sys);
    }
}

void cleanup_plan(struct arc_plan *plan)
{
    for(int i=0;i<plan->num_actions;i++)
    {
	if(plan->actions[i])
	{
	    plan_action_cleanup(plan->actions[i]);
	    free(plan->actions[i]);
	}
    }
    plan_destroy(plan);     
}

void cleanup_strategy(struct arc_strategy *strat)
{

    destroy_complex_condition(strat->strat_cond);
    free(strat->strat_cond);
    
    for(int i=0;i<strat->num_cond_plans;i++)
    {	
	if(strat->cond_plans[i])
	{
	    destroy_complex_condition(strat->cond_plans[i]->condition);
	    free(strat->cond_plans[i]->condition);
	    free(strat->cond_plans[i]);
	}
    }
    destroy_strategy(strat);
    free(strat);
}

char *read_file(char *filename)
{

    FILE *ifp=fopen(filename,"r");
    if(!ifp)
    {
	fprintf(stderr,"Could not find %s!\n",filename);
    }
    fseek(ifp, 0, SEEK_END);
    long numbytes = ftell(ifp);
    fseek(ifp, 0, SEEK_SET);	
    char *text=malloc(numbytes+1);
    size_t num_read=0;
    if(!text)
    {
	fprintf(stderr,"Could not allocate memory buffer\n");
	exit(-1);
	
    }
    num_read=fread(text, sizeof(char), numbytes, ifp);
    fclose(ifp);
    text[numbytes]='\0';
    
    return text;
}


int component_compare(const void *component_a,const void *component_b)
{
    const struct arc_component *comp_a=component_a;
    const struct arc_component *comp_b=component_b;
    return strcmp(comp_a->comp_name,comp_b->comp_name);
}

void traverse_comp_tree(struct rb_node *node)
{
   
	if(!node)
		return;
	traverse_comp_tree(node->left);
	printf("%s  \n",((struct arc_component*)node->data)->comp_name);
	traverse_comp_tree(node->right);

	
}




int main(int argc, char *argv[])
{

    if(argc<4)
    {
	printf("Usage: YAADL architecture tactic strategy\n");
	exit(-1);
    }
    else
    {

	
    
	char *text = read_file(argv[1]);
	struct arc_system *sys=arc_parse(text);
	free(text);
	
	if(!sys)
	{
	    fprintf(stderr,"Could not parse architecture file, exiting\n");
	    exit(-1);
	}
	struct arc_system *copy=clone_system(sys);

	printf("Before:\n");
	printf("Num components:%zu ",sys->num_components);
	printf("Num invocations:%zu \n",sys->num_invocations);

	
	
	struct knowledge_base k_b;
	init_knowledge_base(&k_b,sys);
	

	//seq_security_analysis(&k_b,argv[4]);
	//exit(-1);
	
	if(argc==5)
	{
	   // seq_security_analysis(&k_b,argv[4]);
	}
	else if (argc==4)
	{
	    security_analysis(&k_b,true);
	    
	}
	
	//exit(-1);
	
	


	
	
	text=read_file(argv[2]);

	struct tokenizer line_tok;
	tokenizer_init(&line_tok,text,"\n");
	char *p_line=NULL;
	while((p_line=next_token(&line_tok))!=NULL)
	{
	    struct arc_plan *plan=malloc(sizeof(struct arc_plan));
	    char *plan_text=read_file(p_line);
	    plan_parse(plan,plan_text);
	    //printf("%s\n",plan_text);
	    knowledge_base_add_plan(&k_b,plan);
	    free(p_line);
	}
	free(text);
	
	
	
	text=read_file(argv[3]);

	struct arc_strategy *strat=strategy_parse(text,sys,&k_b);
	knowledge_base_add_strat(&k_b,strat);
	free(text);
	strategy_execute(strat);

	printf("After:\n");
	printf("Num components:%zu ",sys->num_components);
	printf("Num invocations:%zu ",sys->num_invocations);
	
	struct rb_tree added,removed,updated;

	rb_tree_init(&removed,component_compare);
	rb_tree_init(&added,component_compare);
	arc_diff(copy,sys,&removed,&added);
	printf("Num changes:%zu \n",added.size);


	//return 0;
	/*
	printf("Removed:\n");
	traverse_comp_tree(removed.root);
	printf("Added/Updated:\n");
	traverse_comp_tree(added.root);
	*/

	
	//inc_security_analysis(&k_b,&removed,&added);

	
	//seq_security_analysis(&k_b,argv[4]);
	//security_analysis(&k_b,false);

	//print_system(sys);
	
	if(argc==5)
	{
	    seq_security_analysis(&k_b,argv[4]);
	}
	else if (argc==4)
	{
	    inc_security_analysis(&k_b,&removed,&added);
	    //security_analysis(&k_b,false);
	}
	
	rb_tree_destroy(&removed);
	rb_tree_destroy(&added);
	    
	
	cleanup_strategy(strat);
	//cleanup_plan(&plan);
	cleanup_sys_clone(copy);
	free(copy);
	
	cleanup_sys(sys);
	destroy_knowledge_base(&k_b);

	
	
	/*
	struct arc_property *prop=malloc(sizeof(struct arc_property));;
	init_str_property(prop,"FOO","BAR");
	struct arc_interface interface;
	init_interface(&interface,"aninterface");
	iface_add_property(&interface,prop);
	struct arc_component component;
	init_component(&component,"acomponent");
	comp_add_iface(&component,&interface);
	comp_add_property(&component,prop);
	

	destroy_component(&component);
	destroy_interface(&interface);
	destroy_str_property(prop);
	free(prop);
	*/
    }
    
    /*
        
    ifp=fopen("AddFrontend.plan","r");
    if(!ifp)
    {
	fprintf(stderr,"Could not find plan: AddFrontend.plan\n");
	exit(-1);
    }
    fseek(ifp, 0L, SEEK_END);
    numbytes = ftell(ifp);
    fseek(ifp, 0, SEEK_SET);	
    text=malloc(numbytes+1);

    if(text == NULL)
	return 1;
    num_read=fread(text, sizeof(char), numbytes, ifp);
    fclose(ifp);
    text[numbytes]='\0';
    
    struct arc_plan addfrontend;
    plan_init(&addfrontend,"AddFrontend");
    plan_parse(&addfrontend,text);
    free(text);
    knowledge_base_add_plan(&k_b,&addfrontend);

    ifp=fopen("AddBackend.plan","r");
    if(!ifp)
    {
	fprintf(stderr,"Could not find plan: AddBackend.plan\n");
	exit(-1);
    }
    fseek(ifp, 0L, SEEK_END);
    numbytes = ftell(ifp);
    fseek(ifp, 0, SEEK_SET);	
    text=malloc(numbytes+1);

    if(text == NULL)
	return 1;
    num_read=fread(text, sizeof(char), numbytes, ifp);
    fclose(ifp);
    text[numbytes]='\0';
    
    struct arc_plan addbackend;
    plan_init(&addbackend,"AddBackend");
    plan_parse(&addbackend,text);
    free(text);
    knowledge_base_add_plan(&k_b,&addbackend);
    
    
    
    ifp=fopen("Tiny.strat","r");
    if(!ifp)
    {
	fprintf(stderr,"Could not find strategy: Tiny.strat\n");
	exit(-1);
    }
    
    fseek(ifp, 0L, SEEK_END);
    numbytes = ftell(ifp);
    fseek(ifp, 0, SEEK_SET);	
    text=malloc(numbytes+1);

    if(text == NULL)
	return 1;
    num_read=fread(text, sizeof(char), numbytes, ifp);
    fclose(ifp);
    text[numbytes]='\0';
    struct arc_strategy *strat=strategy_parse(text,sys,&k_b);
    knowledge_base_add_strat(&k_b,strat);
    */
    // strategy_execute(strat);
//    print_system(sys);



    //start monitor
    /*
    struct monitor *mon=malloc(sizeof(struct monitor));
    init_monitor(mon);
    struct probe *probe=malloc(sizeof(struct probe));
    struct report_probe *r_probe=malloc(sizeof(struct report_probe));
    r_probe->port=6666;
    create_probe(probe,report_probe_init,report_probe_run,report_probe_destroy,r_probe);
    monitor_add_probe(mon,probe);

    monitor_run(mon);
    */

    /*  while(1)
    {
	
	//FOREVER
	//See if adaptation is needed
	struct generic_queue *enabled_strats=analysis_enabled_strats(&k_b);
	queue_destroy(enabled_strats);
	free(enabled_strats);
	
	}*/
    
    // destroy_monitor(mon);
    //start_probe(probe);
    //stop_probe(probe);
    //destroy_probe(probe);
    //free(r_probe);
    //free(probe);
    
    // free(mon);
    
    


    //TODO: FIX CLEANUP WHEN STRATEGY PARSING FAILS!!
    //Cleanup
//    cleanup_strategy(strat);
//    cleanup_plan(&addfrontend);
//    cleanup_plan(&addbackend);
//    free(text);


    //   security_analysis(&k_b);
    
    
    
    return 0;
}
