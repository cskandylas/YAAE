#include "task.h"
#include <limits.h>

/*
  Consider this struct to follow an opaque pointer interface.
  This decision was made to seamlessly handle thread_safety.
  An alternative would be making task_id and status Atomic.
  I chose c99 compatibility, thus, *do not* manipulate the task
  contents without holding the lock! Convenience functions are
  provided under thread_safe functions
*/

struct task
{    
    //state variables
    uint32_t task_id;                 
    enum TASK_STATUS status;          
    //lock
    pthread_mutex_t lock;             
    //contexts
    void *task_ctx;                   
    void *cb_ctx;                     
    //function pointers
    task_run_fn run;                  
    task_done_fn done;               
};

void create_task(struct task *task,
		 uint32_t task_id,
		 task_run_fn run,
		 task_done_fn done,
		 void *task_ctx,
		 void *cb_ctx)
{

    pthread_mutex_init(&task->lock,NULL);
    task->task_id=task_id;
    task->status=TASK_CREATED;
    //setup function pointers and contexts
    task->run=run;
    task->done = done;
    task->task_ctx=task_ctx;
    task->cb_ctx=cb_ctx;
}

void destroy_task(struct task *task)
{
    //cleanup the lock
    pthread_mutex_destroy(&task->lock);
}

inline uint32_t task_get_id(struct task *task)
{
    uint32_t task_id = UINT32_MAX;
    pthread_mutex_lock(&task->lock);
    task_id = task->task_id;
    pthread_mutex_unlock(&task->lock);

    return task_id;
}

inline enum TASK_STATUS task_get_status(struct task *task)
{
    enum TASK_STATUS status = TASK_STATUS_UNKNOWN;
    pthread_mutex_lock(&task->lock);
    status = task->status;
    pthread_mutex_unlock(&task->lock);

    return status;
}

inline void *task_get_ctx(struct task *task)
{
    void *ctx = NULL;
    pthread_mutex_lock(&task->lock);
    ctx = task->task_ctx;
    pthread_mutex_unlock(&task->lock);
    return ctx;
}

void *task_get_cb_ctx(struct task *task)
{
    void *cb_ctx = NULL;
    pthread_mutex_lock(&task->lock);
    cb_ctx = task->cb_ctx;
    pthread_mutex_unlock(&task->lock);
    return cb_ctx;
}

inline void task_set_status(struct task *task, enum TASK_STATUS status)
{
    
    pthread_mutex_lock(&task->lock);
    task->status = status;
    pthread_mutex_unlock(&task->lock);
}


inline size_t get_task_struct_size()
{
    return sizeof(struct task);
}

inline void task_done(struct task *task)
{
    if(task->done)
    {
	task->done(task);
    }
}
inline void task_run(struct task *task)
{
    task->run(task);
}



void default_task_done(struct task *task)
{
    //Do nothing
}

