#include "work_thief_thread.h"
#include "task_engine.h"
#include <stdlib.h>

static void *thread_func(void *r_t);

void work_thief_init(struct work_thief *w_t)
{
    w_t->head=w_t->tail=NULL;

    pthread_mutex_init(&w_t->lock,NULL);
    pthread_cond_init(&w_t->signal,NULL);

    w_t->state = W_T_RUNNING;
    w_t->t_q_size = 0;

    //we don't really need to malloc here
    w_t->running = malloc(sizeof(struct thief_task));
    w_t->running->task = NULL;
    w_t->running->state = R_T_NONE;
    w_t->running->thief = w_t;
    pthread_create(&w_t->thread_id,NULL,thread_func,w_t->running);

    pthread_detach(w_t->thread_id);
}

void work_thief_destroy(struct work_thief *w_t)
{

    return;
    //pthread_mutex_unlock(&w_t->lock);
    //pthread_cond_broadcast(&w_t->signal);

    pthread_mutex_lock(&w_t->lock);
    w_t->state=W_T_STOPPED;
    pthread_mutex_unlock(&w_t->lock);

    
    while(w_t->running->state != R_T_ABORTED)
    {
	
    }
    free(w_t->running);
    

    pthread_cond_destroy(&w_t->signal);
    pthread_mutex_destroy(&w_t->lock);
    //wait for cleanup

}


void work_thief_add_task(struct work_thief *w_t, struct task *t)
{
    //lock the queue while adding a task
    pthread_mutex_lock(&w_t->lock);
    //create a new task entry
    struct task_entry *new_entry = malloc(sizeof(struct task_entry));
    new_entry->task = t;
    new_entry->next = NULL;

    //add the entry to the end of the tasks queue
    if(!w_t->head)
    {
	w_t->head=w_t->tail=new_entry;
	w_t->head->next=w_t->tail->next=NULL;
    }
    else
    {
	w_t->tail->next=new_entry;
	w_t->tail=new_entry;
    }
    w_t->t_q_size++;
    //get somone to do the task
    pthread_cond_signal(&w_t->signal);
    pthread_mutex_unlock(&w_t->lock);
}

//consider making this atomic instead of locking and unlocking just to assign a variable
size_t work_thief_queue_size(struct work_thief *w_t)
{
    size_t size=UINT32_MAX;
    pthread_mutex_lock(&w_t->lock);
    size = w_t->t_q_size;
    pthread_mutex_unlock(&w_t->lock);

    return size;
}

struct task *work_thief_tail_steal(struct work_thief *w_t)
{
    struct task *loot = NULL;
    //Try to get a lock to steal a task if we fail, it's okay
    if(pthread_mutex_trylock(&w_t->lock) == 0)
    {
	struct task_entry *entry = w_t->head;
	while(entry && entry->next!=w_t->tail)
	{
	    entry = entry->next;
	}
	//entry points to the previous element to the tail or to nothing.
	if(entry)
	{
	    loot = entry->next->task;
	    entry->next=NULL;
	    w_t->tail=entry;
	    w_t->t_q_size--;
	}
		
	pthread_mutex_unlock(&w_t->lock);
    }
    return loot;
}

static void *thread_func(void *r_t)
{
    //do nothing for now.
    struct thief_task *running = r_t;
    struct work_thief *w_t = running->thief;
    struct task_entry *old_head = NULL;
    w_t->state = W_T_RUNNING;
    //printf("Starting thread: %u\n",w_t->thread_id);
    while(1)
    {
	
	pthread_mutex_lock(&w_t->lock);
	
	//lock and check our queue
	while(!w_t->head && w_t->state != W_T_STOPPED )
	{
		pthread_cond_wait(&w_t->signal,&w_t->lock);
	}
	if(w_t->state == W_T_STOPPED)
	{
	    break;
	}

	//There's something at the head of our queue
	running->task=w_t->head->task;
	old_head = w_t->head;
	w_t->head= w_t->head->next;
	w_t->t_q_size--;
	    
	pthread_cond_signal(&w_t->signal);
	pthread_mutex_unlock(&w_t->lock);

	if(running->task)
	{
	    //clean up old task
	    free(old_head);
	    //printf("preparing to run task %u\n",running->task->task_id);
	    //run the task main body
	    
	    task_run(running->task);

	    //This is possibly wrong, we should probably make status atomic
	    //Alternative, introduce a mutex on the task struct
	    task_set_status(running->task, TASK_COMPLETE);
	    
	    //done callbacks
	    task_engine_done(running->task);
	    task_done(running->task);
	    
	}

	
	//If our queue is empty, try to steal from someone else.
	size_t queue_size = work_thief_queue_size(w_t);
	if(queue_size == 0)
	{
	    task_engine_try_steal(w_t);
	}
		
    }
    //dropped out of loop but we still hold the lock
    pthread_cond_signal(&w_t->signal);
    pthread_mutex_unlock(&w_t->lock);

    running->state = R_T_ABORTED;

    pthread_exit(NULL);
    
}



