#include "thread_pool.h"
#include "task.h"
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <stdbool.h>

//forward declare for simplicity
static void *thread_func(void *t_p);

void thread_pool_init(struct thread_pool *thread_pool, int num_running)
{

    //start with an empty queue
    thread_pool->head=thread_pool->tail=NULL;

    //create the running tasks
    thread_pool->running=malloc(num_running * sizeof(struct pool_task));
    thread_pool->num_running=num_running;

    pthread_mutex_init(&thread_pool->lock,NULL);
    pthread_cond_init(&thread_pool->signal,NULL);

    thread_pool->state=T_P_RUNNING;
    //create the running threads
    for(int i=0;i<num_running;i++)
    {
	struct pool_task *r_t=&thread_pool->running[i];
	r_t->thread_pool=thread_pool;
	//set the thread to running and the task to NULL
	pthread_create(&r_t->thread_id,NULL,
		       thread_func,r_t);
	r_t->task=NULL;
	pthread_detach(r_t->thread_id);
    }

    //busy wait for threads to initialize

    
    
}



void thread_pool_destroy(struct thread_pool *thread_pool)
{

    //set pool status to stopped
    pthread_mutex_lock(&thread_pool->lock);
    thread_pool->state=T_P_STOPPED;
    pthread_mutex_unlock(&thread_pool->lock);
    pthread_cond_broadcast(&thread_pool->signal);

    bool all_done=false;

    //spin until all thread tasks are aborted
    while(!all_done)
    {
	pthread_mutex_lock(&thread_pool->lock);
	
	all_done=true;
	for(size_t i=0;i<thread_pool->num_running;i++)
	{
	    if(thread_pool->running[i].state!=R_T_ABORTED)
		all_done=false;
	}
	pthread_mutex_unlock(&thread_pool->lock);
	pthread_cond_broadcast(&thread_pool->signal);
    }
    
    //free the threads array and the queue array
    free(thread_pool->running);
    pthread_cond_destroy(&thread_pool->signal);
    pthread_mutex_destroy(&thread_pool->lock);
    
}

void thread_pool_add_task(struct thread_pool *thread_pool, struct task *task)
{

    //lock the queue, add a task, then unlock the queue
    pthread_mutex_lock(&thread_pool->lock);
    //create the task entry
    struct task_entry *new_entry=malloc(sizeof(struct task_entry));
    new_entry->task=task;
    new_entry->next=NULL;

    //add the new entry to the end of the queue
    if(!thread_pool->head)
    {
	thread_pool->head=thread_pool->tail=new_entry;
	thread_pool->head->next=thread_pool->tail->next=NULL;
    }
    else
    {
	thread_pool->tail->next=new_entry;
	thread_pool->tail=new_entry;
    }
    //we have work so let's get someone to do it.
    pthread_cond_signal(&thread_pool->signal);
    pthread_mutex_unlock(&thread_pool->lock);
}




static void *thread_func(void *r_t)
{
    struct pool_task *running=r_t;
    struct thread_pool *thread_pool = running->thread_pool;
    struct task_entry *old_head=NULL;
    while(1)
    {
	//fetch the next task in the queue if one exists
	
	//lock the queue
	pthread_mutex_lock(&thread_pool->lock);

	//wait on the condition variable
	while(!thread_pool->head && thread_pool->state!=T_P_STOPPED)
	{    
	    pthread_cond_wait(&thread_pool->signal,&thread_pool->lock);
	}
	if(thread_pool->state==T_P_STOPPED)
	{
	    //We're done here
	    break;
	}
	
	//There's definitely something at the head of the queue 	    
	running->task=thread_pool->head->task;
	old_head=thread_pool->head;
	thread_pool->head=thread_pool->head->next;

	pthread_cond_signal(&thread_pool->signal);
	pthread_mutex_unlock(&thread_pool->lock);

	
	
	if(running->task)
	{
	    //clean up the previous node
	    free(old_head);
	    task_run(running->task);
	    
	    //job done
	    task_set_status(running->task,TASK_COMPLETE);
	    
	    //callback on done
	    task_done(running->task);
	    
	    
	}

	
    }
    //we dropped out cause the pool was stopped but still hold the lock
    running->state=R_T_ABORTED;
    pthread_cond_signal(&thread_pool->signal);
    pthread_mutex_unlock(&thread_pool->lock);

    pthread_exit(NULL);
    
    
}
