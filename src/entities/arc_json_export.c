#include "arc_json_export.h"

#include <stdbool.h>

bool arc_json_dump_property(struct string_buffer *s_b, struct arc_property *a_p)
{
    if(a_p->type==P_INT)
    {
	return string_buffer_add_fmt(s_b, "\t\t{\"%s\" : %d}",a_p->name, a_p->v_int);
    }
    else if(a_p->type==P_DOUBLE)
    {
	return string_buffer_add_fmt(s_b, "\t\t{\"%s\" : %f}",a_p->name, a_p->v_dbl);
    }
    else
    {
	return string_buffer_add_fmt(s_b, "\t\t{\"%s\" : \"%s\"}",a_p->name, a_p->v_str);
    }
    
    
}

bool arc_json_dump_interface(struct string_buffer *s_b, struct arc_interface *a_i)
{
    bool res = false;
    res = string_buffer_add_fmt(s_b, "\n\t\t\t\t{\n\t\t\t\t\t\"Name\" : \"%s\",",a_i->if_name);

    res = string_buffer_add_str(s_b, "\n\t\t\t\t\t\"Properties\" : [\n");
    
    //make sure we've not failed to add to the buffer
    for(size_t i = 0; i < a_i->num_properties && res;i++)
    {
	res = string_buffer_add_str(s_b, "\t\t\t\t");
	res = arc_json_dump_property(s_b, a_i->properties[i]);
	if(i < a_i->num_properties-1)
	{
	    res = string_buffer_add_str(s_b, ",\n");
	}
	else
	{
	    res = string_buffer_add_str(s_b, "\n");
	}
    }
    
    res = string_buffer_add_str(s_b, "\t\t\t\t\t]");
    if(res)
    {
	return string_buffer_add_str(s_b, "\n\t\t\t\t}");
    }
    return false;
}


bool arc_json_dump_component(struct string_buffer *s_b, struct arc_component *a_c)
{
    bool res = false;
    res = string_buffer_add_fmt(s_b, "\n\t\t{\n\t\t\t\"Name\" : \"%s\",",a_c->comp_name);

    res = string_buffer_add_str(s_b, "\n\t\t\t\"Interfaces\" : [\n");
    //make sure we've not failed to add to the buffer
    for(size_t i = 0; i < a_c->num_interfaces && res;i++)
    {
	res = arc_json_dump_interface(s_b, a_c->interfaces[i]);
	if(i < a_c->num_interfaces - 1)
	{
	    res = string_buffer_add_str(s_b, ",");
	}
	else
	{
	    res = string_buffer_add_str(s_b, "\n");
	}
    }
    
    res = string_buffer_add_str(s_b, "\t\t\t],");

    res = string_buffer_add_str(s_b, "\n\t\t\t\"Properties\" : [\n");
    //make sure we've not failed to add to the buffer
    for(size_t i = 0; i < a_c->num_properties && res;i++)
    {
	res = string_buffer_add_str(s_b, "\t\t");
	res = arc_json_dump_property(s_b, a_c->properties[i]);
	if(i < a_c->num_properties-1)
	{
	    res = string_buffer_add_str(s_b, ",\n");
	}
	else
	{
	    res = string_buffer_add_str(s_b, "\n");
	}
    }
    res = string_buffer_add_str(s_b, "\t\t\t]");
    
    if(res)
    {
	return string_buffer_add_str(s_b, "\n\t\t}");
    }
    return false;
}

bool arc_json_dump_invocation(struct string_buffer *s_b, struct arc_invocation *a_i)
{
    bool res = false;
    
    res = string_buffer_add_str(s_b, "\n\t\t{\n");
    res = string_buffer_add_fmt(s_b, "\t\t\"Source\" : \"%s\",\n",a_i->from->comp_name);
    res = string_buffer_add_fmt(s_b, "\t\t\"Target\" : \"%s\",\n",a_i->to->comp_name);
    res = string_buffer_add_fmt(s_b, "\t\t\"Interface\" : \"%s\"\n",a_i->iface->if_name);
    
    if(res)
    {
	return string_buffer_add_str(s_b, "\t\t}");
    }
    return false;
}

bool arc_json_dump_system(struct string_buffer *s_b, struct arc_system *a_s)
{
    bool res = false;
    res = string_buffer_add_fmt(s_b, "{\n\t\"Name\" : \"%s\",",a_s->sys_name);

    res = string_buffer_add_str(s_b, "\n\t\"Components\" : [");

    
    
    //make sure we've not failed to add to the buffer
    for(size_t i = 0; i < a_s->num_components && res;i++)
    {
	res = arc_json_dump_component(s_b, a_s->components[i]);
	if(i < a_s->num_components-1)
	{
	    res = string_buffer_add_str(s_b, ",");
	}
	else
	{
	    res = string_buffer_add_str(s_b, "\n");
	}
    }
    res = string_buffer_add_str(s_b, "\t],");

    
    res = string_buffer_add_str(s_b, "\n\t\"Invocations\" : [");
    //make sure we've not failed to add to the buffer
    for(size_t i = 0; i < a_s->num_invocations && res;i++)
    {
	res = arc_json_dump_invocation(s_b, a_s->invocations[i]);
	if(i < a_s->num_invocations-1)
	{
	    res = string_buffer_add_str(s_b, ",");
	}
	else
	{
	    res = string_buffer_add_str(s_b, "\n");
	}

    }
    res = string_buffer_add_str(s_b, "\t],");
    
    //make sure we've not failed to add to the buffer
    res = string_buffer_add_str(s_b, "\n\t\"Properties\" : [\n");
    for(size_t i = 0; i < a_s->num_properties && res;i++)
    {
	res = arc_json_dump_property(s_b, a_s->properties[i]);
	if(i < a_s->num_properties-1)
	{
	    res = string_buffer_add_str(s_b, ",\n");
	}
	else
	{
	    res = string_buffer_add_str(s_b, "\n");
	}
    }
    res = string_buffer_add_str(s_b, "\t]");
    
    if(res)
    {
	return string_buffer_add_str(s_b, "\n}");
    }
    return false;
}
