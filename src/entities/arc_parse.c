#define  _XOPEN_SOURCE 500
#include "arc_parse.h"
#include "parse_common.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "tokenize.h"
#include "parse_common.h"
#include "arc_recognizer.h"


static bool parse_system(struct token_stream *t_s, struct arc_system *system);
static bool parse_components(struct token_stream *t_s, struct arc_system *system, size_t *pos);
static bool parse_invocations(struct token_stream *t_s, struct arc_system *system, size_t *pos);
static bool parse_properties(struct token_stream *t_s, void *entity, enum ARC_ENTITY_TYPE type, size_t *pos);
static bool parse_interfaces(struct token_stream *t_s, struct arc_component *comp, size_t *pos);
static struct arc_component* parse_component(struct token_stream *t_s, struct arc_system *system, size_t *pos);
static struct arc_invocation* parse_invocation(struct token_stream *t_s, struct arc_system *system, size_t *pos);
static struct arc_interface* parse_interface(struct token_stream *t_s, struct arc_component *comp, size_t *pos);
static struct arc_property* parse_property(struct token_stream *t_s, size_t *pos);


struct arc_system* arc_parse(char *txt)
{

	struct token_stream t_s;
    token_stream_init(&t_s);



	size_t len  = strlen(txt);
	for(size_t i = 0 ; i < len  ; i++)
	{
		if(txt[i] == '\'')	
		{
			txt[i] = '"';
		}
	}
	
    if(!build_token_stream(txt, &t_s))
	{
		fprintf(stderr, "[Panic] Could not parse system\n");
		token_stream_destroy(&t_s);
		exit(0);
	}

	
    
    struct arc_system *sys = malloc(sizeof(struct arc_system));
    
    if(parse_system(&t_s,sys))
    {
		token_stream_destroy(&t_s);
		return sys;
    }
	
    token_stream_destroy(&t_s);
    return NULL;
    
}


static bool parse_system(struct token_stream *t_s, struct arc_system *system)
{


	/*Might make a lot of sense to embed pos to the token_stream */
	size_t pos = 0;

	if(!expect(t_s,"System",pos) ||!consume_p(t_s,TOKEN_ID, &pos) )
	{		
		return false;
	}
	
    if(!consume_p(t_s, TOKEN_ID, &pos))
	{
		return false;
	}

	//Guaranteed to have a valid id.
	char *sys_name = t_s->tokens[pos-1].str;
	init_system(system,sys_name);

	if(!consume_p(t_s,TOKEN_L_CBRACE, &pos))
	{
		return false;
	}
	if(peek_str(t_s,TOKEN_ID,"Component",&pos))
	{
		parse_components(t_s,system,&pos);
	}
	if (peek_str(t_s,TOKEN_ID,"Invocation",&pos))
	{
		parse_invocations(t_s, system, &pos);
	}
	if (peek_str(t_s,TOKEN_ID,"Property",&pos))
	{
		parse_properties(t_s, system,ARC_ENT_SYSTEM, &pos);
	}
	
	if(!consume_p(t_s,TOKEN_R_CBRACE, &pos))
	{
		return false;
	}
	
    return true;    
}

static bool parse_components(struct token_stream *t_s, struct arc_system *system, size_t *pos)
{

    struct arc_component *comp=parse_component(t_s,system,pos);
    while(comp!=NULL)
    {
		sys_add_component(system,comp);
		if(peek_str(t_s, TOKEN_ID,"Component",pos))
		{
			comp=parse_component(t_s,system,pos);
		}
		else
		{
			comp = NULL;
		}
    }

    return true;
}
static bool parse_invocations(struct token_stream *t_s, struct arc_system *system, size_t *pos)
{

    struct arc_invocation *invo=parse_invocation(t_s, system, pos);
    while(invo!=NULL)
    {
	sys_add_invocation(system,invo);
	invo=parse_invocation(t_s, system, pos);
    }
	return true;
}
static bool parse_properties(struct token_stream *t_s, void *entity, enum ARC_ENTITY_TYPE type, size_t *pos)
{
	struct arc_property *prop=parse_property(t_s, pos);
	while(prop!=NULL)
	{
		switch(type)
		{
		case ARC_ENT_SYSTEM:
			sys_add_property(entity,prop);
			break;
		case ARC_ENT_COMPONENT:
			comp_add_property(entity,prop);
			break;
		case ARC_ENT_INTERFACE:
			iface_add_property(entity,prop);
			break;
		}
		if(peek_str(t_s,TOKEN_ID, "Property",pos))
		{
			prop=parse_property(t_s, pos);
		}
		else
		{
			prop = NULL;
		}
    }
	return true;
}
static struct arc_component* parse_component(struct token_stream *t_s, struct arc_system *system, size_t *pos)
{

	
	if(!expect(t_s,"Component",*pos) || !consume_p(t_s,TOKEN_ID, pos))
	{		
		return NULL;
	}

	if(!consume_p(t_s,TOKEN_ID, pos))
	{
		return NULL;
	}

	char *comp_name = t_s->tokens[*pos-1].str;
	
	struct arc_component *comp=malloc(sizeof(struct arc_component));
	init_component(comp,comp_name);
	
	if(!consume_p(t_s,TOKEN_L_CBRACE, pos))
	{
		free(comp);
		return NULL;
	}
	
	if(peek_str(t_s,TOKEN_ID, "Interface", pos))
	{
		parse_interfaces(t_s,comp,pos);
	}
	if(peek_str(t_s,TOKEN_ID, "Property", pos))
	{
		parse_properties(t_s, comp,ARC_ENT_COMPONENT, pos);
	}
	
	
	if(!consume_p(t_s,TOKEN_R_CBRACE, pos))
	{
		free(comp);
		return NULL;
	}

	
    return comp;
}

static bool parse_interfaces(struct token_stream *t_s, struct arc_component *comp, size_t *pos)
{

	
	
    struct arc_interface *iface=parse_interface(t_s,comp,pos);
    while(iface!=NULL)
    {
		comp_add_iface(comp,iface);
		if(peek_str(t_s,TOKEN_ID,"Interface",pos))
		{
			iface=parse_interface(t_s,comp,pos);
		}
		else
		{
			iface =  NULL;
		}
	   
    }
	

	return true;
}

static struct arc_interface* parse_interface(struct token_stream *t_s, struct arc_component *comp, size_t *pos)
{

	

	if(!expect(t_s,"Interface", *pos))
	{
		return NULL;
	}
	
	if(!consume_p(t_s,TOKEN_ID, pos))
	{		
		return NULL;
	}
	
	if(!consume_p(t_s,TOKEN_ID, pos))
	{
		return NULL;
	}
	
	char *if_name = t_s->tokens[*pos-1].str;
	
	struct arc_interface *iface=malloc(sizeof(struct arc_interface));
	init_interface(iface,if_name);
	
	if(!consume_p(t_s,TOKEN_L_CBRACE, pos))
	{
		free(iface);
		return NULL;
	}

	if(peek_str(t_s,TOKEN_ID,"Property", pos))
	{
		parse_properties(t_s, iface, ARC_ENT_INTERFACE, pos);
	}


	if(!consume_p(t_s,TOKEN_R_CBRACE, pos))
	{
		free(iface);
		return NULL;
	}
	
	return iface;

}

static struct arc_invocation* parse_invocation(struct token_stream *t_s, struct arc_system *system, size_t *pos)
{


	if(!expect(t_s,"Invocation",*pos) || !consume_p(t_s,TOKEN_ID, pos) )
	{		
		return NULL;
	}
	
	if(!consume_p(t_s,TOKEN_L_CBRACE, pos))
	{

		return false;
	}

	if(!expect(t_s,"Source",*pos) || !consume_p(t_s,TOKEN_ID, pos))
	{		
		return NULL;
	}

	if(!consume_p(t_s,TOKEN_ID, pos))
	{		
		return NULL;
	}

	char *src=t_s->tokens[*pos-1].str;
	
	if(!expect(t_s,"Target",*pos) || !consume_p(t_s,TOKEN_ID, pos))
	{		
		return NULL;
	}

		
	if(!consume_p(t_s,TOKEN_ID, pos))
	{		
		return NULL;
	}
	
	char *tgt=t_s->tokens[*pos-1].str;

	if(!consume_p(t_s,TOKEN_ID, pos) || !expect(t_s,"Interface",*pos))
	{		
		return NULL;
	}

	if(!consume_p(t_s,TOKEN_ID, pos))
	{		
		return NULL;
	}

	char *if_name=t_s->tokens[*pos-1].str;
	
	
	struct arc_invocation *invo=malloc(sizeof(struct arc_invocation));
	struct arc_component *src_comp=sys_find_component(system,src);
	struct arc_component *tgt_comp=sys_find_component(system,tgt);
	struct arc_interface *iface=comp_find_iface(tgt_comp,if_name);
	if(!src_comp)
	{
		fprintf(stderr,"%s not found\n",src);
		return NULL;
	}
	if(!tgt_comp)
	{
		fprintf(stderr,"%s not found\n",tgt);
		return NULL;
	}
	if(!if_name)
	{
		fprintf(stderr,"%s not found\n",if_name);
		return NULL;
	}
	init_invocation(invo,iface,src_comp,tgt_comp);

	if(!consume_p(t_s,TOKEN_R_CBRACE, pos))
	{
		return NULL;
	}
	
	return invo;

    
}
static struct arc_property* parse_property(struct token_stream *t_s, size_t *pos)
{
	
	
	if(!expect(t_s,"Property",*pos) || !consume_p(t_s,TOKEN_ID, pos) )
	{		
		return NULL;
	}
	
	if(!consume_p(t_s,TOKEN_ID, pos))
	{
		return NULL;
	}
	
	char *prop_name = t_s->tokens[*pos-1].str;
	struct arc_property *prop=malloc(sizeof(struct arc_property));

	if(!consume_p(t_s,TOKEN_EQUALS, pos))
	{
		free(prop);
		return NULL;
	}

	
	if(peek_type(t_s,TOKEN_NUMBER,pos))
	{
		consume_p(t_s,TOKEN_NUMBER,pos);
		if(strchr(t_s->tokens[*pos-1].str,'.'))
		{
			init_dbl_property(prop,prop_name,atof(t_s->tokens[*pos-1].str));
			return prop;
		}
		else
		{
			init_int_property(prop,prop_name,atoi(t_s->tokens[*pos-1].str));
			return prop;
			
		}
	}
	else if(peek_type(t_s,TOKEN_STRING,pos))
	{
		consume_p(t_s,TOKEN_STRING,pos);
		strip_quotes(t_s->tokens[*pos-1].str);
		init_str_property(prop,prop_name,t_s->tokens[*pos-1].str);
		return prop;
	}
	else
	{
		return NULL;
	}
	
}



struct arc_property* parse_txt_property(char *txt,struct arc_system *sys)
{
    struct tokenizer dot_tok;
    tokenizer_init(&dot_tok,txt,".");
    int num_tokens=count_tokens(&dot_tok);
    struct arc_property *ret;
    if(num_tokens==1)
    {
	//"N_COMPONENTS" -> System property
	char *prop_name=next_token(&dot_tok);
	ret=sys_find_property(sys,prop_name);
    }
    else if(num_tokens==2)
    {
	//Backend.N_FRONTENDS -> Component property
	char *comp_name=next_token(&dot_tok);
	struct arc_component *comp=sys_find_component(sys,comp_name);
	if(comp)
	{
	    char *prop_name=next_token(&dot_tok);
	    ret=comp_find_property(comp,prop_name);
	}
	else
	{
	    fprintf(stderr,"Component %s not found\n",comp_name);
	}
    }
    else if(num_tokens==3)
    {
	//Backend.login.username -> Interface property
	char *comp_name=next_token(&dot_tok);
	struct arc_component *comp=sys_find_component(sys,comp_name);
	if(comp)
	{
	    char *iface_name=next_token(&dot_tok);
	    struct arc_interface *iface=comp_find_iface(comp,iface_name);
	    if(iface)
	    {
	    char *prop_name=next_token(&dot_tok);
	    ret=iface_find_property(iface,prop_name);
	    }
	    else
	    {
		fprintf(stderr,"Interface %s not found\n",iface_name);
	    }
	}
	else
	{
	    fprintf(stderr,"Component %s not found\n",comp_name);
	}
    }
    tokenizer_cleanup(&dot_tok);
    return ret;
}

struct arc_value parse_txt_value(char *txt)
{
    struct arc_value value;
    double dbl_val=atof(txt);
    if(strchr(txt, '.') != NULL && (dbl_val!=0.0 || strcmp(txt,"0.0")==0))
    {
	value.v_dbl=dbl_val;
	value.type = V_DOUBLE;
	return value;
    }

    int int_val=atoi(txt);
    if(int_val!=0 || strcmp(txt,"0")==0)
    {
	value.v_int=int_val;
	value.type = V_INT;
	return value;
    }

    
    value.v_str=malloc(strlen(txt)+1);
    strcpy(value.v_str,txt);
    value.type = V_STRING;
    return value;
    
}

enum ARC_COMPARISON_OPERATOR parse_cond_value(char *txt)
{
    if(strcmp(txt,"<=")==0)
	return ARC_COND_LE;
    else if(strcmp(txt,">=")==0)
	return ARC_COND_GE;
    else if(strcmp(txt,"=")==0)
	return ARC_COND_EQ;
    else if(strcmp(txt,"<")==0)
	return ARC_COND_LT;
    else if(strcmp(txt,">")==0)
	return ARC_COND_GT;
    else
	return ARC_COND_INVALID;
    
}

struct arc_simple_condition* parse_txt_condition(char *txt, struct arc_system *sys)
{
    struct arc_simple_condition *cond=malloc(sizeof(struct arc_simple_condition));

    struct tokenizer op_tok;
    tokenizer_init(&op_tok,txt," ");
    char *cond_txt=next_token(&op_tok);
    size_t count=0;
    while(cond_txt)
    {
	cond_txt=next_token(&op_tok);
	count++;
    }
    tokenizer_cleanup(&op_tok);

    tokenizer_init(&op_tok,txt," ");

    if(count!=3)
	return NULL;

    char *prop_name=next_token(&op_tok);
    struct arc_property *prop=parse_txt_property(prop_name,sys);
    
    if(prop)
    {
	cond->prop=prop;
	char *op_name=next_token(&op_tok);
	enum ARC_COMPARISON_OPERATOR op=parse_cond_value(op_name);
	if(op!=ARC_COND_INVALID)
	{
	    cond->op=op;
	    char *const_value=next_token(&op_tok);
	    
	    struct arc_value value=parse_txt_value(const_value);
	    if(value.type == V_INT)
	    {
		cond->v_int=value.v_int;
	    }
	    else if(value.type == V_DOUBLE)
	    {
		cond->v_dbl=value.v_dbl;
	    }
	    else if(value.type == V_STRING)
	    {
		cond->v_str=value.v_str;
	    }
	    
	    return cond;
	}
	else
	{
	    fprintf(stderr,"Invalid operator %s\n",prop_name);

	}
	
    }
    else
    {
	fprintf(stderr,"Could not find property [%s]\n",prop_name);
    }
    tokenizer_cleanup(&op_tok);
    return cond;
}


struct arc_complex_condition* parse_txt_complex_condition(char *txt,
							  struct arc_system *sys)
{
    char *reset=txt;
    struct arc_complex_condition* cond=malloc(sizeof(struct arc_complex_condition));
    struct tokenizer op_tok;
    tokenizer_init(&op_tok,txt,"(&|)");
    char *cond_txt=next_token(&op_tok);
    size_t count=0;
    while(cond_txt)
    {
	cond_txt=next_token(&op_tok);
	count++;
	
    }

    tokenizer_cleanup(&op_tok);
    if(count<1)
    {
	return NULL;
    }
    
    struct arc_simple_condition *simple_conds=malloc(count*sizeof(struct arc_simple_condition));
    enum ARC_BOOLEAN_OPERATOR *ops=malloc((count-1)*sizeof(enum ARC_BOOLEAN_OPERATOR));
    
    tokenizer_init(&op_tok,reset,"(&|)");
    for(size_t i=0;i<count;i++)
    {
	cond_txt=next_token(&op_tok);   
	struct arc_simple_condition *simple_cond=parse_txt_condition(cond_txt,sys);
	if(simple_cond)
	{
	    memcpy(&simple_conds[i],simple_cond,sizeof(struct arc_simple_condition));
	}
	else
	{
	    fprintf(stderr,"Failed to make simple condition out of %s\n",cond_txt);
	}
    }
    tokenizer_cleanup(&op_tok);
    

    tokenizer_init(&op_tok,reset,"(&|)");
    for(int i=0;i<count-1;i++)
    {
	//printf("Next delim: %c\n",next_delim(&op_tok,"&|"));
	char c=next_delim(&op_tok,"&|");
	if(c=='&')
	{
	    ops[i]=ARC_COND_AND;
	}
	else if(c=='|')
	{
	    ops[i]=ARC_COND_OR;
	}
    }
    cond->num_simple_conditions=count;
    cond->simple_conds=simple_conds;
    cond->ops=ops;
    tokenizer_cleanup(&op_tok);
    return cond;
}

struct arc_simple_condition* parse_simple_cond(char **tokens, size_t num_tokens, size_t *cur_token, struct arc_system *sys)
{
    char *prop_name=parse_name(tokens,num_tokens,cur_token);
    if(prop_name)
    {
	struct arc_simple_condition *cond=NULL;
	struct arc_property *prop=parse_txt_property(prop_name,sys);
	if(prop)
	{
	    enum ARC_COMPARISON_OPERATOR op=parse_cond_value(tokens[*cur_token]);
	    if(op!=ARC_COND_INVALID)
	    {
		(*cur_token)++;
		
		struct arc_value const_value=parse_txt_value(tokens[*cur_token]);
		(*cur_token)++;
		cond=malloc(sizeof(struct arc_simple_condition));
		cond->prop=prop;
		cond->op=op;
		if(const_value.type == V_INT)
		{
		    cond->v_int=const_value.v_int;
		}
		else if(const_value.type == V_DOUBLE)
		{
		    cond->v_dbl=const_value.v_dbl;
		}
		else if(const_value.type == V_STRING)
		{
		    //TODO: CHECK IF THIS MAKESE SENSE!
		    cond->v_str= const_value.v_str;
		}
		
		return cond;
	    }

	    
	}
	else
	{
	    fprintf(stderr,"Could not find property %s\n",prop_name);
	    return NULL;
	}
	
    }
    else
    {
	fprintf(stderr,"Could not resolve %s into a property name\n",tokens[*cur_token]);
	return NULL;
    }
}
struct arc_complex_condition* parse_complex_cond(char **tokens, size_t num_tokens, size_t *cur_token, struct arc_system *sys)
{
    
    size_t count=0;
    
    for(size_t  i= *cur_token ; i < num_tokens;i++)
    {
	
	if(strcmp(tokens[i],"|")==0 || strcmp(tokens[i],"&")==0)
	{
		count++;
	}
	else if(strcmp(tokens[i],")")==0)
	{
	    break;
	}
    }
//    printf("Operator count is:%lu\n",count);
    size_t num_simple_conditions=count+1;
    struct arc_complex_condition* cond=malloc(sizeof(struct arc_complex_condition));
    struct arc_simple_condition *simple_conds=malloc(num_simple_conditions*sizeof(struct arc_simple_condition));
    enum ARC_BOOLEAN_OPERATOR *ops=malloc((num_simple_conditions-1)*sizeof(enum ARC_BOOLEAN_OPERATOR));

    
    if(consume(tokens[*cur_token],"(",cur_token))
    {
	struct arc_simple_condition *s_cond=parse_simple_cond(tokens,num_tokens,cur_token,sys);
	if(!s_cond)
	{
	    return NULL;
	}
	memcpy(&simple_conds[0],s_cond,sizeof(struct arc_simple_condition));
	free(s_cond);
	size_t idx=1;
	bool done=false;
	while(!done)
	{
//	    printf("Curr token: %s\n",tokens[*cur_token]);
	    if(strcmp(tokens[*cur_token],")")==0)
	    {
		done=true;
		(*cur_token)++;
		break;
	    }
	    else if(strcmp(tokens[*cur_token],"|")==0)
	    {
		ops[idx-1]=ARC_COND_OR;
		(*cur_token)++;
	    }
	    else if(strcmp(tokens[*cur_token],"&")==0)
	    {
		ops[idx-1]=ARC_COND_AND;
		(*cur_token)++;
	    }
	    else
	    {
		fprintf(stderr,"Could not make simple condition out of %s\n",tokens[*cur_token]);
		return NULL;
	    }
	    struct arc_simple_condition *s_cond=parse_simple_cond(tokens,num_tokens,cur_token,sys);
	    if(s_cond)
	    {
		memcpy(&simple_conds[idx],s_cond,sizeof(struct arc_simple_condition));
		free(s_cond);
	    }
	    else
	    {
		fprintf(stderr,"Could not make simple condition out of %s\n",tokens[*cur_token]);
		return NULL;
	    }
	    idx++;
	}
	 cond->simple_conds=simple_conds;
	 cond->ops=ops;
	 cond->num_simple_conditions=num_simple_conditions;
	 return cond;
    }
    //we failed so we need to clean up allocated memory
    free(ops);
    free(cond);
    free(simple_conds);
    return NULL;
   
}
