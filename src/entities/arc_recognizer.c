#define  _XOPEN_SOURCE 700
//#define _POSIX_C_SOURCE 200809L
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <ctype.h>

#include "arc_recognizer.h"
#include "tokenize.h"

void token_init(struct token *token, char *str, enum ARC_TOKEN_TYPE t_type)
{
    token->str=strdup(str);
    token->t_type=t_type;
}

void token_cleanup(struct token *token)
{
    free(token->str);
}

void token_stream_init(struct token_stream *t_s)
{
    t_s->tokens=malloc(TOKEN_STREAM_INIT_SIZE*sizeof(struct token));
    t_s->num_tokens=0;
    t_s->max_tokens=TOKEN_STREAM_INIT_SIZE;
}

void token_stream_add_token(struct token_stream *t_s, struct token *token)
{

    if(t_s->num_tokens >= t_s->max_tokens)
    {
		t_s->max_tokens*=2;
		//TODO this is bad if realloc fails.
		t_s->tokens=realloc(t_s->tokens,t_s->max_tokens*sizeof(struct token));
    }

    //copy token
    t_s->tokens[t_s->num_tokens]=*token;
    t_s->num_tokens++;    
}

void token_stream_destroy(struct token_stream *t_s)
{
    for(size_t i = 0; i< t_s->num_tokens;i++)
    {
		token_cleanup(&t_s->tokens[i]);
    }
    
    free(t_s->tokens);
}

void skip_whitespace(char *text, size_t *pos)
{
    while(isspace(text[*pos]))
    {
		(*pos)++;
    }
}

size_t scan_id(char *str)
{
    if(!isalpha(*str))
    {
		return 0;
    }
    
    char *p=str;
    while(isalnum(*p) || *p == '_')
    {
		if(!isalnum(*p) && (*p != '_') )
		{
			return 0;
		}
		p++;
    }
    
    return p-str;
}

size_t scan_string(char *str)
{
    if(str[0]!='"')
    {
		return 0;
    }
    else
    {
		char *p=str;
		p++;
		while(*p!='"')
		{
			if(*p=='\0')
			{
				//oops we ran out of string...
				return false;
			}
			p++;
		}
		p++;
		return p-str;
    }
}

size_t scan_num(char *str)
{
    
    if(!isdigit(*str))
    {
		return 0;
    }

    char *p = str;
    while (isdigit(*p))
    {
		p++;
    }
    if(*p == '.' && ( *(p+1) != '\0' && isdigit(*(p+1))))
    {
		p++;
		while(isdigit(*p))
		{
			p++;
		}
    }
    return p-str;
}


size_t scan_qualified_component(char *str)
{
    if(!isalpha(*str))
    {
		return 0;
    }
    char *p = str;
    while(isalnum(*p))
    {
		p++;
    }
    if(*p!='.' || *(p+1) == '\0' || !isalpha(*(p+1)))
    {
		return 0;
    }
    
    //skip '.'
    p++;
    while(isalnum(*p))
    {
		p++;
    }

    return p-str;
}


size_t scan_qualified_interface(char *str)
{
    if(!isalpha(*str))
    {
		return 0;
    }
    char *p = str;
    while(isalnum(*p))
    {
		p++;
    }
    if(*p!='.' || *(p+1) == '\0' || !isalpha(*(p+1)))
    {
		return 0;
    }
    
    //skip '.'
    p++;
    while(isalnum(*p))
    {
		p++;
    }
	if(*p!='.' || *(p+1) == '\0' || !isalpha(*(p+1)))
    {
		return 0;
    }
    
    //skip '.'
    p++;
    while(isalnum(*p))
    {
		p++;
    }
    return p-str;
}





struct token recognize(char *text, size_t *pos)
{
    size_t len = 0;
    char *o_text = &text[*pos];
    //printf("::%c\n",text[*pos]);
    //implement scratch_buffer_strdup and use a scratch_buffer instead.
    struct token tok;
    if(text[*pos] == '[')
    {
		tok.t_type = TOKEN_L_BRACKET;
		len = 1;
    }
    else if(text[*pos] == ']')
    {	
		tok.t_type =  TOKEN_R_BRACKET;
		len=1;
    }
    else if(text[*pos] == '{')
    {
		tok.t_type =  TOKEN_L_CBRACE;
		len=1;
    }
    else if(text[*pos] == '}')
    {
		tok.t_type =  TOKEN_R_CBRACE;
		len=1;
    }
    else if(text[*pos] == '=')
    {
		tok.t_type =  TOKEN_EQUALS;
		len=1;
    }
    else if(strcmp(&text[*pos], "NIL") == 0)
    {
		tok.t_type =  TOKEN_NIL;
		len=3;
    }
    else if(strcmp(&text[*pos], "->") == 0)
    {
		tok.t_type =  TOKEN_CALLS;
		len=2;
    }
    else if(text[*pos] == ':')
    {
		tok.t_type =  TOKEN_COLON;
		len=1;
    }
    else if(text[*pos] == '+')
    {
		tok.t_type =  TOKEN_PLUS;
		len=1;
    }
    else if(text[*pos] == '-')
    {
		tok.t_type =  TOKEN_MINUS;
		len=1;
    }
    else if((len = scan_num(&text[*pos])))
    {
		tok.t_type =  TOKEN_NUMBER;
    }
    else if((len = scan_id(&text[*pos])))
    {
		tok.t_type =  TOKEN_ID;
    }
    else if((len = scan_qualified_component(&text[*pos])))
    {
		tok.t_type =  TOKEN_QCOMP;
    }
    else if(scan_qualified_interface(&text[*pos]))
    {
		tok.t_type =  TOKEN_QIFACE;
    }
    else if((len = scan_string(&text[*pos])))
    {
		/*
		  Ugly thing here:
		  this is essentially fall down. (e.g., in the case of paths)
		*/
		tok.t_type =  TOKEN_STRING;
		
    }
	else if (text[*pos] == '\0')
	{
		tok.t_type = TOKEN_EOF;
		tok.str = NULL;
	}
    else
    {
		tok.t_type =  TOKEN_UNKNOWN;
		tok.str = NULL;
    }
    if(len > 0)
    {
		//Not calling token_init to not allocate memory twice
		tok.str = strndup(o_text,len);
		(*pos)+=len;
		
    }
    return tok;
}

bool build_token_stream(char *text, struct token_stream *t_s)
{	
    size_t pos = 0;
	
    while(text[pos] != '\0')
    {
		skip_whitespace(text, &pos);

		struct token token = recognize(text,&pos);
		if(token.t_type == TOKEN_UNKNOWN)
		{
			fprintf(stderr,"Could not recognize token: [%s] at %d\n",token,pos);
			return false;
		}
		token_stream_add_token(t_s,&token);
    }
	
    return t_s;
}
