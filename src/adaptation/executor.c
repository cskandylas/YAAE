#include "executor.h"
#include "knowledge_base.h"
#include <stdlib.h>
#include <stdio.h>


void executor_init(struct executor *exec, struct mape_k *mape_k)
{

    exec->mape_k = mape_k;
    
    exec->cmds=malloc(sizeof(struct execution_command*)*NUM_EXECUTION_CMDS_INIT);
    exec->max_cmds=NUM_EXECUTION_CMDS_INIT;
    exec->num_cmds=0;
    
}
void executor_destroy(struct executor *exec)
{
    for(int i=0;i<exec->num_cmds;i++)
    {
	exec_cmd_destroy(exec->cmds[i]);
    }
    free(exec->cmds);
}

void executor_add_command(struct executor *exec, struct exec_cmd *cmd)
{
    
    if(exec->num_cmds >= exec->max_cmds)
    {
	exec->max_cmds *= 2;
	exec->cmds = realloc(exec->cmds,
			     exec->max_cmds*sizeof(struct execution_command*));
    }
    exec->cmds[exec->num_cmds]=cmd;
    cmd_set_executor(cmd, exec);
    exec->num_cmds++;

}

void executor_rem_command(struct executor *exec, struct exec_cmd *cmd)
{
    for(size_t i = 0; i < exec->num_cmds;i++)
    {
	if(exec->cmds[i] == cmd)
	{
	    if( i != exec->num_cmds -1)
	    {
		exec->cmds[i] = exec->cmds[exec->num_cmds-1];
	    }
	    exec->num_cmds--;
	}
    }
}


void executor_rem_command_idx(struct executor *exec, size_t idx)
{
    if(idx >= exec->num_cmds)
    {
	return;
    }

    if(idx != exec->num_cmds-1)
    {
	exec->cmds[idx] = exec->cmds[exec->num_cmds-1];
    }

    exec->num_cmds--;

}
void executor_run(struct executor *exec)
{

    /*Make each of these a task? */
    for(size_t i=0;i<exec->num_cmds;i++)
    {
	struct exec_cmd *cmd = exec->cmds[i];
	exec_cmd_run(cmd);
    }

}

bool executor_all_cmds_done(struct executor *exec)
{
    for(size_t i = 0; i < exec->num_cmds;i++)
    {
	struct exec_cmd *cmd = exec->cmds[i];
	if(cmd_get_status(cmd)!=CMD_DONE)
	{
	    return false;
	}	
    }

    return true;
}
