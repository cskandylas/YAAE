#define  _XOPEN_SOURCE 500

#include "arc_strategy.h"

#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "tokenize.h"
#include "parse_common.h"
#include "arc_parse.h"
#include "knowledge_base.h"

static int parse_strategy(char **tokens, size_t num_tokens, size_t *cur_token,
			  struct arc_strategy *strategy,struct arc_system *sys,
			  struct knowledge_base *kb);
static int parse_condplans(char **tokens, size_t num_tokens, size_t *cur_token, struct arc_strategy *strategy);
static struct arc_condplan*  parse_condplan(char **tokens, size_t num_tokens, size_t *cur_token, struct arc_strategy *strategy);

void init_strategy(struct arc_strategy *strat, char *name, struct arc_system *sys,struct knowledge_base *kb)
{
    strat->name=malloc(strlen(name)+1);
    strcpy(strat->name,name);

    strat->sys=sys;
    strat->kb=kb;
    
    strat->max_cond_plans=ARC_CONDPLAN_INITIAL_CAP;
    strat->num_cond_plans=0;
    strat->cond_plans=malloc(strat->max_cond_plans*sizeof(struct arc_condplan*));
}

void strat_add_plan(struct arc_strategy *strategy , struct arc_condplan *condplan)
{
    if(strategy->num_cond_plans>=strategy->max_cond_plans)
    {
        strategy->max_cond_plans*=2;
	strategy->cond_plans=realloc(strategy->cond_plans,
				 strategy->max_cond_plans*sizeof(struct arc_condplan*));
    }
    strategy->cond_plans[strategy->num_cond_plans]=condplan;
    strategy->num_cond_plans++;
    
}


struct arc_strategy* strategy_parse(char *txt, struct arc_system *sys,struct knowledge_base *kb)
{
    struct arc_strategy *strat = malloc(sizeof(struct arc_strategy));
    struct tokenizer whitespace_tok;
    tokenizer_init(&whitespace_tok,txt," \n\t\r\0");
    int num_tokens=count_tokens(&whitespace_tok);
    char **tokens=malloc(num_tokens*sizeof(char*));
    for(int i=0;i<num_tokens;i++)
    {
	tokens[i]=strdup(next_token(&whitespace_tok));
    }
    
    size_t cur_token=0;
    if( parse_strategy(tokens,num_tokens,&cur_token,strat,sys,kb)==0)
    {

	free(tokens);
	tokenizer_cleanup(&whitespace_tok);
	return strat;
    }
    
    return NULL;
}

static int parse_strategy(char **tokens, size_t num_tokens, size_t *cur_token,
			  struct arc_strategy *strategy,struct arc_system *sys,
			  struct knowledge_base *kb)
{
      if(consume(tokens[*cur_token],"Strategy",cur_token))
      {
	  char *strat_name=parse_name(tokens,num_tokens,cur_token);
	  
	  if(!strat_name)
	  {
	      return -1;
	  }
	  
	  init_strategy(strategy,strat_name,sys,kb);
	  
	  struct arc_complex_condition *cond=parse_complex_cond(tokens,num_tokens,cur_token,strategy->sys);
	  strategy->strat_cond=cond;
	  if(consume(tokens[*cur_token],"{",cur_token))
	  {
	      parse_condplans(tokens,num_tokens,cur_token,strategy);
	      if(consume(tokens[*cur_token],"}",cur_token))
	      {
		  //printf("Strategy %s parse successful\n",strat_name);
		free(strat_name);
		return 0;
	      }
	  }
      }
    
      return -1;    
}


static int parse_condplans(char **tokens, size_t num_tokens, size_t *cur_token, struct arc_strategy *strategy)
{
    struct arc_condplan *condplan=parse_condplan(tokens,num_tokens,cur_token,strategy);
    while(condplan!=NULL)
    {
	strat_add_plan(strategy,condplan);
	condplan=parse_condplan(tokens,num_tokens,cur_token,strategy);
	
    }

    return 0;   
}

static struct arc_condplan* parse_condplan(char **tokens, size_t num_tokens, size_t *cur_token, struct arc_strategy *strategy)
{
    //early return in case we've reached the end 
    if(strcmp(tokens[*cur_token],"}")==0)
	return NULL;
    
    struct arc_complex_condition *cond=parse_complex_cond(tokens,num_tokens,cur_token,strategy->sys);
    if(cond)
    {
//	printf("Cond %p %s %s %s\n",cond,tokens[*cur_token],tokens[(*cur_token)+1],tokens[(*cur_token)+2]);
	if(consume(tokens[*cur_token],"{",cur_token))
	{

	    char *plan_name=parse_name(tokens,num_tokens,cur_token);
	    //printf("Parsing plan: %s\n",plan_name);
	    struct arc_plan *plan=knowledge_base_get_plan(strategy->kb,plan_name);
	    if(plan)
	    {
	    
		if(consume(tokens[*cur_token],"}",cur_token))
		{
		    struct arc_condplan *condplan=malloc(sizeof(struct arc_condplan));
		    condplan->condition=cond;
		    condplan->plan=plan;
		    free(plan_name);
		    return condplan;
		}
	    }
	    else
	    {
		fprintf(stderr,"Plan %s not found \n",plan_name);
		free(plan_name);
	    }
	    
	}
	destroy_complex_condition(cond);
	free(cond);
    }
    return NULL;
}

int strategy_execute(struct arc_strategy *strat)
{
    //printf("Executing strategy:%s\n",strat->name);
    for(int i=0;i<strat->num_cond_plans;i++)
    {
	if(arc_complex_cond_eval(strat->cond_plans[i]->condition,strat->sys))
	{
	    plan_execute(strat->cond_plans[i]->plan,strat->sys);
	}
    }
}

void destroy_strategy(struct arc_strategy *strat)
{
    free(strat->name);
    for(int i=0;i<strat->num_cond_plans;i++)
    {
	/*
	if(strat->cond_plans[i])
	  destroy_complex_condition(strat->cond_plans[i]->condition);
	*/
	
    }
    free(strat->cond_plans);
}
