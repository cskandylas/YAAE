#include "monitor.h"
#include "task.h"
#include "knowledge_base.h"

#include <stdlib.h>

#include <stdio.h>

void init_monitor(struct monitor *mon, struct mape_k *mape_k,
		  goal_eval_fn goal_eval)
{
    mon->probes=malloc(sizeof(struct probe*)*NUM_PROBES_INIT);
    mon->max_probes=NUM_PROBES_INIT;
    mon->num_probes=0;
    mon->mape_k = mape_k;
    mon->goal_eval = goal_eval;
}

void monitor_add_probe(struct monitor *mon, struct probe *probe)
{
    if(mon->num_probes>=mon->max_probes)
    {
        mon->max_probes*=2;
	mon->probes=realloc(mon->probes,
				mon->max_probes*sizeof(struct probe*));
    }
    mon->probes[mon->num_probes]=probe;
    probe_set_monitor(probe, mon);
    mon->num_probes++;    
}


void monitor_rem_probe(struct monitor *mon, struct probe *probe)
{
    for(size_t i = 0; i < mon->num_probes;i++)
    {
	if(mon->probes[i] == probe)
	{
	    if( i != mon->num_probes -1)
	    {
		mon->probes[i] = mon->probes[mon->num_probes-1];
	    }
	    mon->num_probes--;
	}
    }
}

void monitor_run(struct monitor *mon)
{
    
    for(int i=0;i<mon->num_probes;i++)
    {
	struct probe *probe = mon->probes[i];
	start_probe(probe);
    }

}

void monitor_stop_probe(struct monitor *mon, struct probe *probe)
{
    for(int i=0;i<mon->num_probes;i++)
    {
	if(mon->probes[i]==probe)
	{
	    probe_set_status(probe, PROBE_STOPPED);
	}
    }
}
void destroy_monitor(struct monitor *mon)
{
    for(int i=0;i<mon->num_probes;i++)
    {

	destroy_probe(mon->probes[i]);
    }
    free(mon->probes);
}


bool monitor_all_probes_done(struct monitor *mon)
{

    for(int i=0;i<mon->num_probes;i++)
    {
	struct probe *probe = mon->probes[i];
	enum PROBE_STATUS status = probe_get_status(probe);
	if(status != PROBE_DONE)
	{
	    return false;
	}	       
    }

    return true;
}


bool monitor_has_probes_waiting(struct monitor *mon)
{
    for(int i=0;i<mon->num_probes;i++)
    {
	struct probe *probe = mon->probes[i];
	enum PROBE_STATUS status = probe_get_status(probe);
	if(status == PROBE_CREATED)
	{
	    return true;;
	}	       
    }

    return false;
}    

