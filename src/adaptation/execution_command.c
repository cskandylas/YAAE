#include "execution_command.h"

void exec_cmd_init(struct exec_cmd *cmd, cmd_run run, cmd_done done, void *ctx)
{
    
    cmd->run = run;
    cmd->done = done;
    cmd->ctx = ctx;
    pthread_mutex_init(&cmd->lock, NULL);
    cmd_set_status(cmd, CMD_CREAT);
}

void exec_cmd_destroy(struct exec_cmd *cmd)
{
    //nothing for now ;)
    pthread_mutex_destroy(&cmd->lock);
}

void exec_cmd_run(struct exec_cmd *cmd)
{
    cmd_set_status(cmd, CMD_PROC);
    cmd->run(cmd);
}


//safe accessors
void *cmd_get_ctx(struct exec_cmd *cmd)
{
    void *ret = NULL;
    pthread_mutex_lock(&cmd->lock);
    ret = cmd->ctx;
    pthread_mutex_unlock(&cmd->lock);
    return ret;
}


void cmd_set_status(struct exec_cmd *cmd, enum EXEC_CMD_STATUS status)
{
    pthread_mutex_lock(&cmd->lock);
    cmd->status = status;
    pthread_mutex_unlock(&cmd->lock);
}

enum EXEC_CMD_STATUS cmd_get_status(struct exec_cmd *cmd)
{
    enum EXEC_CMD_STATUS ret = CMD_INVALID;
    pthread_mutex_lock(&cmd->lock);
    ret = cmd->status;
    pthread_mutex_unlock(&cmd->lock);
    return ret;
}

struct executor *cmd_get_executor(struct exec_cmd *cmd)
{
    struct executor *ret = NULL;
    pthread_mutex_lock(&cmd->lock);
    ret = cmd->exec;
    pthread_mutex_unlock(&cmd->lock);
    return ret;

}

void cmd_set_executor(struct exec_cmd *cmd, struct executor *exec)
{
    pthread_mutex_lock(&cmd->lock);
    cmd->exec = exec;
    pthread_mutex_unlock(&cmd->lock);
}
