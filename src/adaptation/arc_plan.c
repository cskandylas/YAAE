#define  _XOPEN_SOURCE 500
#include "arc_plan.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "tokenize.h"


static char *ARC_OP_NAMES[] =
{
    "ADD_COMP",
    "REM_COMP",
    "ADD_INVO",
    "REM_INVO",
    "ADD_IFACE",
    "REM_IFACE",
    "ADD_SYS_PROP",
    "REM_SYS_PROP",
    "ADD_COMP_PROP",
    "REM_COMP_PROP",
    "ADD_IFACE_PROP",
    "REM_IFACE_PROP",
};


static char *plan_errors[]=
{
    "Operation Success\n",                  //0
    "Wrong number of arguments\n",          //1
    "Component already exists\n"            //2
    "Component not found\n",                //3 
    "Invocation already exists\n",          //4
    "Invocation not found\n",               //5
    "Interface already exists\n",           //6
    "Interface not found\n",                //7
    "Property already exists\n",            //8
    "Property not found\n",                 //9
    "Invalid property type\n"               //10
    
};

void plan_init(struct arc_plan *plan, char *plan_name)
{
    plan->plan_name=malloc(strlen(plan_name)+1);
    strcpy(plan->plan_name,plan_name);
}
void plan_destroy(struct arc_plan *plan)
{
    if(plan->num_actions>0)
	free(plan->actions);
    free(plan->plan_name);
}

void plan_action_cleanup(struct arc_plan_action *plan_action)
{
    for(int i=0;i<plan_action->num_args;i++)
    {
	free(plan_action->args[i]);
    }
    free(plan_action->args);
}

void plan_parse_old_dsl(struct arc_plan *plan, char *txt)
{

    struct tokenizer tok;
    tokenizer_init(&tok,txt,"\n");
    char *line=strdup(next_token(&tok));
    
    struct tokenizer name_tok;
    tokenizer_init(&name_tok, line, " ");
    char *ignore= next_token(&name_tok);
    char *plan_name = strdup(next_token(&name_tok));
    plan_init(plan,plan_name);
    

    int num_lines=count_tokens(&tok)-1;
    
    plan->actions=malloc(sizeof(struct arc_plan_action*)*num_lines);
    plan->num_actions=num_lines;
    //printf("Expecting %d lines\n",num_lines);
    int idx=0;
    while(line!=NULL)
    {
	//printf("%s\n",line);
	
	//split line into left hand side and right hand side i.e. on '(';
	struct tokenizer op_tok;
	tokenizer_init(&op_tok,line,"()");
	char *operation=next_token(&op_tok);
	char *args=next_token(&op_tok);
	
	if(strcmp(operation,"add_component")==0)
	{
	   plan->actions[idx] = plan_parse_comp_add(args);   
	}
	else if(strcmp(operation,"add_interface")==0)
	{
	    plan->actions[idx]=plan_parse_iface_add(args);
	}
	else if(strcmp(operation,"rem_interface")==0)
	{
	    plan->actions[idx]=plan_parse_iface_rem(args);
	}
	else if(strcmp(operation,"rem_component")==0)
	{
	    plan->actions[idx] = plan_parse_comp_rem(args);   
	}
	else if(strcmp(operation,"add_invocation")==0)
	{
	    plan->actions[idx] = plan_parse_invo_add(args);   
	}
	else if(strcmp(operation,"rem_invocation")==0)
	{
	    plan->actions[idx] =  plan_parse_invo_rem(args);   
	}
	else if(strcmp(operation,"add_property")==0)
	{
	    plan->actions[idx] = plan_parse_prop_add(args);   
	}
	else if(strcmp(operation,"rem_property")==0)
	{
	    plan->actions[idx] = plan_parse_prop_rem(args);   
	}
	else
	{
	    
	    plan->actions[idx]= NULL;
	}
	tokenizer_cleanup(&op_tok);
	
	line=next_token(&tok);
	if(line)
	{
	    line = strdup(line);
	}
	idx++;
    }
    
    //printf("plan idx stopped at:%d\n",idx);
    //ugly hack to deal with null lines, need a better tokenizer or a proper parser.
    for(int i=idx;i<num_lines;i++)
    {
	plan->actions[idx] = NULL;
    }
    
}
int  plan_execute(struct arc_plan *plan, struct arc_system *sys)
{
    //printf("Executing plan: %s\n", plan->plan_name);
    int rc=-1;
    for(int i=0;i<plan->num_actions;i++)
    {
	if(plan->actions[i]!=NULL)
	{
	    //printf("Executing %d %s\n",i,ARC_OP_NAMES[plan->actions[i]->op]);
	    switch(plan->actions[i]->op)
	    {
	    case ADD_COMP:
		rc = plan_comp_add( plan->actions[i] , sys );
		break;
	    case REM_COMP:
		rc=  plan_comp_rem(plan->actions[i],sys);
		break;
	    case ADD_INVO:
		rc=  plan_invo_add(plan->actions[i],sys);
		break;
	    case REM_INVO:
		rc=  plan_invo_rem(plan->actions[i],sys);
		break;
	    case ADD_SYS_PROP:
		rc= plan_sys_prop_add(plan->actions[i],sys);
		break;
	    case REM_SYS_PROP:
		rc=  plan_sys_prop_rem(plan->actions[i],sys);
		break;
	    case ADD_COMP_PROP:
		rc= plan_comp_prop_add(plan->actions[i],sys);
		break;
	    case REM_COMP_PROP:
		rc= plan_comp_prop_rem(plan->actions[i],sys);
		break;
	    case ADD_IFACE:
		rc=plan_iface_add(plan->actions[i],sys);
		break;
	    case REM_IFACE:
		rc=plan_iface_rem(plan->actions[i],sys);
		break;
	    case ADD_IFACE_PROP:
		rc=plan_iface_prop_add(plan->actions[i],sys);
		break;
	    case REM_IFACE_PROP:
		rc=plan_iface_prop_rem(plan->actions[i],sys);
		break;
	    }
	    if(rc!=0){
		//printf("Error code: %d\n",rc);
		//printf("%s\n",plan_errors[rc]);
		return rc;
	    }
	}
	else
	{
	    fprintf(stderr,"NULL in action table, skipping\n");
	}
    }
    return rc;
}


int plan_comp_add(struct arc_plan_action *action, struct arc_system *sys)
{
    if(action->num_args!=1)
    {
	//WRONG NUMBER OF ARGUMENTS FOUND
	return 1;
    }
    else
    {
	//arc_model_add_component(&model,comp_name)
	char *comp_name=action->args[0];
	if(sys_find_component(sys,comp_name))
	{
	    //COMPONENT ALREADY EXISTS
	    printf("%s\n",comp_name);
	    return PLAN_COMP_EXISTS;
	}
	else
	{
	    struct arc_component *comp=malloc(sizeof(struct arc_component));
	    init_component(comp,comp_name);
	    sys_add_component(sys,comp);
	    return PLAN_OK;
	}
    }
}

int plan_comp_rem(struct arc_plan_action *action, struct arc_system *sys)
{
    if(action->num_args!=1)
    {
	//WRONG NUMBER OF ARGUMENTS FOUND
	return 1;
    }
    else
    {
	char *comp_name=action->args[0];
	if(sys_find_component(sys,comp_name)==NULL)
	{
	    //COMPONENT COMPONENT NOT FOUND
	    printf("%s\n",comp_name);
	    return PLAN_COMP_NOT_FOUND;
	}
	else
	{
	    free(sys_rem_component(sys,comp_name));
	    return PLAN_OK;
	}
    }
}

int plan_iface_add(struct arc_plan_action *action, struct arc_system *sys)
{
     if(action->num_args!=2)
    {
	//WRONG NUMBER OF ARGUMENTS FOUND
	return 1;
    }
    else
    {
	char *comp_name=action->args[0];
	struct arc_component *comp=sys_find_component(sys,comp_name);
	if(!comp)
	{
	    //COMPONENT NOT FOUND
	    printf("%s\n",comp_name);
	    return PLAN_COMP_NOT_FOUND;
	}
	else
	{
	    
	    char *iface_name=action->args[1];

	    struct arc_interface *iface= malloc(sizeof(struct arc_interface));
	    init_interface(iface,iface_name);
	    comp_add_iface(comp,iface);
	    return PLAN_OK;
	}
    }
}

int plan_iface_rem(struct arc_plan_action *action, struct arc_system *sys)
{
         if(action->num_args!=2)
    {
	//WRONG NUMBER OF ARGUMENTS FOUND
	return PLAN_WRONG_ARGS;
    }
    else
    {
	char *comp_name=action->args[0];
	struct arc_component *comp=sys_find_component(sys,comp_name);
	if(!comp)
	{
	    //COMPONENT NOT FOUND
	    printf("%s\n",comp_name);
	    return PLAN_COMP_NOT_FOUND;
	}
	else
	{
	    char *iface_name=action->args[1];
	    for(int i=0;i<sys->num_invocations;i++)
	    {
		if(sys->invocations[i])
		{
		    if(strcmp(sys->invocations[i]->iface->if_name,iface_name)==0)
		    {
			struct arc_invocation *invo=sys_rem_invocation(sys,sys->invocations[i]->from->comp_name,
								       sys->invocations[i]->to->comp_name,
								       sys->invocations[i]->iface->if_name);
			free(invo);
		    }
		}
	    }

	    
	    
	    
	    struct arc_interface *rem_iface=comp_rem_iface(comp,iface_name);
	    for(int i=0;i<rem_iface->num_properties;i++)
	    {
		destroy_property(rem_iface->properties[i]);
		free(rem_iface->properties[i]);
	    }
	    destroy_interface(rem_iface);
	    free(rem_iface);

	    
	    
	    return PLAN_OK;
	}
    }
}

int plan_invo_add(struct arc_plan_action *action, struct arc_system *sys)
{
    if(action->num_args!=3)
    {
	return PLAN_WRONG_ARGS;
    }
    else
    {
	char *from=action->args[0];
	char *to=action->args[1];
	char *if_name=action->args[2];
	if(sys_find_invocation(sys,from,to,if_name))
	{
	    //INVOCATION ALREADY EXISTS
	    return PLAN_INVO_EXISTS;
	}
	else
	{
	    struct arc_component *from_comp=sys_find_component(sys,from);
	    struct arc_component *to_comp=sys_find_component(sys,to);
	    if(!from || !to)
	    {
		printf("%s\n",from);
		return PLAN_COMP_NOT_FOUND;
	    }
	    struct arc_interface *iface=comp_find_iface(to_comp,if_name);
	    if(!iface)
	    {
		return PLAN_IFACE_NOT_FOUND;
	    }
	    struct arc_invocation *invo=malloc(sizeof(struct arc_invocation));
	    init_invocation(invo,iface,from_comp,to_comp);
	    sys_add_invocation(sys,invo);
	    return PLAN_OK;
	}
    }
}

int plan_invo_rem(struct arc_plan_action *action, struct arc_system *sys)
{
    if(action->num_args!=3)
    {
	return PLAN_WRONG_ARGS;
    }
    else
    {
	char *from=action->args[0];
	char *to=action->args[1];
	char *if_name=action->args[2];
	if(sys_find_invocation(sys,from,to,if_name)==NULL)
	{
	    //INVOCATION ALREADY EXISTS
	    return PLAN_INVO_EXISTS;
	}
	else
	{
	    free(sys_rem_invocation(sys,from,to,if_name));
	    return PLAN_OK;
	}
    }
}

int plan_sys_prop_add(struct arc_plan_action *action, struct arc_system *sys)
{
    //add_property(N_COMPONENTS,FLOAT,13.0)
    if(action->num_args!=3)
    {
	return PLAN_WRONG_ARGS;
    }
    else
    {
	char *prop_name=action->args[0];
	char *prop_type=action->args[1];
	char *value=action->args[2];
	if(sys_find_property(sys,prop_name))
	{
	    //PROPERTY ALREADY EXISTS
	    free(sys_rem_property(sys,prop_name));
	}
	
	    struct arc_property *prop=malloc(sizeof(struct arc_property));
	    if(strcmp(prop_type,"INT")==0)
	    {
		init_int_property(prop,prop_name,atoi(value));
	    }
	    else if(strcmp(prop_type,"DOUBLE")==0)
	    {
		init_dbl_property(prop,prop_name,atof(value));
	    }
	    else if(strcmp(prop_type,"STRING")==0)
	    {
		init_str_property(prop,prop_name,value);
	    }
	    else
	    {
		return PLAN_PROP_INVALID;
	    }
	    sys_add_property(sys,prop);
	    
	    
	    return PLAN_OK;
	
    }
}

int plan_sys_prop_rem(struct arc_plan_action *action, struct arc_system *sys)
{
    if(action->num_args!=1)
    {
	//WRONG NUMBER OF ARGUMENTS FOUND
	return 1;
    }
    else
    {
	
	char *prop_name=action->args[0];
	if(sys_find_property(sys,prop_name)==NULL)
	{
	    
	    return PLAN_PROP_NOT_FOUND;
	}
	else
	{
	    free(sys_rem_property(sys,prop_name));
	    return PLAN_OK;
	}
    }
}


int plan_comp_prop_add(struct arc_plan_action *action, struct arc_system *sys)
{
        //add_property(N_COMPONENTS,FLOAT,13.0)
    if(action->num_args!=4)
    {
	return PLAN_WRONG_ARGS;
    }
    else
    {
	char *comp_name=action->args[0];
	char *prop_name=action->args[1];
	char *prop_type=action->args[2];
	char *value=action->args[3];
	struct arc_component *comp = sys_find_component(sys,comp_name);
	if(comp)
	{
	    if(comp_find_property(comp,prop_name)!=NULL)
	    {
		free(comp_rem_property(comp,prop_name));
	    }
	    
	    struct arc_property *prop=malloc(sizeof(struct arc_property));
	    if(strcmp(prop_type,"INT")==0)
	    {
		init_int_property(prop,prop_name,atoi(value));
	    }
	    else if(strcmp(prop_type,"DOUBLE")==0)
	    {
		init_dbl_property(prop,prop_name,atof(value));
	    }
	    else if(strcmp(prop_type,"STRING")==0)
	    {
		init_str_property(prop,prop_name,value);
	    }
	    else
	    {
		return PLAN_PROP_INVALID;
	    }
	    comp_add_property(comp,prop);
	    
	    return PLAN_OK;
	}
	else
	{
	    printf("%s\n",comp_name);
	    return PLAN_COMP_NOT_FOUND;
	}
    }
}
int plan_comp_prop_rem(struct arc_plan_action *action, struct arc_system *sys)
{   
    //remove_property(comp_name,prop_name)
    if(action->num_args!=2)
    {
	return -1;
    }
    else
    {
	char *comp_name=action->args[0];
	char *prop_name=action->args[1];

	struct arc_component *comp = sys_find_component(sys,comp_name);
	if(comp)
	{
	    if(comp_find_property(comp,prop_name)==NULL)
	    {
		return PLAN_PROP_NOT_FOUND;
	    }

	    free(comp_rem_property(comp,prop_name));
	    
	    return PLAN_OK;
	}
	else
	{
	    printf("%s\n",comp_name);
	    return PLAN_COMP_NOT_FOUND;
	}
    }
}

int plan_iface_prop_add(struct arc_plan_action *action, struct arc_system *sys)
{
    if(action->num_args!=5)
    {
	return PLAN_WRONG_ARGS;
    }
    else
    {
	char *comp_name=action->args[0];
	char *iface_name=action->args[1];
	char *prop_name=action->args[2];
	char *prop_type=action->args[3];
	char *value=action->args[4];
	struct arc_component *comp = sys_find_component(sys,comp_name);
	if(comp)
	{
	    struct arc_interface *iface=comp_find_iface(comp,iface_name);

	    if(iface)
	    {
		if(!iface_find_property(iface,prop_name))
		{
		    struct arc_property *prop=malloc(sizeof(struct arc_property));
		    if(strcmp(prop_type,"INT")==0)
		    {
			init_int_property(prop,prop_name,atoi(value));
		    }
		    else if(strcmp(prop_type,"DOUBLE")==0)
		    {
			init_dbl_property(prop,prop_name,atof(value));
		    }
		    else if(strcmp(prop_type,"STRING")==0)
		    {
			init_str_property(prop,prop_name,value);
		    }
		    else
		    {
			
			return PLAN_PROP_INVALID;
		    }
		    iface_add_property(iface,prop);
		}
	    }
	    else
	    {
		return PLAN_IFACE_NOT_FOUND;
	    }
	    return PLAN_OK;
	}
	else
	{
	    return PLAN_COMP_NOT_FOUND;
	}
    }
}
int plan_iface_prop_rem(struct arc_plan_action *action, struct arc_system *sys)
{
    
    
    if(action->num_args!=3)
    {
	return PLAN_WRONG_ARGS;
    }
    else
    {
	char *comp_name=action->args[0];
	char *iface_name=action->args[1];
	char *prop_name=action->args[2];
	
	struct arc_component *comp = sys_find_component(sys,comp_name);
	if(comp)
	{
	    struct arc_interface *iface=comp_find_iface(comp,iface_name);

	    if(iface)
	    {
		if(iface_find_property(iface,prop_name))
		{
		    free(iface_rem_property(iface,prop_name));
		}
	    }
	    else
	    {
		return PLAN_IFACE_NOT_FOUND;
	    }
	    return PLAN_OK;
	}
	else
	{
	    return PLAN_COMP_NOT_FOUND;
	}
    }
}


struct arc_plan_action* plan_parse_comp_add(char *args)
{			    
    struct arc_plan_action *plan_action=malloc(sizeof(struct arc_plan_action));
    plan_action->op=ADD_COMP;
    plan_action->num_args=1;
    plan_action->args=malloc(sizeof(char*));
    plan_action->args[0]=malloc(strlen(args)+1);
    strcpy(plan_action->args[0],args);

    return plan_action;
}			    
struct arc_plan_action* plan_parse_comp_rem(char *args)
{			    
    struct arc_plan_action *plan_action=malloc(sizeof(struct arc_plan_action));
    plan_action->op=REM_COMP;
    plan_action->num_args=1;
    plan_action->args=malloc(sizeof(char*));
    plan_action->args[0]=malloc(strlen(args)+1);
    strcpy(plan_action->args[0],args);

    return plan_action;
}


struct arc_plan_action* plan_parse_iface_add(char *args)
{

    struct tokenizer comma_tok;
    tokenizer_init(&comma_tok,args,",");
    char *comp=next_token(&comma_tok);
    char *iface=next_token(&comma_tok);
    
    struct arc_plan_action *plan_action=malloc(sizeof(struct arc_plan_action));
    plan_action->op=ADD_IFACE;
    plan_action->num_args=2;
    plan_action->args=malloc(sizeof(char*)*2);

    plan_action->args[0]=malloc(strlen(comp)+1);
    strcpy(plan_action->args[0],comp);
    plan_action->args[1]=malloc(strlen(iface)+1);
    strcpy(plan_action->args[1],iface);
    
    tokenizer_cleanup(&comma_tok);

    return plan_action;
}

struct arc_plan_action* plan_parse_iface_rem(char *args)
{
    struct tokenizer comma_tok;
    tokenizer_init(&comma_tok,args,",");
    char *comp=next_token(&comma_tok);
    char *iface=next_token(&comma_tok);
    
    struct arc_plan_action *plan_action=malloc(sizeof(struct arc_plan_action));
    plan_action->op=REM_IFACE;
    plan_action->num_args=2;
    plan_action->args=malloc(sizeof(char*)*2);

    plan_action->args[0]=malloc(strlen(comp)+1);
    strcpy(plan_action->args[0],comp);
    plan_action->args[1]=malloc(strlen(iface)+1);
    strcpy(plan_action->args[1],iface);
    
    tokenizer_cleanup(&comma_tok);

    return plan_action;
}



struct arc_plan_action* plan_parse_invo_add(char *args)
{
    struct tokenizer comma_tok;
    tokenizer_init(&comma_tok,args,",");
    char *from=next_token(&comma_tok);
    char *to=next_token(&comma_tok);
    char *iface=next_token(&comma_tok);
    
    struct arc_plan_action *plan_action=malloc(sizeof(struct arc_plan_action));
    plan_action->op=ADD_INVO;
    plan_action->num_args=3;
    plan_action->args=malloc(sizeof(char*)*3);


    plan_action->args[0]=malloc(strlen(from)+1);
    strcpy(plan_action->args[0],from);
    plan_action->args[1]=malloc(strlen(to)+1);
    strcpy(plan_action->args[1],to);
    plan_action->args[2]=malloc(strlen(iface)+1);
    strcpy(plan_action->args[2],iface);

    tokenizer_cleanup(&comma_tok);
    
    return plan_action;	    
}			    
struct arc_plan_action* plan_parse_invo_rem(char *args)
{			    
    struct tokenizer comma_tok;
    tokenizer_init(&comma_tok,args,",");
    char *from=next_token(&comma_tok);
    char *to=next_token(&comma_tok);
    char *iface=next_token(&comma_tok);
    
    struct arc_plan_action *plan_action=malloc(sizeof(struct arc_plan_action));
    plan_action->op=REM_INVO;
    plan_action->num_args=3;
    plan_action->args=malloc(sizeof(char*)*3);


    plan_action->args[0]=malloc(strlen(from)+1);
    strcpy(plan_action->args[0],from);
    plan_action->args[1]=malloc(strlen(to)+1);
    strcpy(plan_action->args[1],to);
    plan_action->args[2]=malloc(strlen(iface)+1);
    strcpy(plan_action->args[2],iface);

    tokenizer_cleanup(&comma_tok);
    
    return plan_action;	    
}			    
struct arc_plan_action* plan_parse_prop_add(char *args)
{
    struct tokenizer comma_tok;
    tokenizer_init(&comma_tok,args,",");
    int num_tokens=count_tokens(&comma_tok);
    
    struct arc_plan_action *plan_action=malloc(sizeof(struct arc_plan_action));
    if(num_tokens==3)
    {
	char *property=next_token(&comma_tok);
	char *type=next_token(&comma_tok);
	char *value=next_token(&comma_tok);
	
	plan_action->op=ADD_SYS_PROP;
	plan_action->num_args=3;
	plan_action->args=malloc(sizeof(char*)*3);

	
	plan_action->args[0]=malloc(strlen(property)+1);
	strcpy(plan_action->args[0],property);
	plan_action->args[1]=malloc(strlen(type)+1);
	strcpy(plan_action->args[1],type);
	plan_action->args[2]=malloc(strlen(value)+1);
	strcpy(plan_action->args[2],value);

	
	tokenizer_cleanup(&comma_tok);
	return plan_action;	    
    }
    else if(num_tokens==4)
    {
	char *comp=next_token(&comma_tok);
	char *property=next_token(&comma_tok);
	char *type=next_token(&comma_tok);
	char *value=next_token(&comma_tok);

	plan_action->op=ADD_COMP_PROP;
	plan_action->num_args=4;
	plan_action->args=malloc(sizeof(char*)*4);

	
	plan_action->args[0]=malloc(strlen(comp)+1);
	strcpy(plan_action->args[0],comp);
	plan_action->args[1]=malloc(strlen(property)+1);
	strcpy(plan_action->args[1],property);
	plan_action->args[2]=malloc(strlen(type)+1);
	strcpy(plan_action->args[2],type);
	plan_action->args[3]=malloc(strlen(value)+1);
	strcpy(plan_action->args[3],value);

	
	tokenizer_cleanup(&comma_tok);
	return plan_action;	    
    }
    else if(num_tokens==5)
    {
	char *comp=next_token(&comma_tok);
	char *iface=next_token(&comma_tok);
	char *property=next_token(&comma_tok);
	char *type=next_token(&comma_tok);
	char *value=next_token(&comma_tok);

	plan_action->op=ADD_IFACE_PROP;
	plan_action->num_args=5;
	plan_action->args=malloc(sizeof(char*)*5);

	
	plan_action->args[0]=malloc(strlen(comp)+1);
	strcpy(plan_action->args[0],comp);
	plan_action->args[1]=malloc(strlen(iface)+1);
	strcpy(plan_action->args[1],iface);
	plan_action->args[2]=malloc(strlen(property)+1);
	strcpy(plan_action->args[2],property);
	plan_action->args[3]=malloc(strlen(type)+1);
	strcpy(plan_action->args[3],type);
	plan_action->args[4]=malloc(strlen(value)+1);
	strcpy(plan_action->args[4],value);

	
	tokenizer_cleanup(&comma_tok);
	return plan_action;	    
	
    }
    else
    {
	free(plan_action);
	tokenizer_cleanup(&comma_tok);
	fprintf(stderr,"Wrong number of arguments passed \n");
	return NULL;
    }

    

}			    
struct arc_plan_action* plan_parse_prop_rem(char *args)
{			    
    struct tokenizer comma_tok;
    tokenizer_init(&comma_tok,args,",");
    int num_tokens=count_tokens(&comma_tok);
    
    struct arc_plan_action *plan_action=malloc(sizeof(struct arc_plan_action));
    if(num_tokens==3)
    {
	char *property=next_token(&comma_tok);
	char *type=next_token(&comma_tok);
	char *value=next_token(&comma_tok);
	
	plan_action->op=REM_SYS_PROP;
	plan_action->num_args=3;
	plan_action->args=malloc(sizeof(char*)*3);

	
	plan_action->args[0]=malloc(strlen(property)+1);
	strcpy(plan_action->args[0],property);
	plan_action->args[1]=malloc(strlen(type)+1);
	strcpy(plan_action->args[1],type);
	plan_action->args[2]=malloc(strlen(value)+1);
	strcpy(plan_action->args[2],value);

	
	tokenizer_cleanup(&comma_tok);
	return plan_action;	    
    }
    else if(num_tokens==4)
    {
	char *comp=next_token(&comma_tok);
	char *property=next_token(&comma_tok);
	char *type=next_token(&comma_tok);
	char *value=next_token(&comma_tok);

	plan_action->op=REM_COMP_PROP;
	plan_action->num_args=4;
	plan_action->args=malloc(sizeof(char*)*4);

	
	plan_action->args[0]=malloc(strlen(comp)+1);
	strcpy(plan_action->args[0],comp);
	plan_action->args[1]=malloc(strlen(property)+1);
	strcpy(plan_action->args[1],property);
	plan_action->args[2]=malloc(strlen(type)+1);
	strcpy(plan_action->args[2],type);
	plan_action->args[3]=malloc(strlen(value)+1);
	strcpy(plan_action->args[3],value);

	
	tokenizer_cleanup(&comma_tok);
	return plan_action;	    
    }
    else if(num_tokens==5)
    {
	char *comp=next_token(&comma_tok);
	char *iface=next_token(&comma_tok);
	char *property=next_token(&comma_tok);
	char *type=next_token(&comma_tok);
	char *value=next_token(&comma_tok);

	plan_action->op=REM_IFACE_PROP;
	plan_action->num_args=5;
	plan_action->args=malloc(sizeof(char*)*5);

	
	plan_action->args[0]=malloc(strlen(comp)+1);
	strcpy(plan_action->args[0],comp);
	plan_action->args[1]=malloc(strlen(iface)+1);
	strcpy(plan_action->args[1],iface);
	plan_action->args[2]=malloc(strlen(property)+1);
	strcpy(plan_action->args[2],property);
	plan_action->args[3]=malloc(strlen(type)+1);
	strcpy(plan_action->args[3],type);
	plan_action->args[4]=malloc(strlen(value)+1);
	strcpy(plan_action->args[4],value);


	tokenizer_cleanup(&comma_tok);
	return plan_action;	    
	
    }
    else
    {
	free(plan_action);
	tokenizer_cleanup(&comma_tok);
	fprintf(stderr,"Wrong number of arguments passed \n");
	return NULL;
    }

}

const char *plan_return_status(enum PLAN_RET_CODES ret)
{
    return plan_errors[ret];
}


void cleanup_plan(struct arc_plan *plan)
{
    for(int i=0;i<plan->num_actions;i++)
    {
	if(plan->actions[i])
	{
	    plan_action_cleanup(plan->actions[i]);
	    free(plan->actions[i]);
	}
    }
    plan_destroy(plan);     
}
