#include "planner.h"

#include <stdio.h>
#include <stdlib.h>


void init_planner(struct planner *pl, struct mape_k *mape_k)
{
    pl->plans=malloc(sizeof(struct plan*)*NUM_PLANS_INIT);
    pl->max_plans=NUM_PLANS_INIT;
    pl->num_plans=0;    
    
    pl->mape_k = mape_k;
}

void run_planner(struct planner *pl)
{
    for(int i=0;i<pl->num_plans;i++)
    {
	struct plan *a = pl->plans[i];
	run_plan(a);
    }
    
}

void destroy_planner(struct planner *pl)
{
    free(pl->plans);
}


void planner_add_plan(struct planner *pl, struct plan *p)
{
    if(pl->num_plans>=pl->max_plans)
    {
        pl->max_plans*=2;
	pl->plans=realloc(pl->plans,
			      pl->max_plans*sizeof(struct plan*));
    }
    pl->plans[pl->num_plans]=p;
    plan_set_planner(p,pl);
    pl->num_plans++;    

}

void planner_rem_plan(struct planner *pl, struct plan *p)
{
    //TODO: Fill this in
}


bool planner_all_plans_created(struct planner *pl)
{
    for(int i=0;i<pl->num_plans;i++)
    {
	struct plan *p = pl->plans[i];
	enum PLAN_STATUS status = plan_get_status(p);
	if(status != PLAN_DONE)
	{
	    return false;
	}	       
    }
    
    return true;
}
