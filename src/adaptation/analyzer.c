#include "analyzer.h"

#include <stdlib.h>
#include <stdio.h>

void init_analyzer(struct analyzer *ana, struct mape_k *mape_k, strategy_sel_fn strategy_sel)
{

    
    ana->analyses=malloc(sizeof(struct analysis*)*NUM_ANALYSES_INIT);
    ana->max_analyses=NUM_ANALYSES_INIT;
    ana->num_analyses=0;
    ana->mape_k = mape_k;
    ana->strategy_sel = strategy_sel;
    
    ana->mape_k = mape_k;
}

void analyzer_add_analysis(struct analyzer *ana, struct analysis *a)
{
    if(ana->num_analyses>=ana->max_analyses)
    {
        ana->max_analyses*=2;
	ana->analyses=realloc(ana->analyses,
			      ana->max_analyses*sizeof(struct analysis*));
    }
    ana->analyses[ana->num_analyses]=a;
    analysis_set_analyzer(a,ana);
    ana->num_analyses++;    

}

void analyzer_rem_analysis(struct analyzer *ana, struct analysis *a)
{
    //TODO: Fill this in
}


void run_analyzer(struct analyzer *ana)
{
    for(int i=0;i<ana->num_analyses;i++)
    {
	struct analysis *a = ana->analyses[i];
	run_analysis(a);
    }
}


void destroy_analyzer(struct analyzer *ana)
{
    //assumes someone has taken care of cleaning up the analyses
    free(ana->analyses);
}


bool analyzer_all_analyses_done(struct analyzer *ana)
{
    for(int i=0;i<ana->num_analyses;i++)
    {
	struct analysis *a = ana->analyses[i];
	enum ANALYSIS_STATUS status = analysis_get_status(a);
	if(status != ANALYSIS_DONE)
	{
	    return false;
	}	       
    }

    return true;
}
