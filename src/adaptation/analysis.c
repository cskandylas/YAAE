#include "analysis.h"

void init_analysis(struct analysis *ana, analysis_run run, analysis_done done, void *ctx)
{
    ana->run = run;
    ana->done = done;
    ana->ctx = ctx;
    pthread_mutex_init(&ana->lock, NULL);
    
}


void run_analysis(struct analysis *ana)
{
    analysis_set_status(ana, ANALYSIS_RUNNING);
    ana->run(ana);
}

void destroy_analysis(struct analysis *ana)
{
    
}




void *analysis_get_ctx(struct analysis *ana)
{
    void *ret = NULL;
    pthread_mutex_lock(&ana->lock);
    ret = ana->ctx;
    pthread_mutex_unlock(&ana->lock);
    return ret;
}

void analysis_set_status(struct analysis *ana, enum ANALYSIS_STATUS status)
{
    pthread_mutex_lock(&ana->lock);
    ana->status = status;
    pthread_mutex_unlock(&ana->lock);
}

enum ANALYSIS_STATUS analysis_get_status(struct analysis *ana)
{
    enum ANALYSIS_STATUS a_s = ANALYSIS_STATUS_UNDEF;
    pthread_mutex_lock(&ana->lock);
    a_s = ana->status;
    pthread_mutex_unlock(&ana->lock);
    return a_s;
}

struct analyzer *analysis_get_analyzer(struct analysis *ana)
{
    struct analyzer *az = NULL;
    pthread_mutex_lock(&ana->lock);
    az = ana->az;
    pthread_mutex_unlock(&ana->lock);
    return az;
}

void analysis_set_analyzer(struct analysis *ana, struct analyzer *az)
{
    pthread_mutex_lock(&ana->lock);
    ana->az = az;
    pthread_mutex_unlock(&ana->lock);
}

void *analysis_get_result(struct analysis *ana)
{
    void *ret = NULL;
    pthread_mutex_lock(&ana->lock);
    ret = ana->res;
    pthread_mutex_unlock(&ana->lock);
    return ret;
}
