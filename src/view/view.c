#define _XOPEN_SOURCE 600

#include "view.h"

#include <stdio.h>
#include <string.h>
#include <pthread.h>

#include "string_idx.h"


void component_view_init(struct component_view *c_v, struct arc_component *c,
			 uint8_t v_f)
{
    c_v->component=c;
    c_v->view_flags=v_f;
    if(c)
    {
	c_v->comp_name = strdup(c->comp_name);
    }
}

void component_view_destroy(struct component_view *c_v)
{
    //Still not happy about this...
    free(c_v->comp_name);
}

void component_view_to_dot(struct component_view *c_v, FILE *fp)
{

    
    struct arc_component *c = c_v->component;    
    
    if(!c)
	return;
    
    //Draw based on view flags.
    if(c_v->view_flags == VIEW_FLAG_NAME_ONLY)
    {	
	fprintf(fp,"%s [shape=box]\n",c->comp_name);	
    }
    else
    {
	fprintf(fp,"subgraph %s {\n cluster = true \n label=%s\n"
	       ,c->comp_name,c->comp_name);
	
	if(c_v->view_flags & VIEW_FLAG_INTERFACES)
	{
	    for(int i=0;i<c->num_interfaces;i++)
	    {
		struct arc_interface *i_f = c->interfaces[i];
		if(i_f)
		{
		    fprintf(fp,"%s_%s [shape=box, label=\"%s\"]\n",c->comp_name,
			    i_f->if_name,i_f->if_name);
		}
	    }
	}

	if(c_v->view_flags & VIEW_FLAG_PROPERTIES)
	{
	    for(int i=0;i<c_v->component->num_properties;i++)
	    {
		struct arc_property *prop = c->properties[i];
		if(prop)
		{
		    fprintf(fp,"%s_%s [shape=note, label=\"%s\"]\n",
			   c->comp_name, prop->name, prop->name);

		    if( (c_v->view_flags & VIEW_FLAG_PROP_VALUE) )
		    {
			char prop_value[128];
			if(prop->type == P_STRING)
			{
			    fprintf(fp, "%s_%s_VALUE [ label=\"%s\"]\n",
				    c->comp_name, prop->name, prop->v_str);
			}
			else if(prop->type == P_DOUBLE)
			{
			    fprintf(fp, "%s_%s_VALUE [ label=\"%.4f\"]\n",
				    c->comp_name, prop->name, prop->v_dbl);
			}
			char prop_edge[256];
			fprintf(fp,"%s_%s -> %s_%s_VALUE\n",
				c->comp_name, prop->name,c->comp_name, prop->name);
		    }
		    
		}
	    }
	    
	    for(int j=0;j<c->num_interfaces;j++)
	    {
		struct arc_interface *i_f = c->interfaces[j];
		if(i_f)
		{
		    for(int i=0;i<i_f->num_properties;i++)
		    {
			struct arc_property *prop = i_f->properties[i];
			if(prop)
			{
			    fprintf(fp,"%s_%s_%s [shape=note, label=\"%s.%s\"]\n",
				    c->comp_name,i_f->if_name, prop->name, i_f->if_name, prop->name);
			    
			    if( (c_v->view_flags & VIEW_FLAG_PROP_VALUE) )
			    {
				char prop_value[128];
				if(prop->type == P_STRING)
				{
				    fprintf(fp, "%s_%s_%s_VALUE [ label=\"%s\"]\n",
					    c->comp_name,i_f->if_name, prop->name, prop->v_str);
				}
				else if(prop->type == P_DOUBLE)
				{
				    fprintf(fp, "%s_%s_%s_VALUE [ label=\"%.4f\"]\n",
					    c->comp_name,i_f->if_name, prop->name, prop->v_dbl);
				}
				char prop_edge[256];
				fprintf(fp, "%s_%s_%s -> %s_%s_%s_VALUE\n",
					c->comp_name,i_f->if_name, prop->name,c->comp_name,i_f->if_name, prop->name);

			    }
			    
			}
		    }
		}
	    }
	
	
	}
    
	fprintf(fp,"}\n");
    }
    
}


void system_view_init(struct system_view *s_v, struct arc_system *s,uint8_t vf)
{
    s_v->system=s;
    s_v->default_flags = vf;
    
    size_t num_components = s->num_components;
    s_v->num_component_views = num_components;

    s_v->max_component_views = MAX_INIT_COMPONENT_VIEWS;
    while(s_v->num_component_views > s_v->max_component_views)
    {
	s_v->max_component_views *=2;
    }
    
    
    
    s_v->component_views = malloc(s_v->max_component_views * sizeof(struct component_view));

    memset(s_v->component_views, 0, s_v->max_component_views * sizeof(struct component_view));
    
    for(int i=0;i<num_components;i++)
    {
	if(s->components[i])
	{
	    component_view_init(&s_v->component_views[i],s->components[i],vf);
	}
	else
	{
	    //THIS WAS FIXED BUT EH, let it be for safety
	    component_view_init(&s_v->component_views[i],NULL,vf);
	}
    }
    
    
}

void system_view_add(struct system_view *s_v,struct component_view *c_v)
{
    if( s_v->num_component_views >= s_v->max_component_views)
    {
	s_v->max_component_views *=2;
	//this goes to shit if realloc fails
	s_v->component_views = realloc(s_v->component_views,
				       s_v->max_component_views
				       * sizeof(struct component_view));
    }

    s_v->component_views[s_v->num_component_views] = *c_v;
    s_v->num_component_views++;
	    
}


void system_view_rem(struct system_view *s_v,struct component_view *c_v)
{
    char *comp_name = c_v->comp_name;
    for(int i=0;i<s_v->num_component_views;i++)
    {
	char *ith_comp_name = s_v->component_views[i].comp_name;
	
	if( strcmp(ith_comp_name,comp_name) == 0)
	{
	    //trick to remove an item in O(1)
	    if(i != s_v->num_component_views-1)
	    {
		//component_view_destroy(&s_v->component_views[i]);
		//Possibly leaking s_v->component_views[i].comp_name, check later
		s_v->component_views[i] = s_v->component_views[s_v->num_component_views-1];

	    }
	    s_v->num_component_views--;
	}
    }
}

void system_view_update(struct system_view *s_v, struct arc_system *system)
{
    //for every component in the system, check if it is in the view
    for(int i=0;i<system->num_components;i++)
    {
	struct arc_component *c = system->components[i];
	if(!component_view_for(s_v,c->comp_name))
	{
	    printf("Adding %s\n",c->comp_name);
	    //add a new component view for this component;
	    struct component_view c_v;
	    component_view_init(&c_v,c, s_v->default_flags);
	    system_view_add(s_v,&c_v);
	    //this sucks
	    //free(c_v.comp_name);
	}
    }

    //for every component_view check if it is in the system//this might have invalid pointers,
    //keep comp_name separately...
    for(int i=0;i<s_v->num_component_views;i++)
    {
	struct component_view *c_v = &s_v->component_views[i];
	
	if(!sys_find_component(system,c_v->comp_name))
	{
	    printf("Removing %s\n",c_v->comp_name);
	    system_view_rem(s_v,c_v);
	    
	}
    }
}

void system_view_destroy(struct system_view *s_v)
{
    for(int i=0;i<s_v->num_component_views;i++)
    {
	struct component_view *c_v = &s_v->component_views[i];
	if(c_v && c_v->comp_name)
	{
	    free(c_v->comp_name);
	}
    }
    
    free(s_v->component_views);
}

struct component_view *component_view_for(struct system_view *s_v, char *name)
{
    for(int i=0;i<s_v->num_component_views;i++)
    {
	if(s_v->component_views[i].component)
	{
	    if(strcmp(s_v->component_views[i].comp_name,name)==0)
	    {
		return &s_v->component_views[i];
	    }
	}
    }

    return NULL;
}



void system_view_to_dot(struct system_view *s_v, char *filename)
{

    struct arc_system *sys = s_v->system;
    
    FILE *fp=fopen(filename,"w");
    fprintf(fp,"Digraph {\n rankdir=\"LR\"\n compound=true;\n");

    for(int i=0;i<s_v->num_component_views;i++)
    {
	component_view_to_dot(&s_v->component_views[i],fp);
    }


    
    
    for(int i=0;i<s_v->system->num_invocations;i++)
    {

	struct arc_invocation *i_v = s_v->system->invocations[i];
	if(i_v)
	{
	    struct component_view *f_v = component_view_for(s_v, i_v->from->comp_name);
	    struct component_view *t_v = component_view_for(s_v, i_v->to->comp_name);
	    
	    if(f_v->view_flags == VIEW_FLAG_NAME_ONLY
	       && t_v->view_flags == VIEW_FLAG_NAME_ONLY)
	    {
		fprintf(fp,"%s -> %s\n",i_v->from->comp_name,i_v->to->comp_name);
	    }
	    else if( (f_v->view_flags & VIEW_FLAG_INTERFACES)
		     && t_v->view_flags == VIEW_FLAG_NAME_ONLY)
	    {
		//if source is an explanded box and dest is a single box then:
		struct arc_interface *dummy_iface= i_v->from->interfaces[0];
		fprintf(fp,"%s_%s -> %s_%s [ltail=%s]\n",i_v->from->comp_name,dummy_iface->if_name,
			i_v->to->comp_name,i_v->to->comp_name,i_v->from->comp_name);
	    }
	    else if( (f_v->view_flags == VIEW_FLAG_NAME_ONLY)
		     && (t_v->view_flags & VIEW_FLAG_INTERFACES) )
	    {

		//if source is a single box and dest is an expanded box then:
		fprintf(fp,"%s -> %s\n",i_v->from->comp_name,i_v->iface->if_name);
		
	    }
	    else if ( (t_v->view_flags & VIEW_FLAG_INTERFACES)
		      && (f_v->view_flags & VIEW_FLAG_INTERFACES) )
	    {
		struct arc_interface *dummy_iface= i_v->from->interfaces[0];
		//both are expanded
		if(dummy_iface)
		{
		    fprintf(fp,"%s_%s -> %s_%s [ltail=%s]\n",
			    i_v->from->comp_name,dummy_iface->if_name,
			    i_v->to->comp_name,i_v->iface->if_name,i_v->from->comp_name);
		}
	    }
	}
    }

    fprintf(fp,"}");

    fclose(fp);
}
