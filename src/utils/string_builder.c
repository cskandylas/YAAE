/*
*  Copyright 2023 Charilaos Skandylas <Nope>
   Heavily based on:
   https://gitlab.com/herrhotzenplotz/hotzenbot-2/-/blob/trunk/src/sb.h
   by:  Nico Sonack <nsonack@herrhotzenplotz.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "string_builder.h"

#include <string.h>


void string_builder_init(struct string_builder *s_b, size_t n_bytes)
{
    mem_arena_init(&s_b->m_a, n_bytes);
    s_b->len=0;
    s_b->num_chunks=0;
}

void string_builder_append_str(struct string_builder *s_b, char *str)
{

    int len = strlen(str);
    struct string_chunk *c;
    c = mem_arena_alloc(&s_b->m_a,sizeof(struct string_chunk));
    c->str = str;
    c->next = NULL;
    c->len = len;
	
    if(!s_b->tail)
    {		
	s_b->head=s_b->tail=c;
	
    }
    else
    {
	s_b->tail->next = c;
	s_b->tail = c;
    }

    s_b->len += len;
    s_b->num_chunks++;
    
}
struct string_builder *string_builder_chain(struct string_builder *l,
					    struct string_builder *r)
{
    if(!l->head)
    {
	l->head=r->head;
	l->tail=r->tail;
	l->len=r->len;
	l->num_chunks = r->num_chunks;
    }
    else
    {
	l->tail->next=r->head;
	l->tail = r->tail;
	l->len +=  r->len;
	l->num_chunks += r->num_chunks;
	//need to do a bit more work here maybe?
    }
}



void string_builder_prepend_str(struct string_builder *s_b, char *str)
{

    int len = strlen(str);
    struct string_chunk *c;
    c = mem_arena_alloc(&s_b->m_a,sizeof(struct string_chunk));
    c->str = str;
    c->next = s_b->head;;
    c->len=len;
    
    if(!s_b->head)
    {		
	s_b->head=s_b->tail=c;
    }
    else
    {
	s_b->head = c;
    }

    s_b->len+=len;
    s_b->num_chunks++;
}


bool string_builder_to_str(struct string_builder *s_b, char *b_buff,
			   size_t b_buff_len)
{
    if(b_buff_len < s_b->len +1)
    {
	return false;
    }
    else
    {
	struct string_chunk *c = s_b->head;

	size_t off = 0;
	while(c)
	{
	    memcpy(&b_buff[off],c->str,c->len);
	    off+= c->len;
	    
	    c = c->next;
	}
	b_buff[s_b->len]='\0';
	return true;
    }
}


void string_builder_destroy(struct string_builder *s_b)
{
    mem_arena_destroy(&s_b->m_a);
    s_b->len=0;
    s_b->num_chunks;
    s_b->head=s_b->tail=NULL;
}
