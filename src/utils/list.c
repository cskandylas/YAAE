#include "list.h"

void list_node_init(struct list_node *l_n, void *elem)
{
    l_n->elem = elem;
    l_n->next = NULL;
}

void list_init(struct list *l)
{
    l->head = l->tail = NULL;
    l->len = 0;
}

void list_tail_add(struct list *l, struct list_node *l_n)
{
    if(!l->tail)
    {
	l->head = l->tail = l_n;
    }
    else
    {
	l->tail->next = l_n;
	l->tail = l_n;
    }
    l->len++;
}

void list_head_add(struct list *l, struct list_node *l_n)
{
    if(!l->head)
    {
	l->head = l->tail = l_n;
    }
    else
    {
	l_n->next = l->head;
	l->head = l_n;
    }
    l->len++;
}


struct list_node *list_rem(struct list *l)
{
    struct list_node *ret =  l->head;
    if(l->head)
    {
	l->head = l->head->next;
	l->len--;
    }
    
    return ret;
}

