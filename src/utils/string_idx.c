#include "string_idx.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void string_idx_init(struct string_idx *s_i, size_t max_elems, size_t buff_len)
{
    s_i->max_elements = max_elems;
    s_i->num_elements = 0;
    s_i->buffer_len = buff_len;

    s_i->idx = malloc(s_i->max_elements*sizeof(size_t));
    
    s_i->buffer = malloc(s_i->buffer_len);
    memset(s_i->buffer, 0, s_i->buffer_len);
}

void string_idx_put(struct string_idx *s_i, char *str)
{
    size_t len = strlen(str)+1;
    size_t prev_len = 0;
    
    if(s_i->num_elements > 0)
    {
	prev_len = s_i->idx[s_i->num_elements-1];
    }

    size_t new_len = prev_len + len;

    
    
    if( s_i->num_elements >= s_i->max_elements )
    {
	s_i->max_elements *= 2;
	s_i->idx=realloc(s_i->idx, s_i->max_elements * sizeof(size_t));
    }

    if( new_len >= s_i->buffer_len)
    {
	s_i->buffer_len *= 2;
	s_i->buffer = realloc(s_i->buffer, s_i->buffer_len);
    }
    
    s_i->idx[s_i->num_elements]= new_len;
    strcpy(&s_i->buffer[s_i->idx[s_i->num_elements]],str);
    s_i->num_elements++;
    
}

char *string_idx_get(struct string_idx *s_i, size_t idx)
{
    if(idx > s_i->num_elements -1 )
    {
	return NULL;
    }


    return &s_i->buffer[s_i->idx[idx]];
}


void string_idx_destroy(struct string_idx *s_i)
{
    free(s_i->idx);
    free(s_i->buffer);
}


int example(void)
{
    struct string_idx s_i;

    string_idx_init(&s_i,128,2048);
    for(int i=0;i<1024*1024;i++)
    {
	char buff[10];
	sprintf(buff,"%d",i);
	string_idx_put(&s_i,buff);
    }

    printf("%s %s %s\n",string_idx_get(&s_i,1024),string_idx_get(&s_i,128*1024),string_idx_get(&s_i,1023*1024));
    string_idx_destroy(&s_i);
    return 0;
}
