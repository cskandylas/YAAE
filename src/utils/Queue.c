#include "Queue.h"
#include <string.h>


void  queue_init(struct generic_queue *queue)
{
	queue->head=queue->tail=NULL;
	queue->size=0;
}
void  queue_push_front(struct generic_queue *queue,void *elem)
{

    struct queue_entry *entry=malloc(sizeof(struct queue_entry));
    entry->elem=elem;
    entry->prev=NULL;
    entry->next=queue->head;
    if(!queue->head)
    {
	queue->head=queue->tail=entry;
    }
    else
    {
	queue->head->prev=entry;
	queue->head=entry;
    }
    
	queue->size++;
	
}
void  queue_push_back(struct  generic_queue  *queue,void *elem)
{
	struct queue_entry *entry = malloc(sizeof(struct queue_entry));
	entry->elem=elem;
	entry->prev=queue->tail;
	entry->next=NULL;
	if(!queue->tail)
	{
		queue->head=queue->tail=entry;
	}
	else
	{
		queue->tail->next=entry;
		queue->tail=entry;
	}
	queue->size++;
}

void* queue_pop_front(struct generic_queue *queue)
{
	if(queue->head)
	{
		struct queue_entry *entry=queue->head;
		void *ret=entry->elem;
		
		if(queue->head->next)
		{
		queue->head=queue->head->next;
		queue->head->prev=NULL;
		}
		else
		{
		    queue->head=queue->tail=NULL;
		}
		queue->size--;
		free(entry);
		return ret;
		
	}
	return NULL;
}

void* queue_pop_back(struct generic_queue *queue)
{
	if(queue->tail)
	{
		struct queue_entry *entry=queue->tail;
		void *ret=entry->elem;
		free(entry);
		queue->tail=queue->tail->prev;
		queue->tail->next=NULL;
		queue->size--;
		return ret;
	}
	return NULL;
}
//returning value in case it needs to be freed
void* queue_remove(struct generic_queue *queue, struct queue_entry *entry)
{
	if(entry->prev)
		entry->prev->next=entry->next;
	if(entry->next)
		entry->next->prev=entry->prev;
	//if(entry==queue->head)
	//	queue->head=entry->next;
	//if(entry==queue->tail)
	//	queue->tail=entry->prev;
	void *value=entry->elem;
	free(entry);
	queue->size--;
	if(queue->size==0)
	{
		queue->head=queue->tail=NULL;
	}
	return value;
}

//To compare  objects
int   queue_find(struct generic_queue *queue, void *elem,
		 int (*cmpfunc)(void *elem1,void *elem2))
{
	if(!elem)
	{
		return -1;
	}
	struct queue_entry *entry=queue->head;
	while(entry)
	{
		if(cmpfunc(entry->elem,elem)==0)
		{
			return 0;
		}
		entry=entry->next;
	}
	return 1;
	
}

void   queue_destroy(struct generic_queue *queue)
{
	struct queue_entry *elem=queue->head;
	while(elem)
	{
		struct queue_entry *tmp=elem;
		elem=elem->next;
		free(tmp);
	}
	queue->size=0;
	queue->head=queue->tail=NULL;

}


void queue_cpy(struct generic_queue *dst,struct generic_queue *src)
{

    //sanity checks
    if(src->head && dst->tail)
    {
	//Link src head to dst tail->next
	dst->tail->next=src->head;
	//new tail is now in src tail
	dst->tail=src->tail;
	//free ptr of src
	free(src);
	src=NULL;
    }
}

void* queue_get_idx(struct generic_queue *queue, size_t idx)
{

    size_t count=0;
    struct queue_entry *elem=queue->head;
    while(elem)
    {
	if(count==idx)
	    return elem->elem;
	elem=elem->next;
	count++;
    }
    return NULL;
}



//some common comparison functions

int str_cmp(void *str_a,void *str_b)
{
    const char *s_a=str_a;
    const char  *s_b=str_b;
    return strcmp(s_a,s_b);
}

