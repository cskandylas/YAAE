#include "parse_common.h"
#include "arc_recognizer.h"

#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <stdio.h>


void strip_quotes(char *str)
{
    size_t len = strlen(str);
    if(len >= 2 && str[0] == '"' && str[len-1] == '"')
    {
	for(size_t i=0;i<len-2;i++)
	{
	    str[i]=str[i+1];
	}
	str[len-2]='\0';
    }

}

//This is essentially lookahead
bool peek_type(struct token_stream *t_s, enum ARC_TOKEN_TYPE t_type, size_t *pos)
{
	struct token *token = &t_s->tokens[*pos];
	
    if(token->t_type != t_type)
    {
		return false;
    }
    return true;
}

bool peek_str(struct token_stream *t_s, enum ARC_TOKEN_TYPE t_type, char *str,  size_t *pos)
{
	struct token *token = &t_s->tokens[*pos];
	
    if(token->t_type != t_type || strcmp(str,token->str) != 0)
    {
		return false;
    }
    return true;
}

bool consume_p(struct token_stream *t_s, enum ARC_TOKEN_TYPE t_type, size_t *pos)
{

	struct token *token = &t_s->tokens[*pos];
	
    if(token->t_type != t_type)
    {
		fprintf(stderr, "Expected [%s], got [%s] instead!\n", TOKEN_NAME[t_type], token->str);
		return false;
    }
	
    (*pos)++;

    return true;
}

bool eat_until(struct token_stream *t_s, enum ARC_TOKEN_TYPE t_type, size_t *pos)
{

    if(*pos >= t_s->num_tokens)
    {
	    return false;
    }
    
    //keep eating tokens until we encouter a token of type t_type
    while( t_s->tokens[*pos].t_type != t_type)
    {
		(*pos)++;
		
		if(*pos >= t_s->num_tokens)
		{
			return false;
		}
    }
	
    return true;
}

bool expect(struct token_stream *t_s, char *expected, size_t pos)
{
	
	char *token = t_s->tokens[pos].str;
		   
    if(strcmp(token,expected)==0)
    {
		return true;
    }
    else
    {
		fprintf(stderr,"Found [%s], expected: [%s]\n",token,expected);
    }

    return false;
}




inline int consume(char *token, char *expected,size_t *cur_token)
{
    if(strcmp(token,expected)==0)
    {
	(*cur_token)++;
	return 1;
    }
    else
    {
	#ifdef DEBUG
	fprintf(stderr,"Found [%s], expected: [%s]\n",token,expected);
	#endif
    }

    return 0;
}


char* parse_name(char **tokens, size_t num_tokens, size_t *cur_token)
{
    if(isalpha(tokens[*cur_token][0]))
    {
	char *ret=malloc(strlen(tokens[*cur_token])+1);
	strcpy(ret,tokens[*cur_token]);
	(*cur_token)++;
	return ret;
    }
    return NULL;
}

int parse_int(char **tokens, size_t num_tokens, size_t *cur_token)
{
    int val=atoi(tokens[*cur_token]);
    if(val!=0 || strcmp(tokens[*cur_token],"0")==0)
    {
	(*cur_token)++;
    }
    return val;
}

double parse_double(char **tokens, size_t num_tokens, size_t *cur_token)
{
    double val=atof(tokens[*cur_token]);
    if(val!=0.0 || strcmp(tokens[*cur_token],"0.0")==0)
    {
		(*cur_token)++;
    }
    return val;
}

char* parse_string(char **tokens, size_t num_tokens, size_t *cur_token)
{
    int len=strlen(tokens[*cur_token]);
    if(tokens[*cur_token][0]=='\'' && tokens[*cur_token][len-1]=='\'')
    {
	char *ret=malloc(len-1);
	strncpy(ret,&tokens[*cur_token][1],len-2);
	ret[len-2]='\0';

	(*cur_token)++;
	
	return ret;
    }
    return NULL;
}
