#define _XOPEN_SOURCE 600

#include "ini.h"

#include <string.h>

#include <stdio.h>

void ini_str_property_init(struct ini_property *i_p, char *key,  char *str)
{
    i_p->key = strdup(key);
    i_p->p_type = INI_P_STR;

    
    
    i_p->value.str = strdup(str);
    
}    

void ini_num_property_init(struct ini_property *i_p, char *key,  double num)
{
    i_p->key = strdup(key);
    i_p->p_type = INI_P_NUM; 
    i_p->value.num = num;
}
void ini_property_destroy(struct ini_property *i_p)
{
    free(i_p->key);

    if(i_p->p_type == INI_P_STR)
    {
	free(i_p->value.str);
    }
}


void ini_subsection_init(struct  ini_subsection *i_ss, char *name)
{
    i_ss->name = strdup(name);
    i_ss->num_properties = 0;
    i_ss->max_properties = INI_INIT_NUM_PROPERTIES;

    //Stil *very unhappy about this*
    i_ss->properties = malloc(i_ss->max_properties*sizeof(struct ini_property));
}

void ini_subsection_add(struct ini_subsection *i_ss,
			struct ini_property *p)
{
    //Properties array is full so realloc
    if(i_ss->num_properties >= i_ss->max_properties)
    {
	i_ss->max_properties *= 2;
	//if realloc fails we're doomed
	i_ss->properties = realloc(i_ss->properties,
				   i_ss->max_properties*sizeof(struct ini_property));
    }
    
    i_ss->properties[i_ss->num_properties] = *p;
    i_ss->num_properties++;
}

void ini_subsection_destroy(struct ini_subsection *i_ss)
{
    free(i_ss->name);
    for(size_t i=0;i<i_ss->num_properties;i++)
    {
	ini_property_destroy(&i_ss->properties[i]);
    }
    free(i_ss->properties);
}


void ini_section_init(struct ini_section *i_s, char *name)
{
    i_s->name = strdup(name);
    i_s->num_properties = 0;
    i_s->max_properties = INI_INIT_NUM_PROPERTIES;

    //Stil *very unhappy about this*
    i_s->properties = malloc(i_s->max_properties*sizeof(struct ini_property));


    i_s->num_subsections = 0;
    i_s->max_subsections = INI_INIT_NUM_SUBSECTIONS;
    i_s->subsections = malloc(i_s->max_subsections * sizeof(struct ini_subsection));
    
}
void ini_section_add_property(struct ini_section *i_s,
			      struct ini_property *p)
{
     //Properties array is full so realloc
    if(i_s->num_properties >= i_s->max_properties)
    {
	i_s->max_properties *= 2;
	//if realloc fails we're doomed
	i_s->properties = realloc(i_s->properties,
				   i_s->max_properties*sizeof(struct ini_property));
    }
    
    i_s->properties[i_s->num_properties] = *p;
    i_s->num_properties++;
}
void ini_section_add_subsection(struct ini_section *i_s,
				struct ini_subsection *i_ss)
{
    //Subsections array is full so realloc
    if(i_s->num_subsections >= i_s->max_subsections)
    {
	i_s->max_subsections *= 2;
	//if realloc fails we're doomed
	i_s->subsections = realloc(i_s->subsections,
				   i_s->max_subsections*sizeof(struct ini_subsection));
    }
    
    i_s->subsections[i_s->num_subsections] = *i_ss;
    i_s->num_subsections++;
}

struct ini_subsection *ini_section_find_subsection(struct ini_section *i_s,
						   char *name)
{
    for(size_t i=0;i<i_s->num_subsections;i++)
    {
	struct ini_subsection *i_ss = &i_s->subsections[i];
	if(strcmp(i_ss->name,name)==0)
	{
	    return i_ss;
	}
    }

    return NULL;
}

void ini_section_destroy(struct ini_section *i_s)
{
   free(i_s->name);
   for(size_t i=0;i<i_s->num_properties;i++)
   {
       ini_property_destroy(&i_s->properties[i]);
   }
   free(i_s->properties);

   for(size_t i=0;i<i_s->num_subsections;i++)
   {
       ini_subsection_destroy(&i_s->subsections[i]);
   }
   free(i_s->subsections);
}


void ini_init(struct ini *ini)
{
    
    ini->num_global_props = 0;
    ini->max_global_props = INI_INIT_NUM_PROPERTIES;
    ini->global_props = malloc(ini->max_global_props*sizeof(struct ini_property));

    ini->num_sections = 0;
    ini->max_sections = INI_INIT_NUM_SECTIONS;
    ini->sections = malloc(ini->max_sections * sizeof(struct ini_section));
}

void ini_add_property(struct ini *ini, struct ini_property *i_p)
{
      //Global_Props array is full so realloc
    if(ini->num_global_props >= ini->max_global_props)
    {
	ini->max_global_props *= 2;
	//if realloc fails we're doomed
	ini->global_props = realloc(ini->global_props,
				   ini->max_global_props*sizeof(struct ini_property));
    }
    
    ini->global_props[ini->num_global_props] = *i_p;
    ini->num_global_props++;
}


void ini_add_section(struct ini *ini, struct ini_section *i_s)
{
      //Sections array is full so realloc
    if(ini->num_sections >= ini->max_sections)
    {
	ini->max_sections *= 2;
	//if realloc fails we're doomed
	ini->sections = realloc(ini->sections,
				   ini->max_sections*sizeof(struct ini_section));
    }
    
    ini->sections[ini->num_sections] = *i_s;
    ini->num_sections++;
}


void ini_destroy(struct ini *ini)
{
    for(size_t i=0;i<ini->num_global_props;i++)
   {
       ini_property_destroy(&ini->global_props[i]);
   }
   free(ini->global_props);

   for(size_t i=0;i<ini->num_sections;i++)
   {
       ini_section_destroy(&ini->sections[i]);
   }
   free(ini->sections);
}

struct ini_section *ini_find_section(struct ini *ini, char *name)
{
    for(size_t i=0;i<ini->num_sections;i++)
    {
	struct ini_section *i_s= &ini->sections[i];
	if(strcmp(i_s->name,name)==0)
	{
	    return i_s;
	}
    }

    return NULL;
}
    

void ini_dump(struct ini *ini)
{
    for(size_t i=0;i<ini->num_global_props;i++)
    {	
	if(ini->global_props[i].p_type == INI_P_STR)
	    printf("%s:%s\n",ini->global_props[i].key,
		   ini->global_props[i].value.str);
	else
	    printf("%s:%g\n",ini->global_props[i].key,
		   ini->global_props[i].value.num);
    }

    for(size_t i=0;i<ini->num_sections;i++)
    {

	struct ini_section *i_s = &ini->sections[i];
	printf("[%s]\n",i_s->name);
	
	for( size_t j=0;j<i_s->num_properties;j++)
	{
	    if(i_s->properties[j].p_type == INI_P_STR)
		printf("%s:%s\n",i_s->properties[j].key,
		       i_s->properties[j].value.str);
	    else
		printf("%s:%g\n",i_s->properties[i].key,
		       i_s->properties[j].value.num);
	}

	for(size_t j=0;j<i_s->num_subsections;j++)
	{
	    struct ini_subsection *i_ss = &i_s->subsections[j];
	    printf("[%s]\n",i_ss->name);

	    for(size_t k=0;k<i_ss->num_properties;k++)
	    {
		if(i_ss->properties[k].p_type == INI_P_STR)
		    printf("%s:%s\n",i_ss->properties[k].key,
			   i_ss->properties[k].value.str);
		else
		    printf("%s:%g\n",i_ss->properties[k].key,
			   i_ss->properties[k].value.num);
	    }
	}
	
    }

    
}

//OKAY LET'S TEST THIS THING
int run(void)
{


    struct ini rat;
    ini_init(&rat);
    struct ini_property rat_eyes;
    ini_str_property_init(&rat_eyes, "eyes", "yes");

    struct ini_property rat_ears;
    ini_str_property_init(&rat_ears, "ears", "yes");

    struct ini_property rat_barfs;
    ini_str_property_init(&rat_barfs, "barfs", "no");


    struct ini_property num_props[14];
    
    for(int i=0;i<14;i++)
    {
	ini_num_property_init(&num_props[i], "num",(double)i);
	ini_add_property(&rat, &num_props[i]);
    }
    
    ini_add_property(&rat, &rat_eyes);
    ini_add_property(&rat, &rat_ears);
    ini_add_property(&rat, &rat_barfs);

   
    //So far so good.

    struct ini_section navigation;
    ini_section_init(&navigation,"navigation");

    struct ini_property pos;
    ini_str_property_init(&pos,"POSITION", "0,0");

    struct ini_property up;
    ini_str_property_init(&up,"UP", "U");

    struct ini_property down;
    ini_str_property_init(&down,"DOWN", "#");

    struct ini_property left;
    ini_str_property_init(&left,"LEFT", "U");

    struct ini_property right;
    ini_str_property_init(&right,"RIGHT", " ");

    ini_section_add_property(&navigation,&pos);
    ini_section_add_property(&navigation,&up);
    ini_section_add_property(&navigation,&down);
    ini_section_add_property(&navigation,&left);
    ini_section_add_property(&navigation,&right);

    struct ini_subsection wander;
    ini_subsection_init(&wander, "navigation.wander");

    struct ini_property wander_strategy;
    ini_str_property_init(&wander_strategy,"strategy", "random");
    ini_subsection_add(&wander,&wander_strategy);

    ini_section_add_subsection(&navigation, &wander);

    struct ini_section navigation_clone;
    ini_section_init(&navigation_clone,"navigation0");
    ini_section_add_property(&navigation_clone,&pos);
    ini_section_add_property(&navigation_clone,&up);
    ini_section_add_property(&navigation_clone,&down);
    ini_section_add_property(&navigation_clone,&left);
    ini_section_add_property(&navigation_clone,&right);
    ini_section_add_subsection(&navigation_clone,&wander);

    

    //duh adding sections helps :D

    ini_add_section(&rat, &navigation);
    ini_add_section(&rat, &navigation_clone);
    
    ini_dump(&rat);

    
    
    return 0;
}
