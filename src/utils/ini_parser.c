#define _POSIX_C_SOURCE 200809L

#include "ini_parser.h"
#include "ini.h"

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>


//carefull to only call this on malloced strings
static void strip_quotes(char *str)
{
    size_t len = strlen(str);
    if(len >= 2 && str[0] == '"' && str[len-1] == '"')
    {
	for(size_t i=0;i<len-2;i++)
	{
	    str[i]=str[i+1];
	}
	str[len-2]='\0';
    }
}


static char *INI_TOKEN_NAME[] =
{
    "INI_TOK_L_BRACKET",
    "INI_TOK_R_BRACKET",
    "INI_TOK_DOT",
    "INI_TOK_EQUALS",
    "INI_TOK_NUMBER",
    "INI_TOK_ID",
    "INI_TOK_STRING",
    "INI_TOK_WSPACE",
    "INI_TOK_END",
    "INI_TOK_UNKNOWN",
    "INI_NUM_TOKENS"
};


void ini_token_init(struct ini_token *token, char *str, enum INI_TOKEN_TYPE t_type)
{
    token->str=str;
    token->t_type = t_type;
}


void ini_token_stream_init(struct ini_token_stream *t_s)
{
    t_s->num_tokens = 0;
    t_s->max_tokens = INI_TOKEN_STREAM_INIT_SIZE;
    t_s->tokens = malloc(t_s->max_tokens * sizeof(struct ini_token));
}

void ini_token_stream_add_token(struct ini_token_stream *t_s,
				struct ini_token *token)
{
    if(t_s->num_tokens >= t_s->max_tokens)
    {
	t_s->max_tokens *= 2;
	t_s->tokens = realloc(t_s->tokens, t_s->max_tokens*sizeof(struct ini_token));
    }

    t_s->tokens[t_s->num_tokens] = *token;
    t_s->num_tokens++;
}


void ini_token_stream_destroy(struct ini_token_stream *t_s)
{
    //Decide if we take ownership of tokens this time, we probably should.
    
    for(int i=0;i<t_s->num_tokens;i++)
    {
	free(t_s->tokens[i].str);
    }
    
    free(t_s->tokens);
}


static enum INI_TOKEN_TYPE recognize_ini_str_token(char *text,int *pos)
{

    int orig_pos = *pos;
    
    if(text[*pos]!='"')
	return INI_TOK_UNKNOWN;
    //move past '"'
    (*pos)++;

    while(text[*pos]!='"')
    {
	if(text[*pos] == '\0')
	{
	    fprintf(stderr,"Reached end of text without encountering '\"' %s",
		    &text[orig_pos]);
	    return INI_TOK_UNKNOWN;
	}
	(*pos)++;
    }
    //move past '"'
    (*pos)++;
    
    return INI_TOK_STRING;
}

static enum INI_TOKEN_TYPE recognize_ini_id_token(char *text,int *pos)
{
    
    if(!isalpha(text[*pos]))
    {
	return INI_TOK_UNKNOWN;
    }
    
    while(isalnum(text[*pos]))
    {
	(*pos)++;
    }

    //Decide if we want to check for whitespace after numbers
    return INI_TOK_ID;
}


//we have to have composite detection for a.b since we declared '.' as a token
static enum INI_TOKEN_TYPE recognize_ini_num_token(char *text,int *pos)
{
    if(!isdigit(text[*pos]))
    {
	return INI_TOK_UNKNOWN;
    }
    
    while(isdigit(text[*pos]))
    {
	(*pos)++;
    }
    return INI_TOK_NUMBER;
}


//let's actually write a character based recognizer this time.
enum INI_TOKEN_TYPE ini_tok_recognize(char *text, int *pos)
{
    //printf("%c\n",text[*pos]);
    //start at text[*pos], do "easy cases first"
    if(text[*pos] == '[')
    {
	(*pos)++;
	return INI_TOK_L_BRACKET;
    }
    else if(text[*pos] == ']')
    {
	(*pos)++;
	return INI_TOK_R_BRACKET;
    }
    else if(text[*pos] == '.')
    {
	(*pos)++;
	return INI_TOK_DOT;
    }
    else if(text[*pos] == '=')
    {
	(*pos)++;
	return INI_TOK_EQUALS;
    }
    else if(text[*pos] == '"')
    {
	return recognize_ini_str_token(text, pos);
    }
    else if(isalpha(text[*pos]))
    {
	return recognize_ini_id_token(text, pos);
    }
    else if(isdigit(text[*pos]))
    {
	return recognize_ini_num_token(text, pos);
    }
    else if(isspace(text[*pos]))
    {
	(*pos)++;
	return INI_TOK_WSPACE;
    }
    else if(text[*pos=='\0'])
    {
	return INI_TOK_END;
    }
    else
    {
	fprintf(stderr, "Could not recognize token: %s at position: %d\n",&text[*pos], *pos);
    }

}

bool build_ini_token_stream(char *text, struct ini_token_stream *t_s)
{

    int pos=0;
    int prev_pos=0;
    
    
    while(pos < strlen(text))
    {
	//printf("%d: %s\n",pos, &text[pos]);
	enum INI_TOKEN_TYPE t_type = ini_tok_recognize(text, &pos);
	if(t_type != INI_TOK_UNKNOWN
	   && t_type != INI_TOK_WSPACE
	   && t_type != INI_TOK_END)
	{
	    int str_len=pos-prev_pos;
	    char *str_start=&text[prev_pos];

	    
	    
	    struct ini_token ini_tok;
	    char *tok_str=strndup(str_start,str_len);
	    ini_token_init(&ini_tok,tok_str,t_type);
	    ini_token_stream_add_token(t_s,&ini_tok);
	}
	else if(t_type == INI_TOK_UNKNOWN)
	{
	    fprintf(stderr, "Could not recognize token at: %d, %s\n",
		    prev_pos, text);
	    return false;
	}
	    
	prev_pos=pos;
    }
    

    /*
    for(int i=0;i<t_s->num_tokens;i++)
    {
	printf("%s:%s\n",INI_TOKEN_NAME[t_s->tokens[i].t_type],t_s->tokens[i].str);
    }
    */

    return true;
}



static bool consume_i(struct ini_token *token, enum INI_TOKEN_TYPE t_type, int *pos)
{
    if(token->t_type != t_type)
    {
	fprintf(stderr, "Expected [%s], got [%s] instead!\n",
		INI_TOKEN_NAME[t_type], token->str);
	return false;
    }
    (*pos)++;

    return true;
}


bool parse_ini_propvalue(struct ini_token_stream *t_s, int *pos,
			 struct ini *ini, struct ini_elem *last)
{
    //peek ahead
    if(t_s->tokens[*pos].t_type == INI_TOK_STRING)
    {
	if(!consume_i(&t_s->tokens[*pos],INI_TOK_STRING,pos))
	{
	    return false;
	}
		
	struct ini_property str_prop;
	strip_quotes(t_s->tokens[*pos-1].str);
	ini_str_property_init(&str_prop,t_s->tokens[*pos-3].str,
			      t_s->tokens[*pos-1].str);

	//we have a string property
	switch(last->l_s)
	{
	case INI_S_INI:
	{
	    ini_add_property(last->ini, &str_prop);
	    break;
	}
	case INI_S_SECTION:
	{
	    ini_section_add_property(last->i_s, &str_prop);
	    break;
	}
	case INI_S_SUBSECTION:
	{
	    ini_subsection_add(last->i_ss, &str_prop);
	    break;
	}
	}
	return true;    

    }
    else if(t_s->tokens[*pos].t_type == INI_TOK_NUMBER)
    {
	if(!consume_i(&t_s->tokens[*pos],INI_TOK_NUMBER,pos))
	{
	    return false;
	}
	if(!consume_i(&t_s->tokens[*pos],INI_TOK_DOT,pos))
	{
	    return false;
	}
	if(!consume_i(&t_s->tokens[*pos],INI_TOK_NUMBER,pos))
	{
	    return false;
	}


	
	int len = strlen(t_s->tokens[*pos-3].str) + strlen(t_s->tokens[*pos-1].str) + 2;
	char buff[len];
	memset(buff,0,len);
	strcat(buff,t_s->tokens[*pos-3].str);
	strcat(buff,".");
	strcat(buff,t_s->tokens[*pos-1].str);
	buff[len-1]='\0';
	    

	double num = atof(buff);
	
	struct ini_property num_prop;
	ini_num_property_init(&num_prop,t_s->tokens[*pos-5].str,
			      num);

	//we have a numeric property
	switch(last->l_s)
	{
	case INI_S_INI:
	{
	    ini_add_property(last->ini, &num_prop);
	    break;
	}
	case INI_S_SECTION:
	{
	    ini_section_add_property(last->i_s, &num_prop);
	    break;
	}
	case INI_S_SUBSECTION:
	{
	    ini_subsection_add(last->i_ss, &num_prop);
	    break;
	}
	}
	
	return true;
	
    }
    else
    {
	fprintf(stderr,"Expected ID or STRING but got %s:%s at %d instead!\n",
		INI_TOKEN_NAME[t_s->tokens[*pos].t_type],
		t_s->tokens[*pos].str, *pos);
	for(int i=0;i<t_s->num_tokens;i++)
	{
	    fprintf(stderr,"%s ",t_s->tokens[*pos].str);
	}
	fprintf(stderr,"\n");
	return false;
    }
}

bool parse_ini_property(struct ini_token_stream *t_s, int *pos,
			struct ini *ini, struct ini_elem *last)
{
    if(!consume_i(&t_s->tokens[*pos],INI_TOK_ID,pos))
    {
	return false;
    }
    if(!consume_i(&t_s->tokens[*pos],INI_TOK_EQUALS,pos))
    {
	return false;
    }
    if(!parse_ini_propvalue(t_s,pos,ini,last))
    {
	return false;
    }

    return true;
}

bool parse_ini_properties(struct ini_token_stream *t_s, int *pos,
			  struct ini *ini,struct ini_elem *last)
{
    
    // deal with epsilon, be optimistic 
    if(*pos >= (t_s->num_tokens-1))
    {
	return true;
    }
    

    if(t_s->tokens[*pos].t_type == INI_TOK_L_BRACKET)
    {
	return true;
    }
    else
    {
	
	if(!parse_ini_property(t_s,pos,ini,last))
	{
	    
	    fprintf(stderr, "Could not parse property at:%d, token:%s %s\n",
		    *pos, t_s->tokens[*pos].str,
		    INI_TOKEN_NAME[t_s->tokens[*pos].t_type]);
	    return false;
	}

	if(!parse_ini_properties(t_s,pos,ini,last))
	{
	    return false;
	}

	return true;
    }
    
}

bool parse_ini_section1(struct ini_token_stream *t_s, int *pos,
			struct ini *ini, struct ini_elem *last)
{
    //lookahead
    if(t_s->tokens[*pos].t_type == INI_TOK_R_BRACKET)
    {
	if(!consume_i(&t_s->tokens[*pos],INI_TOK_R_BRACKET,pos))
	{
	    return false;
	}

	/*
	  Here we've got a section
	                     pos
                              v
	      [sectionname]  
	*/
	
	struct ini_section *i_s = ini_find_section(ini, t_s->tokens[*pos-2].str);

	//Do nothing if the section already exists for now.
	if(!i_s)
	{
	    struct ini_section new_section;
	    ini_section_init(&new_section,t_s->tokens[*pos-2].str);
	    ini_add_section(ini,&new_section);
	    i_s = ini_find_section(ini, t_s->tokens[*pos-2].str);
	}
	last->l_s = INI_S_SECTION;
	last->i_s = i_s;
	
    }
    else if(t_s->tokens[*pos].t_type == INI_TOK_DOT)
    {
	if(!consume_i(&t_s->tokens[*pos],INI_TOK_DOT,pos))
	{
	    return false;
	}

	if(!consume_i(&t_s->tokens[*pos],INI_TOK_ID,pos))
	{
	    return false;
	}

	if(!consume_i(&t_s->tokens[*pos],INI_TOK_R_BRACKET,pos))
	{
	    return false;
	}
	
	//here we know we've got a subsection


	struct ini_section *i_s;
	i_s= ini_find_section(ini, t_s->tokens[*pos-4].str);

	//Do nothing if the section already exists for now.
	if(!i_s)
	{

	    /*
	      Here we decide to add sections for orphaned subsections
	      instead of aborting
	    */
	    
	    struct ini_section new_section;
	    ini_section_init(&new_section,t_s->tokens[*pos-4].str);
	    ini_add_section(ini,&new_section);
	    i_s = ini_find_section(ini, t_s->tokens[*pos-4].str);
	    
	}

	int len=strlen(t_s->tokens[*pos-2].str)+strlen(t_s->tokens[*pos-4].str)+2;
	char buff[len];
	memset(buff,0,len);
	strcat(buff,t_s->tokens[*pos-4].str);
	strcat(buff,".");
	strcat(buff,t_s->tokens[*pos-2].str);
	buff[len-1]='\0';
	
	struct ini_subsection *i_ss;
	i_ss = ini_section_find_subsection(i_s,buff);

	if(!i_ss)
	{
	    struct ini_subsection new_subsection;
	    ini_subsection_init(&new_subsection,buff);
	    ini_section_add_subsection(i_s,&new_subsection);
	    i_ss = ini_section_find_subsection(i_s,buff);
	}

	last->l_s = INI_S_SUBSECTION;
	last->i_ss = i_ss;
	    
	
    }
    else
    {
	if(!parse_ini_sections(t_s,pos,ini,last))
	{
	    fprintf(stderr,"Could not parse section at: %d, token:%s %s\n",
		    *pos, t_s->tokens[*pos].str,
		    INI_TOKEN_NAME[t_s->tokens[*pos].t_type]);
	    return false;
	}
    }
    

    if(!parse_ini_properties(t_s,pos,ini,last))
    {
	return false;
    }

    
    return true;
    
}



bool parse_ini_section(struct ini_token_stream *t_s, int *pos,
		       struct ini *ini,struct ini_elem *last)
{
    if(!consume_i(&t_s->tokens[*pos],INI_TOK_L_BRACKET,pos))
    {
	return false;
    }

    if(!consume_i(&t_s->tokens[*pos],INI_TOK_ID,pos))
    {
	return false;
    }


    if(!parse_ini_section1(t_s,pos,ini,last))
    {
	return false;
    }

    return true;
    
}

bool parse_ini_sections(struct ini_token_stream *t_s, int *pos,
			struct ini *ini,struct ini_elem *last)
{

    
    if(*pos >= (t_s->num_tokens-1))
    {
	return true;
    }
    else
    {
	if(!parse_ini_section(t_s,pos,ini,last))
	{
	    fprintf(stderr,"Could not parse section at: %d, token:%s %s\n",
		    *pos, t_s->tokens[*pos].str,
		    INI_TOKEN_NAME[t_s->tokens[*pos].t_type]);
	    return false;
	}
	if(!parse_ini_sections(t_s,pos,ini,last))
	{
	    fprintf(stderr,"Could not parse sections at: %d, token:%s %s\n",
		    *pos, t_s->tokens[*pos].str,
		    INI_TOKEN_NAME[t_s->tokens[*pos].t_type]);
	    return false;
	}
	return true;
    }
    
}


bool parse_ini(struct ini_token_stream *t_s, int *pos,
	       struct ini *ini,struct ini_elem *last)
{
    if(!parse_ini_properties(t_s,pos,ini,last))
    {
	fprintf(stderr,"Could not parse properties at :%d, token:%s\n",*pos,
		t_s->tokens[*pos].str);
	return false;
    }
    
    if(!parse_ini_sections(t_s,pos,ini,last))
    {
	fprintf(stderr,"Could not parse sections at :%d, token:%s\n",*pos,
		t_s->tokens[*pos].str);
	return false;
    }
    ini_token_stream_destroy(t_s);
    return true;
}




bool ini_parse(char *ini_text, struct ini *ini)
{

    //assumes we passed in an initialized ini
    struct ini_token_stream t_s;
    ini_token_stream_init(&t_s);
    
    build_ini_token_stream(ini_text, &t_s);
    int pos = 0;
    struct ini_elem sel;
    sel.l_s = INI_S_INI;
    sel.ini= ini;
    return parse_ini(&t_s,&pos,ini,&sel);
}
