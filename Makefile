TARGET_LIB ?= libyaae.so
BUILD_DIR ?= ./build
SRC_DIRS ?= ./src
SRCS := $(shell find $(SRC_DIRS) -name *.c)
OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)
DEPS := $(OBJS:.o=.d)

INC_DIRS := ./include
INC_FLAGS := $(addprefix -I,$(INC_DIRS))

CFLAGS = $(INC_FLAGS) -fPIC -O0 -std=c11 -fsanitize=address -g -ggdb  #-pedantic -Wall
#LDFLAGS ?= -L./ -lpthread -lm -ldl  -fsanitize=address -lyaae -Wl,-rpath,./
LDFLAGS = -shared -fsanitize=address -lpthread -lm -ldl -Wl,-fuse-ld=gold 

$(TARGET_LIB): $(OBJS)
	$(CC) ${LDFLAGS} -o $@ $^


$(BUILD_DIR)/%.c.o: %.c
	$(MKDIR_P) $(dir $@)
	$(CC) $(CFLAGS) -c $< -o $@

.PHONY: clean

clean:
	$(RM) -r $(BUILD_DIR)

-include $(DEPS)

MKDIR_P ?= mkdir -p
