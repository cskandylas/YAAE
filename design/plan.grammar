This is an EBNF grammar for the custom tactic DSL for changes in a component based system.


(*
A = { START, LINES, LINE, PROPOP, PUPDATE, PVALUE, GENOP, GENOP1, GENOP2, QSYS,QCOMP, QIFACE, NUM, STRING, ID}
Σ = { '[', ']', '=', 'NIL', '->', ':', [0-9], [a-z] } 
S = START 
*)

(* Our grammar is "just" a bunch of lines which are either architectural
   operations (GENOP) or property operations(PROPOP) one per line *)
START = LINES
LINES = LINE LINES | ε
LINE  = (GENOP | PROPOP) FFI

(* A property operation updates or removes a property of the system,
   a component, or an interface. We support two types of property
   values, numbers which are "just" doubles and opaque strings *)
(* Examples of property update commands:
InsecureStore [NCOMPONENTS] = 6.0 { }  (System property update) 
InsecureStore.backend [NCONNECTED] = 1.0 { } (Component property update)
InsecureStore.backend.login [exploit] = NIL { } ( Interface property u.)
*)

PROPOP  = (QSYS | QCOMP | QIFACE ) PUPDATE
PUPDATE = '[' ID ']' '=' PVALUE
PVALUE  = NUM | STRING | 'NIL'

(* architectural operations include adding and removing components,
   interfaces, and invocations *)
(* Example architectural operation updates:
+ InsecureStore foo { ignore for now } ( adds a component foo )
+ InsecureStore.foo bar { } ( adds an interface bar to foo )
- InsecureStore.foo bar { } ( removes an interface bar from foo )
- InsecureStore foo { }  ( removes the foo component )
+ InsecureStore.backend -> InsecureStore.accountmanager : fetchuser { }
( Adds an invocation from the backend component to the accountmanager )
- InsecureStore.backend -> InsecureStore.accountmanager : fetchuser { }
( Removes invocation from the backend component to the accountmanager )
*)

GENOP  = OP GENOP1 
GENOP1 = QSYS ID | QCOMP GENOP2
GENOP2 = ID | '->' QCOMP ':' ID

(* We have to recognize a few things: Qualified system names(QSYS) ,
   qualified component names(QCOMP) qualified interface names(QIFACE),
   numbers(NUM), strings(STRING) and possibly ids (ID), and
   operators(OP) *)

(* Note that we do not really do symbol resolution at the parser level,
   if needed (highly likely) we will ofload that to a plan checker *)
(* Missusing regex notation here for simplicity *)
QSYS   = ID
QCOMP  = ID '.' ID
QIFACE = ID '.' ID '.' ID
NUM    = '[0-9]'+ '.' '[0-9]'+    
STRING = '"' ('([a-z]|[A-Z]|[0-9])')* '"'
ID     = ('([a-z]|[A-Z]|[0-9])')+  
FFI    = '{' ID* '}'
OP     = '+' | '-'
(* For ease of implementation I will assume ' ' is a break character to make tokenization simpler *)






